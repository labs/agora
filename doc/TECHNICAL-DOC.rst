==============================
Agora technical documentation
==============================

This document contains technical information about the Agora project. This
documentation is indented for the use of programmers. Python vocabulary is used
for data structures and the like when needed.

------------------
Agora data formats
------------------

Agora export file format
=========================

When an initiative is closed for submission of support statements, a file is
exported containing all the support statements. This file is a gzip compressed
format based on CSV.

Each line in the uncompressed output contains comma separated values where the
first value identifies the type of line. The format is stateful, so the order of
lines is significant.
The following types are defined:

* 0 - key for symmetric encryption encrypted using the public key associated
  with the initiative.
* 1 - support statement encrypted using the closest preceding symmetric cipher
  key.
* 2 - statistical information about the file content
* 3 - mapping between ids (used in encrypted data) and names (used in PDF
  output) of ID documents

The following sections describe the format of data for each type mentioned
above. The format describes only the rest of the data excluding the type
identifier.

Key 0
-------

Items:

* encrypted_key - base64 encoded data of the symmetric cipher key encrypted
  using the public key associated with the initiative.

Key 1
-------

Items:

* ccode - iso country code of the country for which this statement was submitted
* date - iso formated date when the statement was submitted
* cipher_name - name of the symmetric encryption cipher used, if format suitable
  for M2Crypto, e.g. 'aes_256_cbc'
* encrypted_data - base64 encoded encrypted data. The plain text of the
  encrypted data contains zlib compressed JSON data describing individual fields
  of the submission (more below).

Key 2
-------

Items:

* number_of_keys - the number of symmetric keys used in this file
* number_of_records - the number of encrypted records in this file

The data in this record can be used to display progress information during
decryption as well as to detect truncation of the file.

Key 3
-------

Items:

* iddoc_names - base64 encoded JSON serialization of mapping id->name for ID
  documents. The name of an ID document is an local name in the language of the
  country to which is belongs.


EDB database file format
=========================

When data from the .agora format is decrypted using the standard client, it is
stored in several .edb file, one for each participating country. This .edb file
contains a standard Sqlite3 database (https://www.sqlite.org/).

The schema is expressed using the following create statements::

    CREATE TABLE records (
      hash TEXT PRIMARY KEY,
      date TEXT,
      given_name TEXT,
      family_name TEXT,
      data BLOB);
    CREATE TABLE iddocs (
      code TEXT PRIMARY KEY,
      name TEXT);

The meaning individual columns are as follows:

* hash - sha1 hash of the data stored in 'data'. This is used for duplicate
  detection. Because the hash is a primary key, duplicate data is automatically
  thrown away on insert.
* date - support statement submission date. Can be used for sorting.
* given_name
* family_name - names extracted from data, these can be used for sorting.
* data - binary BLOB containing zlib compressed data of the statement of
  support in JSON format.


Statement of support JSON format
==================================

Each encrypted statement of support contains JSON data. It is as list of
individual dictionaries, one dict for each value. The format is

{"name": "field name", "value": "field value", "src": "data provider id"}

The 'src' field is optional and is present only in fields which have a value
comming from a data provider (such as MojeID) and was not changed by the user.

The list of fields is sorted by field names, so that a cannonical ordering is
obtained. Because the keys in individual field records are also sorted,
duplicate records can be easily detected.


-----------------------
Encryption in detail
-----------------------

Details about the encryption scheme can be found in the SECURITY-DOC.rst
document.
