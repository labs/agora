=========================
Security documentation
=========================

This document discusses in depth the security features of the Agora
application. It assumes that you have read at least the README.rst and hopefully
even the TECHNICAL-DOC.rst and USER-DOC.rst.

The organization of this document to some extent corresponds to the life-cycle
of an initiative, as described in the USER-DOC.rst document.

----------------------------------
Encryption of support statements
----------------------------------

Creation of an RSA key-pair 
==============================

All statements of support are encrypted using an RSA public key supplied by the
organizer as part of an X509 certificate. This certificate is created using a
custom program provided in the client section of Agora distribution. It
creates RSA keys of size 2048 bits which is considered highly secure.

Uploading of a certificate into Agora
=========================================

As part of the process of creation of a new initiative, the staff of the Agora
provider uploads the certificate supplied by the organizer into the system.

It is necessary, that the organizer verifies proper installation of the
certificate by downloading a sample file and trying to decrypt it (see the
USER-DOC.rst document for more details).

This step is not only necessary to make sure that the organizer is in possesion
of the correct private key which will be used to decrypt the initiative support
statements, it is also necessary to verify that the staff did not (intentionaly
of by chance) upload a different certificate.

Encryption of statement of support
====================================

After successfully submitted, all data from a support statement, except the date
and country name, are encrypted. Depending on the settings, a group of
statements may share the same encryption key (see `Encryption in detail`_
below), but if security considerations described in this document are observed,
the possible fallout of this feature is minimal.

Handling export of support statements
=======================================

When the support statement of one initiative are exported from the system, in
order to be passed on to the organizer, the are still in encrypted form. A
possible leak of the data at this stage should not present data security breach,
provided the private key is kept secret by the organizer.

Handling of data by the organizer
==================================

The organizer of an initiative is the only holder of the private key needed to
decrypt the statements of support. The Agora system does not contain
unencrypted data in any form after the data was written out to the database in
encrypted form and removed from memory.

Therefore the weakest point where data leak might occur is from the organizer,
after the data was decrypted.

It is strongly advised that the organizer should take security precautions, such
as using encryption when storing the data and sharing them with other member of
the organizing team.

------------------------
Web interface security
------------------------

The Agora application consists mainly of a web interface for submission of
statement of support. The following paragraphs discuss how this interface is
secured against possible attacks.

Cross-site request forgery
===========================

Agora uses standard Django method for preventing cross-site request forgery
(CSRF) attacks. This method is based on a secure cookie stored into the users
browser, which is bound to the domain of the system running the Agora
software. The content of this cookie must be present in every form that the user
submits to the Agora system and because other sites do not have access to this
cookie, they are not able to set it properly to perform the attack.

More information about the CSRF protection in Django may be found at
https://docs.djangoproject.com/en/1.4/ref/contrib/csrf/.

SQL injection
===============

All SQL commands are processed internally by the Django framework which ensures
all input is correctly escaped and user data are not inserted into the SQL
command in plain form.

Furthermore, visitors without an account do not have access to any part of the
system, where they would be able to provide text input which would figure in a
subsequent SQL command. 

The only user supplied data which are in form of text is the actual content of a
support statement, which is stored in an encrypted and base64 encoded form, thus
making it impossible to use it for SQL injection attack.


Cross-site scripting (XSS)
============================

The only place where user supplied content is displayed on a page is when the
support statement is submitted and some data need to be fixed. Because this
page is displayed to the current user only, it would not make sense to try
XSS on it.  

In any case, internal Django mechanisms for escaping data in HTML is used.
As Django is used on thousands of websites and is subject to both internal and
external security testing, this method should be safe.

Other user supplied data are limited to logged-in staff users and are subject to
the same limitations (are passed through the same Django escaping routines).


Eavesdropping
================

The Agora system does not prevent eavesdropping on its own. However, when
properly installed, according to the installation instructions, it should be run
on an HTTPS only web, thus preventing eavesdropping by encrypting data using
transport layer security (TLS).

Brute-force attacks
====================

In order to allow organizers an access to the monitoring page of an initiative,
the backstage area of Agora is by default accessible from the internet.

*Note*: When desired, this may be changed by the administrator, for example by
providing an IP-based filter on the HTTP server. This is however beyond the
scope of this document.

Because of this feature, it is possible for an attacker to try a bruteforce
attack against the registered accounts. However, because the Agora system
enforces user passwords to have at least 10 characters, with at least one number
and one special character, such attempts should be relatively harmless.

*Note*: The administrator might further mitigate such attacks by providing an IP
based rate limit to accesses to the Agora backstage. This is however again
beyond the scope of this document.

Machine created support statement
===================================

The Agora system uses ReCAPTCHA to validate entry of user data. Without a
successful solving of a CAPTCHA, the user data are not stored. Thus the system
should be relatively safe against automatic submission of support statement by
automated systems.

*Note*: The only case when Agora does not require CAPTCHA for user data is
when the data comes from a data provider, such as MojeID, rather than from the
actual user. In this case, no modification of data supplied by the provider is
allowed.


--------------
Forensic log
--------------

Because the data are encrypted, it should be impossible for an attacker to read
statements of support stored in the database.

However, a user with significant privileges would be able to delete of modify
the data. Both of these attacks should be mitigated by performing periodic
off-site backups.

Backups however do not solve the problem of detecting such an attack which could
lead to loss of all data or their credibility in case part of the records were
modified to contain obviously bogus data.

To detect any modifications to the database, Agora creates a so called
forensic log, into which all support statement submissions are stored.

The path to the forensic log file could be configured using the ``FORENSIC_LOG``
settings attribute. The file pointed to by ``FORENSIC_LOG`` must be writable by
the user running the webserver (for example in Apache under Ubuntu it is
www-data).

Protecting forensic log
=========================

The first rule is that the forensic log should be protected. To make sure that
the attacker does not change both the database and the forensic log, it is
necessary to periodically backup the forensic log to a different machine. It is
also necessary to ensure that the forensic log was not tampered with before a
new backup is made.

The nature of the log ensures that under normal circumstances, it will only
grow. To check that the new forensic log is an appended version of the last
(backed up) version, the ``check_append_file.py`` and
``check_append_file_simple.sh`` scripts may be used. The first one is written in
Python and does a line-by-line comparison of the files. Thus it can warn you
about where the problem in the file is. It is used like this::

  python check_file_append.py -v backup.log agora_forensic.log

The ``-v`` switch ensures that human readable output will be produced. Without
it, the only information is hidden in the exit status of the script and may be
retreived in a script. Any non-zero value means an error. An example script to
be run before backup might look like this::

  python check_file_append.py backup.log agora_forensic.log
  if [ $? -eq 0 ]; then
      # do new backup 
      echo "OK";
  else
      # report error to admins - possible break-in
      echo "ERROR";
  fi

The ``check_append_file_simple.sh`` is a simple bash script which relies on the
``sha1sum`` command to be present in the system. It uses hashing to detect if
the file given as first argument was appended to to give the second given
file and makes the result available both as human readable text and in its exit
value. In case of a problem, it cannot tell where the problem is, but it might
be faster. It can also be used as a basis for a remote comparison scheme in
which only the sha1 checksum and length are extracted from a backed-up log,
rather than the log itself. (This is not implemented and is left up to the
administrator if desired).


Checking forensic log
=======================

When the log is properly backed up and checked, any ex-post changes to it should
be prevented (with exception of the newest records that were not part of a
backup).
Such a forensic log may be used to check that the content of the database
matches that of the log. To do so (at best periodically, similarly to backups),
use the ``check_forensic_log.py`` script::

  python check_forensic_log.py

If this check is to be used from another script, it would be useful to run it as
follows::

  python check_forensic_log.py -f -q

where ``-f`` means to quit on first error (which may speed up processing in case
of errors) and ``-q`` means quiet operation - no output. Exit status other than
0 means an error of some sorts.
  
For full list of options, run ``check_forensic_log.py`` with ``-h`` option.

Note
======

There is not ready made solution to automatically deal with changes detected
either in the forensic log or in the database. The tools described above could
help you, but it is up to you to incorporate them into your environment,
depending on the used backup solution, etc.


------------
Appendices
------------

Encryption in detail
=====================

The encryption scheme used for statements of support encryption corresponds to
a standard scheme used for RSA public key cryptography. A random key for
symmetric cipher is generated and encrypted using the organizer provided public
key. This encrypted key is then stored into the database.

The symmetric key in unencrypted form is never stored onto a disk and is
only kept in memory. This key is then used for encryption of individual
statements of support using a symmetric cipher, by default 256 bit AES in CBC
mode.

The following schema describes the sources and processing of individual pieces
of stored data::


  Source             User             Random        Organizer
                      |                 |               |
                      V                 V               V
                 +----------+     +-----------+     +--------+
                 | Support  |     | Symmetric |     | Public |
  Memory         | statement|  +--| key       |  +--| key    |
                 +----------+  |  +-----------+  |  +--------+
                      |        |        |        |      |
                      | AES    |        |  RSA   |      | 
                      |--------+        |--------+      |  
                      |                 |               |   
                      V                 V               V
                +-----------+     +-----------+     +--------+
                | Encrypted |     | Encrypted |     | Public |
  Database      | data      |     | key       |     | key    |
                +-----------+     +-----------+     +--------+


Because the symmetric keys are only stored in memory, the only kind of attack
that could lead to extraction of support statement data would require direct
memory access to the server process by the attacker.
The fallout of such attack might be mitigated by a limited lifetime of the
symmetric key (a limited number of uses of the same key). 

To strike some balance between performance and security, the memory lifetime
of an individual symmetric key was made adjustable. In the most secure setting,
the key might be used for only one record and immediately forgotten. This would
however lead to very long decryption times because the RSA decryption is by far
the slowest step in the decryption process. From practical tests it seems that
a reasonable value of reuses of one key is between 5 and 50. It leads to
decrease of decryption time by factor of 5 to 50 (which means that 10M records
could be decrypted in several hours on common PC) while not significantly 
worsening the security of stored data.

When the attacker gains access to the memory of the HTTP server process(es),
he will be only able to extract the data encrypted using the most recent
key(s). The maximum number of such records per server process is equal to the
lifetime (L) of the symmetric key. Because each server process has its own key
and the server typically spawns several processes (N), the maximum number of
records that the attacker could extract is equal to L*N with the typical value
being L*N/2 (when the remaining lifetime of key in individual processes is
evenly distributed between 0 and L).
In typical setup N would be ~10 (this value is adjustable in
the server configuration). With L=10, this means max ~100 (typically ~50)
existing records could be obtained. Let us discuss if this value is reasonable.

Provided the attacker has direct memory access, he would be also able to
observe incoming data into the system and, depending on the time window he has
before detection, could be able to collect significantly more data than what
he can decipher from the stored records. For a initiative that is just about to
reach the required goal of obtaining 1M support statements in 1 year, the
average speed of incomming statements is ~2 per minute, which means that the
same amount of data will be collected in 50 resp. 25 minutes. However, in such
a low-traffic case, it would not be necessary for the server to spawn 10 server
processes and the number of compromisable records would be significantly lower.
For a server with traffic high enough to lead to spawning of 10 server processes
the timeframe required to collect enough data would in reverse be much shorter.

Based on the above arguments, we would suggest that a value of L=10 is a
reasonable compromise between security and performance.


Security beyond Agora
=========================

Agora is only one application that will be running at your server and it is
equally important to secure other parts of your system as it is to secure your
Agora installation. The following paragraphs will deal with security measures
that are beyond Agora, but should be considered to improved security of the
whole system.

The scope of this document does not allow for detailed description of these
measures, but should be enough to point you in the right direction.

Access policy for user accounts
--------------------------------

The Agora web interface enforces relatively strong passwords on the online
user accounts. However, there must be some personnel with direct access to the
server running Agora with their user accounts.

It is important to protect these accounts to at least the same extent as the
online accounts. Because there is no need for such access to be open from the
internet and can be limited to a specified network, there are several extra
measures to consider:

- use SSH for remote logins
- enforce either strong passwords for SSH or preferentially certificate only
  authentication
- log user activity
- give access to the lowest number of people necessary
- limit SSH access using firewall or other tools to you own network
- detect, log and limit unsuccessful attempts to login
- limit physical access to the machine 


Penetration detection
-----------------------

As we described above, the data of statements of support are encrypted
immediatelly after being received by the server and never stored in plain text
form anywhere in the system. However, an attacker with root access to the
machine would be able to extract the data prior encryption in several ways
(directly from memory, by intercepting network traffic, by modifying the Agora
code, etc.).

Because of encryption, all already collected data should be safe from the
attacker (with the exception of a few last records, depending on the encryption
settings described above). However, attacker able to do long-term monitoring of
your system would be able to record all new arriving data. Therefore it is
crucial that any successful attack be **detected as soon as possible**. 

There are several solutions that enable monitoring of unexpected changes to the
server, which are usually one of the symptoms of a successful break-in. Among
these are integrity checkers, such as AIDE or Tripwire, log monitors, such as
logcheck, etc.

We strongly advise you to implement these security measures in order to minimize
fallout from a successful attack.


Backups
---------

One of the possible attacks on the Agora system, after a successful break-in
into the server, would be to modify or delete existing data. To mitigate this
threat, it is necessary to do periodic, preferentially off-site backups of the
Agora database.

