========================
Agora User documentation
========================

This document contains some information about how to run and manage the Agora
software once it is installed on your server.

It is intended for personnel who will take care of the daily business of
running the site.

---------------------------
Agora backstage
---------------------------

The Agora application has a backstage intended for logged-in users. This backstage
can be found at path /*agora_prefix*/b/ where *agora_prefix* is the (optional)
prefix you chose for your Agora installation.

Depending on the user account type and specifically assigned access right,
different things are available to the user in the backstage interface.

The possible features are as follows:

- **Your initiatives** - a list of initiative for which you are an owner.

- **Your account** - activities related to your account:

  - **Change your password**

- **Administration** - administrative actions:

  - **Create initiative** - allows creation of a new initiative using a wizard.

  - **Manage initiatives** - gives overview of all initiatives and allows
    advancing of the initiative status (depending on user permissions) and
    contains link to admin interface of individual intiatives (depending on user
    permissions).

  - **Administrative interface** - allows access to administrative interface
    (based on Django admin interface).



--------------------------
Administration interface
--------------------------

The administrative interface uses the standard Django admin interface to provide
users with access to some of the database objects. Any staff users (see below)
is allowed to access the admin interface, but access to individual objects is
specific to account type and assigned privileges.

Admin users can access and modify all objects registered with the admin
interface (content of support statements **is not** part of the admin
interface).

Staff users have no rights by default, but can be assigned specific rights by
an admin upon account creation or later on. (See `Special rights`_ for rights with
special importance in Agora).

Users can be parts of groups which allows easy assignment of sets of rights to a
specific group of users. In Agora a group called *agora_staff* is automatically
created upon installation. Any member of this group is allowed to create normal
users and modify some aspects of an initiative (see `User types and
permissions`_ below).


----------------------------
User types and permissions
----------------------------

There are several user types in Agora. If you have ever worked with Django, you
will find much described below familiar.

User types:

- **administrator** - This type of account is intended for managers of the the
  whole system. He can create new users and perform many changes from the
  administrative interface. He can also prematurely close active initiatives.

  The first administrator account is created during Agora installation. It is up
  to him to use his administrative interface to create new users, optionally
  also with administrator status.

  An administrator has all defined permissions implicitly.
    
- **staff user** - Can access the administrative interface and perform actions
  for which they were specifically assigned rights. For the default staff
  rights, add each staff member into a built-in group *agora_staff* which will
  allow them the following actions - create new initiatives, create new users of
  type 'normal' and activate new initiatives. *Note*: in the rest of this text,
  we will use the term *staff user* for a user that is both assigned the staff
  status and is part of the *agora_staff* group.

- **normal user** - This is a user with access to the backstage, but without
  any access to the administrative interface. He is not allowed any action by
  default, but can have specific rights assigned to it.
  
  This type of user is typically used for initiative owners as it provides a
  read-only access by default.

The table below summarizes specific rights of different types of user:

+------------+-----------+--------------+
| User type  | Admin     | Create users |
|            | interface |              |
+============+===========+==============+
| admin      | full      | all types    |
+------------+-----------+--------------+
| staff      | limited   | organizer    |
|            |           | only         |
+------------+-----------+--------------+
| normal     | no        | no           |
+------------+-----------+--------------+

The following table contains the permissions that a user has to initiatives:

+------------+------------+--------------+------------+-----------+
| User type  | Create ECI | Activate ECI | Modify ECI | Close ECI |
|            |            |              |            |           |
+============+============+==============+============+===========+
| admin      | yes        | yes          | yes        | yes       |
+------------+------------+--------------+------------+-----------+
| staff      | yes        | yes          | partially  | no        |
|            |            |              |            |           |
+------------+------------+--------------+------------+-----------+
| normal     | no         | no           | no         | no        |
+------------+------------+--------------+------------+-----------+

The staff users can only modify some attributes of an initiative after it has
been created, such as the localized names. Other attributes, such as the
code-name, start-date and end-date may be only modified by the admin.

The whole purpose of this separation of roles and privileges is to minimize
fallout when a staff user account or the staff user himself is compromised.

-----------------
Special rights
-----------------

There are some extra rights that can be added to individual staff users to allow
them to perform actions otherwise reserved for admins.

The rights can be added to the user by an admin in the administrative interface,
either when the account is created or by modification.

The right are:

Can create staff users
    Staff users with this permission can create new staff user account
    (otherwise they would only be allowed to create normal users).

Can close initiative
    User with this right can close an initiative before its end date.

Can advance initiative status
    User with this right can advance the status of an initiative according to
    the initiative life-cycle. He can however not lower the status (go
    back). One of the specific of this access right is the ability to close an
    initiative **after** it has reached its end date.

Can change initiative status
    User with this right can change the status of an initiative to any
    value. This right is intended for restricted use only as it allows undue
    manipulation with the initiative and can be used for example to deny users
    the ability to submit new statements of support.

Can change initiative dates
    User can change the start and end dates of an initiative. Access to this
    right should be restricted for the same reasons given above.

Can change initiative code name
    User can change the code name of an initiative. As the code name forms basis
    of the URL for an initiative, this right could be misused for denial of
    access and thus should be restricted.

Please note that there is some overlap between the rights that deal with
initiative status manipulation. This is by intention and allows finetuning of
inidividual user permissions to create a desired work-flow.

---------------------
Initiative ownership
---------------------

Initiative ownership is independent of user status - any registred user can be
an owner of an initiative. Also, an initiative may and usually does have more
than one owner (usually all members of the organizational comittee of an
initiative would have a member account associated with their initiative).

Iniative ownership is assigned to a user on initiative creation (it is one of
the steps in the process) or ex-post in the administrative interface. Both staff
and admin users are allowed to assign a user to an initiative, but only admin
can remove users from an initiative.

The only extra privilege an owner of an initiative has, is that he/she can view
the progress of support collection in the backstage. This overview is limited
only to total number of support statements obtained in individual countries and
does not allow display of individual statements of support.

-----------------------
Initiative lifecycle
-----------------------

In the following paragraphs, we will describe a typical lifecycle of an
initiative in the Agora application. We will not discuss how to create an
initiative and submit it to the EC - this is outside of the scope of this
document. 

Creation
=========

When an organizer has his initiative registred and prepared for online
collection of support, he could either install Agora or find a provider who
already runs a working copy of Agora. In the latter case, the organizer will
probably sign a contract with the provider specifying the details of the service
and of the initiative at hand.

The organizer is required to use the client software bundled with Agora to create
a private key and certificate for encryption of the support statements. He
should deliver the certificate to the provider and store the private key safely
in several copies for eventual decryption of the data.

When the provider has all the necessary details of the initiative and the
encryption certificate, he can create a user account for the organizer and
proceed to creating then the initiative in the Agora backstage. Both these actions
can be done by either a staff or admin user.


Activation
===========

When an initiative is created, it is not active - visitors cannot submit their
statements of support. Before an initiative is activated, the organizer should
use his backstage monitorring page to download a sample output encrypted with
the (by him) provided certificate. He **must** make sure that he can decrypt the
data using his private key. Only after he has done so, should the initiative be
allowed to be activated.

Activation of an initiative is done on the maintenance page which is part of the
backstage interface. The `Can advance the status of an initiative`_ permission
controls access to this function. Once the initiative is active, it collects
support statements until the desired end date is reached or it is deactivated
(closed). Data can only be extracted from a closed initiative and a closed
initiative cannot be reopened without special permissions. Initiative can be
closed by users with the "advance status" permission when it is past its end
date. Prior to that date either the "change status" or "close initiative" rights
are necessary.

Once the end date of an initiative is reached, it automatically starts refusing
new support submissions. In this phase, it can be deactivated (closed) by
designated users and the data may be exported.


Export of data
================

Export of data is done from a command line directly on the server, not via
HTTP. A command line script dump_results.py is used for this::

  python dump_results.py initiative

Where *intiative* is an unique code of the initiative. By default a file is
created with the same name as that of the initiative and an extension ".eci" is
appended. When desired, a different output filename may be given on the command
line using the '-o' option. The dump_results.py script reports the progress of
the extraction process.

When the export is complete, the obtained file should be passed to the
organizers. Because this file is fairly large for successful initiatives (a few
hundreds of megabytes for 1 million support statements), it is intentionally
left to the provider's staff to take care of this process. It usually depends on
the relationship and contract terms between the organizer and the provider - it
may be given over on a CD, DVD or USB flash disk, it may be offered for download
from a password protected website, etc.

*Note*: Please note that at this stage, all the data are encrypted and the only
one able to decrypt them is the owner of the right private key (the
organizer). Therefore, the handling of the file does not need to have any
special security (but the data should still be treated as confidential anyway).

Once the data is handed over to the organizer, it is up to him to decrypt it
(using the provided client software) and export it in either PDF or XML format
for submission to the corresponding authorities. The rest is beyond the scope of
this document.

**Warning**: This stage, when decrypted data is handled, is the one that is most
susceptible to possible data breaches. Even though it is not the responsibility
of the provider anymore, he is advised to provide the organizer with enough
information so that he takes this matter seriously and handles the data with
security in mind. Especially when distributing the data for individual countries
to the corresponding team members, special care has to be taken. The data should
in ideal case be encrypted, either by public key cryptography or using a
symmetric cipher, such as AES, with a predetermined good password. The
proceedure for doing so is beyond the scope of this document.
