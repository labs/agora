===========================
Agora project documentation
===========================

Agora is a project implementing server and client applications for electronic
collection of support statements for the European Citizens' Initiative (ECI).

This document contains basic overview of the Agora application. For installation
instructions see the INSTALL.rst document, for information about how to use and
maintain the application see USER-DOC.rst. For more in-depth technical
documentation for programmers and discussion of the security features, see
TECHNICAL-DOC.rst.


----------
Overview
----------

The Agora project consist of two parts - a server web application which allows
citizens to state their support of individual initiatives, and a client 
application which allows the organizers to process the collected support
statements.

The system was created to fulfill all requirements posed on such system by
the related EU regulations. This document describes how individual requirements
were implemented.


---------
Features
---------

- encryption of all sensitive user data
- support statement form localized to match requirements for each EU country
- conforms to EU regulations for European Citizens' Initiative
- security features beyond ECI regulations
- easy deployment


--------------------------
Privacy of submitted data
--------------------------

One of the most important concerns when dealing with support statement
collection is the privacy of submitted data. The Agora system deals with this
issue by using asymmetric encryption for protect the support statement data.

Before electronic collection of support for an initiative starts, the organizers
generate a pair of private and public keys (for this a client application is 
provided). The public key is then submitted to
the administrators of the Agora application and connected with the initiative at
hand. After the collection of supports starts, all support statements are
encrypted using this key. Therefore it is impossible for anybody else do decrypt
the data without having the private key. Because the private key is in the hands
of the organizers and is not used in the web application, the possibility of
data extraction even in case of the system being compromises is greatly
diminished.

After the collection of support for an initiative is closed, the collected data
are extracted by the administrators from the database and (still in encrypted
form) made available to the organizers. The organizers then use the client
application and a stored private key to decrypt the support statement data.
The decrypted data can then be used to generate list of support statements,
either in PDF of XML format. For this a client application is also provided.


