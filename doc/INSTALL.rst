=======================================
Agora server installation instructions
=======================================

This document describes installation of the Agora server. For basic information
about the Agora system, see README.rst.

The Agora server is based on Django (https://www.djangoproject.com/) and uses this
framework for implementation of many of its features, such as user
authentication, etc.

---------------
Prerequisites
---------------

Agora server was implemented with the intention of running on Linux OS. It might
be possible to run it on other platforms, but this is not documented and the
user needs to adapt this information for such purpose.

The following lists contain libraries that are necessary for the Agora server. The
first list contains libraries that are available as standard packages in Ubuntu
Linux with the name of corresponding package in brackets.

Concerning the connection between python and Apache, there are two
possibilities - mod_python and mod_wsgi. The latter it recommended as it is more
modern, has higher performace and is less demanding of system resources. Moreover,
as the application is developed and tested only under mod_wsgi,
if you choose mod_python, you may expect problems (which might be solved by editing
the configuration of urls). To sum up, use mod_wsgi to avoid problems.

* Apache 2 (apache2)
* mod-wsgi (libapache2-mod-wsgi) or mod-python (libapache2-mod-python)
* PostgreSQL (postgresql) - it is possible to use a different database supported
  by Django, such as MySQL or Oracle, but it was not tested and is not
  supported.
* Psycopg 2 (python-psycopg2)
* Python GeoIP bindings (python-geoip)
* Python OpenID library (python-openid)
* M2Crypto python library (python-m2crypto)
* Python setuptools (python-setuptools)

All the packages can be installed on Ubuntu using the following command as root::

  apt-get install apache2 libapache2-mod-wsgi postgresql python-psycopg2 \
  python-geoip python-openid python-m2crypto

or (in case of mod-python based installation)::

  apt-get install apache2 libapache2-mod-python postgresql python-psycopg2 \
  python-geoip python-openid python-m2crypto


The following libraries are optional and only required in specific setups:

* Python imaging library (python-imaging) - needed for django-simple-captcha
* Flite (flite) - needed for django-simple-captcha
* Memcached (memcached) - needed for Memcached cache backend
* Python memcache library (python-memcache) - needed for Memcached cache backend
    

The following libraries do not have packages in standard Ubuntu repositories
(or in case of Django the default version is too old) and should be installed
from python sources using either easy_install, pip or manually:

* Django 1.4 or newer (django)
* django-transmeta

All this can be installed using (as root)::

  easy_install django django-transmeta


And then either

* django-recaptcha

or 

* django-simple-captcha

See section `Selecting CAPTCHA library`_ for more info.


Local library modifications
============================

In case of some versions of Django, it is necessary to patch it because of a
known error that would crash Agora when trying to delete user accounts.

This is true for Django-1.4.1 and Django-1.4.2.

The patch is included in the agora ``doc/`` directory in file
``django-LogEntry-unicode.patch``. To apply it, do the following from the
command line::

  sudo patch /usr/local/lib/python2.7/dist-packages/Django-1.4.2-py2.7.egg/django/contrib/admin/models.py < django-LogEntry-unicode.patch

If the path to your Django installation differs, you have to adjust it.


--------------
Installation
--------------

Some steps of the installation procedure will be documented using a series of
commands on the command line. All such commands work with the assumption that the
source code of Agora was placed in a directory /var/www, that is there is
/var/www/agora where all the Agora code resides.


Template edit
==============

If you are using Django >=1.5 you can remove all lines

  {% load url from future %}

from all templates *.html
If this line is missing and you use Django<1.5, it will fail to parse url template tag correctly.


Creation of database
=====================

The Agora server requires a database for its work. You should create a database
and a user with read-write access to the database.


Agora settings file
====================

The directory /var/www/agora/server/agora contains a file called
setting_local.py.example. In order to set Agora server up, rename this file to
settings_local.py::

  cd /var/www/agora/server/agora
  cp setting_local.py.example settings_local.py

Edit the file settings_local.py using your favorite editor. The file contains
hints about the purpose of individual settings. Some of the settings are
discussed in more detail below.

Admins
-------

The ``ADMINS`` setting variable describes who is administrating the system and
should be informed about important things. Most importantly the email addresses
given in this setting will be used to send reports in case server errors occur,
so make sure these point to real people who will react to the reports.


Database
---------

One of the most important things is to change the ``NAME``, ``USER`` and
``PASSWORD`` fields in the ``DATABASE`` settings to the values you used when
creating the database.

If your database is running on a different machine, you can also enter the
``HOST`` field.


Secret key
-----------

Each Agora installation needs a unique ``SECRET_KEY`` value. This value is used
in password hashing, etc. A new random key can be generated on the command line
using the following command::

  head -c 36 /dev/urandom | base64

Allowed hosts
-------------

This applies to Django >=1.5 (for older Django version you can ignore/comment out
this setting)
This is a security measure. Set it to FQDN of the machine this application
is running on. Or you can
use "*" which denotes all domains or if the value starts with dot "."
it will match all subdomains as well.


Forensic log
-------------

Forensic log is used to log statements of support and detect potential
unauthorized changes in the database content. For this reason, the forensic log
should be protected from unauthorized modifications and should be writable only
by the HTTP server.

There is usually no need to change the default path to the forensic log, but if
you wish to do so, use the ``FORENSIC_LOG`` variable.

In any case, you must make sure that the file pointed to by ``FORENSIC_LOG`` is
writable by the user under which the server will be running. With the default
settings and Apache as HTTP server under Debian/Ubuntu, you can do the
following::

  sudo touch /var/log/agora_forensic.log
  sudo chown www-data /var/log/agora_forensic.log


CAPTCHA library
----------------

Agora supports two different CAPTCHA libraries. These are used to prevent
automated scripts from submitting statements of support.

You can choose between ``django-recaptcha`` which uses the external Google
ReCAPTCHA service or ``django-simple-captcha`` which uses the local system for
generation of CAPTCHAS.

The selection of CAPTCHA library is up to you. Both fullfill the criteria for
use with ECI. ReCAPTCHA is largely known and does not require local setup beyond
registering at Google and obtaining two keys. On the other hand, it has become
excesively complicated to solve it (warning: subjective opinion) and cannot be
tuned. Simple captcha requires some local database access, but it is handled
without user intervention. It also requires installation of specific program for
audio CAPTCHA generation (see
https://django-simple-captcha.readthedocs.org/en/latest/advanced.html#captcha-flite-path),
but is highly configurable and can be made much easier on the users (for tuning
information, see
https://django-simple-captcha.readthedocs.org/en/latest/advanced.html).

To select one of the libraries, set ``CAPTCHA_BACKEND`` to either
``django-recaptcha`` or ``django-simple-captcha`` and fill in the necessary
backend specific settings.


Caching
---------

Django supports several different caching backends for speeding up HTTP
responses. By default a in-memory cache is used, which does not require any
specific setup, but has several disadvantages.

If possible, setup Memcache and turn on Memcache based caching for production
environment. The following example sets up local memcached caching::

  CACHES = {
      'default': {
          'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
          'LOCATION': '127.0.0.1:11211',
      }
  }

For the above to work, you need to have ``memcached`` and ``python-memcached``
installed on the local machine.


Url base
---------

It is possible to have Agora run under a specific URL on your server. For this,
the ``URL_BASE`` variable is used. More about it is described in `Installation
under non-root URL`_.



Database initialization
========================

Once the settings for the database are ready, you can initialize the database
using::

  cd /var/www/agora/server
  python manage.py syncdb

You will be asked for the admin name, email and password. These values will be
used when accessing the Agora administrative interface.

When the database is initialized, it is necessary to import basic Agora related
data into the system, such as the list of EU countries, ID documents valid in
each country, data required from citizens from each country, etc. To do so, run
the following commands::

  cd /var/www/agora/server
  python load_country_data.py


Testing the setup sofar
=========================

At this stage, you can test that the database is running OK and the system is
set up properly. To do so, you can run a simple development server with the
Agora server from the command line::

  cd /var/www/agora/server
  python manage.py runserver 0.0.0.0:8000

(This will run the server on port 8000 and it will be accessible on any active
interface. You can change the IP address and/or port if you desire.)

If everything goes according to plan, you should not get any error and the
script should *not* return to the command line.

You can now point your browser to the address eci.server.addr:8000 and see a
page with Agora content. If you did not turn on ``DEBUG`` in
``settings_local.py``, there will be no CSS styling, but we will deal with this
later on when installing under a real HTTP server. 

If an Agora webpage is not displayed on the chosen address and port, something
did not work and you should retrace your steps. One of the possible reasons
might be some firewall on the way blocking access to the port 8000. In this
case, you can use for example an SSH tunnel for access.


Installing under Apache
========================

The test client described above is not safe and powerful enough and is provided
only for testing and development. For production use, you must setup a proper
HTTP server to serve Agora. This document describes installation under
the popular Apache 2 HTTP server.

To set up Apache to work properly with Agora, you need to adjust the
configuration of the virtual server that will be hosting the system.
The settings described bellow should appear somewhere in the configuration,
typically in one of the config files in */etc/apache2/sites-enabled* on Debian
systems.

Currently only installation using ``mod_wsgi`` is supported.

mod_wsgi
----------

As was described earlier in this document, there are two options for making
Agora run under Apache - mod_wsgi and mod_python. mod_wsgi is recommended and is
described here.

To enable serving of Agora using mod_wsgi, the following has to be added into
your config file::

    WSGIScriptAlias / /var/www/agora/server/agora/wsgi.py
    WSGIDaemonProcess agora processes=2 maximum-requests=500 python-path=/var/www/agora/server/
    WSGIProcessGroup agora

    Alias /static /var/www/agora/server/static
    
    <Directory /var/www/agora/server/agora/>
    <Files wsgi.py>
      Order deny,allow
      Allow from all
    </Files>
    </Directory>

and enable the wsgi mod::

  sudo a2enmod wsgi


Finalization
--------------

Regardless of the chosen Apache mod for Python, you also need to collect the
static files into one directory for easier access. To do so, use the following
command:: 

  cd /var/www/agora/server
  python manage.py collectstatic

Once you have added the above into your Apache configuration file and done the
static file collection, you can restart Apache and test if
http://agora.server.addr/ returns the same as the test server running on
port 8000.

**Note**: According to the ECI regulations, system for ECI support collection
must be run over HTTPS and all cookies must be set as secure. Unless in debug
mode (``DEBUG`` was set to ``True`` in ``settings_local.py``) Agora enforces
this in the settings and thus cookies seem not to work over normal HTTP.
You can manually override this by editing settings.py, but be warned that
running over plain HTTP would be against the ECI rules.

-------
Notes
-------

Security considerations
=========================

In order to be compatible with the EU regulations about ECI, your server
installation should have the following properties (this list is not
comprehensive and there is more you must do beyond the scope of this document):

* HTTPS - To prevent eavesdropping, all the communication between the client and
  the server should be encrypted. Therefore you should setup your website to use
  HTTPS and redirect all connections attempts over HTTP to HTTPS.
  HTTPS is also a requirement for a system for collection of ECI statements of
  support.

* Number of server processes - in order to minimize fallout from successful
  breach into the system, encryption keys are rotated after serveral uses. The
  amount of data that an attacker could extract is proportional to the number of
  HTTP server processes that are spawned. Therefore it is reasonable to limit
  this number to as small a number as possible. It is also desirable to set up a
  limit to the number of requests that each child process handles before it is
  restarted. This can ensure that independent of the Python garbage collector
  all user supplied data will be flushed from memory. See TECHNICAL-DOC.rst for
  more information.
  
* To minimize a chance of tampering with the support statement data after it was
  stored, you can modify the privileges on the corresponding database tables::
  
    ALTER DATABASE agora_db OWNER TO postgres;
    ALTER TABLE collector_supportstatement OWNER TO postgres;
    ALTER TABLE collector_encryptionkey OWNER TO postgres;
    ALTER TABLE collector_encryptioncipher OWNER TO postgres;

    REVOKE UPDATE,DELETE ON collector_supportstatement FROM agora_user, PUBLIC;
    REVOKE UPDATE,DELETE ON collector_encryptionkey FROM agora_user, PUBLIC;
    REVOKE UPDATE,DELETE ON collector_encryptioncipher FROM agora_user, PUBLIC;
    REVOKE GRANT OPTION FOR ALL PRIVILEGES ON DATABASE agora_db FROM agora_user, PUBLIC;

    GRANT SELECT,INSERT ON collector_supportstatement TO agora_user;
    GRANT SELECT,INSERT,REFERENCES ON collector_encryptionkey TO agora_user;
    GRANT SELECT,INSERT,REFERENCES ON collector_encryptioncipher TO agora_user;
    GRANT USAGE,SELECT,UPDATE ON collector_supportstatement_id_seq TO agora_user;
    GRANT USAGE,SELECT,UPDATE ON collector_encryptionkey_id_seq TO agora_user;
    GRANT USAGE,SELECT,UPDATE ON collector_encryptioncipher_id_seq TO agora_user;
  
  where agora_user is the name of the user you use to access the database and
  agora_db is the name of the database.

* Logging of administrator/organizer access, backup creation and changes and
  actualizations of database administrator is also a requirement of the ECI
  regulations. To support it, Agora implements syslog logging of user logins and
  logouts using the 'agora.audit' logger. You should set your syslog in a way
  that all messages pertaining to the above are logged into the same log
  file. How to do it is beyond the scope of this document.


Installation under non-root URL
=================================

The installation instructions in this document assumed that your Agora server will
be running under the root URL of your server, the is without a path component -
something like http://agora.server.addr/. However, it might be desirable to have
Agora running under a specific path, such as http://agora.server.addr/agora/. For
this, there are only minimal changes required in the setup.

1. Modify the WSGIScriptAlias setting to point to the specific URL, rather
   than the root. To ensure proper functioning of links when the user enters
   the top URL without the trailing slash, it is also necessary to create a
   corresponding redirect::

     WSGIScriptAlias /agora /var/www/agora/server/agora/wsgi.py
     RedirectMatch Permanent ^/agora$ /agora/

1. Modify STATIC_URL and LOGIN_URL in your 
   /var/www/agora/server/agora/settings_local.py to contain the top URL for
   Agora. Example::
     
     STATIC_URL = '/agora/static/'
     LOGIN_URL = '/agora/b/login/'

2. Modify the Alias line in the Apache configuration file to contain the
   prefix. For the example at hand it would be::
     
     Alias /agora/static /var/www/agora/server/static

   instead of::
   
     Alias /static /var/www/agora/server/static


