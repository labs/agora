# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

"""
Contains functionality related to RSA key pair creation
"""

# built-in modules
from __future__ import print_function
import time

# third party modules
from M2Crypto import EVP, X509, RSA, ASN1


class KeyPairGenerator(object):
    """
    Class implementing generation of an RSA key pair as well as related
    self-signed certificate
    """
    
    ISSUER_CN = 'Agora client self-signed certificate issuer'
    ISSUER_ORG = ''
    

    def _gen_callback(self, *args):
        pass


    def generate_key_pair(self, bits):
        pub_key = EVP.PKey()
        keys = RSA.gen_key(bits, 65537, callback=self._gen_callback)
        pub_key.assign_rsa(keys, capture=0)
        return keys, pub_key
        

    def create_request(self, pub_key, common_name="Agora initiative"):
        req = X509.Request()
        req.set_pubkey(pub_key)
        name = req.get_subject()
        #name.C = "UK"
        name.CN = common_name
        return req


    def generate_cert(self, pub_key, common_name="Agora initiative"):
        # create certificate request
        req = self.create_request(pub_key, common_name=common_name)
        # process the request
        sub = req.get_subject()
        cert = X509.X509()
        cert.set_serial_number(1)
        cert.set_version(2)
        cert.set_subject(sub)
        now = long(time.time()) + time.timezone
        start = ASN1.ASN1_UTCTIME()
        start.set_time(now)
        end = ASN1.ASN1_UTCTIME()
        end.set_time(now + 60 * 60 * 24 * 365 * 2) # two years
        cert.set_not_before(start)
        cert.set_not_after(end)
        issuer = X509.X509_Name()
        issuer.CN = self.ISSUER_CN
        issuer.O = self.ISSUER_ORG
        cert.set_issuer(issuer)
        cert.set_pubkey(pub_key)
        cert.sign(pub_key, 'sha1')
        return cert
    
if __name__ == "__main__":
    from optparse import OptionParser
    import sys
    opsr = OptionParser(usage="python %prog [options]")
    opsr.add_option("-v", "--verbose", action="store_true",
                    dest="verbose", default=False,
                    help="Log extra info about what the script is doing")
    opsr.add_option("-o", "--out-basename", action="store", type="string",
                    dest="outfilename", help="Base for the names of output "
                    "certificate and private key file. Two file with extension "
                    ".crt and .key will be created with the certificate and "
                    "private key respectively",
                    default="client_cert")
    (options, args) = opsr.parse_args()
    gen = KeyPairGenerator()
    priv_key, pub_key = gen.generate_key_pair(bits=2048)
    cert = gen.generate_cert(pub_key)
    # save the data
    basename = options.outfilename
    cert_fname = basename + ".crt" 
    if options.verbose:
        print("Creating certificate file: {0}".format(cert_fname),
              file=sys.stderr)
    cert.save_pem(cert_fname)
    key_fname = basename + ".key"
    if options.verbose:
        print("Creating private key file: {0}".format(key_fname),
              file=sys.stderr)
    priv_key.save_key(key_fname, cipher=None)