# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

"""
Contains code for generation of PDF output of statements of support for
one country from a database created by the 'decrypt_results.py' script
"""

# built-in modules
from __future__ import print_function, unicode_literals
import sys
import os
import json
import sqlite3
import logging
import re
from copy import copy
from xml.sax.saxutils import XMLGenerator
import zlib

# report lab imports
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import cm, mm
from reportlab.lib import colors
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

# pyPdf
from pyPdf import PdfFileReader, PdfFileWriter

# top level stuff
logger = logging.getLogger("agora.pdf")



class AbstractExporter(object):
    """
    Abstract class defining exporter interface
    """
    
    def __init__(self, db_filename, country):
        """
        db_filename is the name of the database file to read and use as input.
        country is the iso code of the country for which this data was
        collected.
        """
        self.db_filename = db_filename
        self.country = country
        self.id_type_remap = self._get_id_type_remap()
        
    def export_support_table(self, outfilename):
        raise NotImplementedError("This is abstract method")
    

    def _get_id_type_remap(self):
        connection = sqlite3.connect(self.db_filename)
        cursor = connection.cursor()
        result = {}
        cursor.execute("SELECT code, name FROM iddocs")
        for code, name in cursor:
            result[code] = name
        cursor.close()
        connection.close()
        return result


class XMLExporter(AbstractExporter):
    """
    Exports support statements from a database into a simple XML file.
    """
    
    def export_support_table(self, outfilename):
        with file(outfilename, "w") as outfile:
            writer = XMLGenerator(outfile, encoding="utf-8")
            writer.startElement("agora", {"country": self.country})
            writer.characters("\n")
            for date, data in self._gen_data_rows():
                writer.startElement("record", {"date": date})
                for part in data:
                    name = part['name']
                    value = part['value']
                    if name == "id_type":
                        value = self.id_type_remap.get(value, value)
                    src = part.get('src')
                    attrs = {} if not src else {"src": src}
                    writer.startElement(name, attrs)
                    writer.characters(value)
                    writer.endElement(name)
                writer.endElement("record")
                writer.characters("\n")
            writer.endElement("agora")
       
    
    def _gen_data_rows(self):
        connection = sqlite3.connect(self.db_filename)
        cursor = connection.cursor()
        cursor.execute("SELECT date, data FROM records ORDER BY "
                       "family_name,given_name")
        for date, json_data in cursor:
            data_list = json.loads(zlib.decompress(json_data))
            yield date, data_list
        cursor.close()    
        

class ReportLabExporter(AbstractExporter):
    """
    Exports support statements from a database into a PDF file (or a series
    of them).
    """
    
    # list of countries which do and do not require id-documents
    ID_DOC_COUNTRIES = ['BG','CZ','GR','ES','FR','IT','CY','LV','LT','LU','HU',
                        'MT','AT','PL','PT','RO','SI','SE']
    NON_ID_DOC_COUNTRIES = ['BE','DK','DE','EE','IE','NL','SK','FI','GB']
    
    # mapping between countries and languages (en is default)
    COUNTRY_TO_LANG = {"CZ": "cs"}
    
    # column names
    COL_NAMES = {'given_name': {"en": "full first names",
                                "cs": "jméno/jména",
                                },
                 'family_name': {"en": "family names",
                                 "cs": "příjmení",
                                 },
                 'address': {"en": "permanent residence",
                             "cs": "bydliště",
                             },
                 'dpob': {"en": "date and place of birth",
                          "cs": "datum a místo narození",
                          },
                 'citizenship': {"en": "nationality",
                                 "cs": "státní příslušnost",
                                 },
                 'id_number': {"en": "personal identification number / "
                                     "identification document type and number",
                               "cs": "osobní identifikační číslo / "
                                     "typ a číslo dokladu",
                               },
                 'date': {"en": "date",
                          "cs": "datum",
                          }
                 }
    
    # columns that could contain long data and should be treated as paragraphs
    PARAGRAPH_COLS = set(["address", "id_number"])
    
    # columns in table for each country,
    # depending on if they require id-document
    ID_DOC_TABLE = ['given_name','family_name','address','dpob','citizenship',
                    'id_number','date']
    
    NON_ID_DOC_TABLE = ['given_name','family_name','address','dpob',
                        'citizenship','date']
    
        
    # column composition
    # each column in the resulting table may possibly be composed of data from
    # several different fields in the support record
    # the following code allows a list of field names, a formatting string or a
    # callable to be supplied for each column which will describe how it will be
    # populated from the record data
    COL_CONTENT = {
        "given_name": "{given_name}",
        "family_name": "_f_family_name", # a name of method
        "address": ["address_street", "address_street_num", "address_zip",
                    "address_city", "address_country"],
        "dpob": ["date_of_birth","place_of_birth"],
        "citizenship": "{citizenship}",
        "id_number": "_f_id_number", # a method
        "date": "{date}"
    }
    
    # STYLES
    BASE_FONT_SIZE = 8 
    styles = getSampleStyleSheet()
    styles['Normal'].fontName = 'FreeSerif'
    styles['Normal'].spaceBefore = 0
    styles['Normal'].spaceAfter = 0
    styles.byAlias['Header'] = copy(styles['Normal'])
    styles['Normal'].fontSize = BASE_FONT_SIZE
    styles['Header'].fontSize = BASE_FONT_SIZE - 1
    
    # some internal stuff
    _TABLE_SIZE_TRIGGER = 1000
    _DOCUMENT_SIZE_TRIGGER = 10000

    def __init__(self, db_filename, country):
        AbstractExporter.__init__(self, db_filename, country)
        pdfmetrics.registerFont(TTFont('FreeSerif',
                                    os.path.join("contrib", "FreeSerif.ttf")))
        pdfmetrics.registerFontFamily('FreeSerif', normal='FreeSerif')
        self.lang = self.COUNTRY_TO_LANG.get(self.country, "en")
        

    @classmethod
    def _format_from_field_list(cls, fields, data):
        values = []
        for field in fields:
            value = data.get(field)
            if value:
                values.append(value)
        return ", ".join(values)


    def _format_cell_content(self, col_name, data):
        desc = self.COL_CONTENT.get(col_name)
        if not desc:
            raise Exception("Unknown column {0}".format(col_name))
        # check for types matching a method
        if type(desc) in (str, unicode) and desc.startswith("_f_") \
            and hasattr(self, desc):
            desc = getattr(self, desc)
        # now decide how to use the desc and return a corresponding value
        if callable(desc):
            try:
                return desc(data)
            except Exception as exc:
                logger.error("Error outputting data for column '%s': %s",
                             col_name, exc)
                return ""
        elif type(desc) is list:
            try:
                return self._format_from_field_list(desc, data)
            except Exception as exc:
                logger.error("Error outputting data for column '%s': %s",
                             col_name, exc)
                return ""
        else:
            try:
                return desc.format(**data)
            except KeyError as exc:
                logger.warn("Missing value error: %s", exc)
                return ""
        

    def _get_column_names(self):
        if self.country in self.ID_DOC_COUNTRIES:
            return self.ID_DOC_TABLE
        elif self.country in self.NON_ID_DOC_COUNTRIES:
            return self.NON_ID_DOC_TABLE
        else:
            raise Exception("Unsupported country %s" % self.country)


    def export_support_table(self, outfilename):
        """
        Exports the content of self.db_filename into pdf file outfilename.
        """
        def _filename(counter):
            return "{0}-{1:04d}{2}".format(outfile_base, counter, outfile_ext)
        
        commands = [('FONT',(0,0),(-1,-1),'FreeSerif'),
                    ('SIZE',(0,0),(-1,-1), self.BASE_FONT_SIZE),
                    ('INNERGRID',(0,0),(-1,-1), 0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,0), 0.5, colors.black),
                    ('LINEBELOW', (0,-1), (-1,-1), 0.5, colors.black),
                    ('TOPPADDING',(0,0),(-1,-1), 1),
                    ('BOTTOMPADDING',(0,0),(-1,-1), 0),
                    ('VALIGN',(0,0),(-1,0),"MIDDLE"),
                    #('ALIGN',(0,0),(0,-1),"RIGHT"),
                    ]
        # the code itself
        outfile_base, outfile_ext = os.path.splitext(outfilename)
        # the holder of all information
        story = []
        columns = self._get_column_names()
        # header
        header = [Paragraph(self.COL_NAMES[col][self.lang].upper(),
                            self.styles['Header'])
                  for col in columns]
        table_rows = [header]
        # individual records
        # the following code writes intermediate PDF files for each 
        # self._DOCUMENT_SIZE_TRIGGER records
        # because otherwise the memory and processing time goes too high
        # the thing is also sped-up by creating a separate table each
        # self._TABLE_SIZE_TRIGGER because reportlab does not have to check
        # so many rows when laying out the table
        pdf_file_count = 0
        col_lengths = {}
        for i, record in enumerate(self._gen_data_rows()):
            row = []
            for column in columns:
                content = self._format_cell_content(column, record)
                col_lengths[column] = col_lengths.get(column, 0) + len(content)
                if content and column in self.PARAGRAPH_COLS:
                    # do not convert to Paragraph if it is not necessary
                    content = Paragraph(content, self.styles['Normal'])
                row.append(content)
            table_rows.append(row)
            # if there is enough rows, put the table into the story
            if i and i % self._TABLE_SIZE_TRIGGER == 0:
                col_widths = self._compute_column_widths(columns, col_lengths)
                table = Table(table_rows, colWidths=col_widths,
                              repeatRows=1, style=TableStyle(commands))
                story.append(table)
                table_rows = [header]
                col_lengths = {}
                # if there are enough rows for one document, save the data
                # and open a new story
                if i % self._DOCUMENT_SIZE_TRIGGER == 0:
                    pdf_file_count += 1
                    self._write_pdf_file(_filename(pdf_file_count), story)
                    story = []
        # output all data that were accumulated after last save
        if table_rows:
            col_widths = self._compute_column_widths(columns, col_lengths)
            table = Table(table_rows, colWidths=col_widths,
                          repeatRows=1, style=TableStyle(commands))
            story.append(table)
            pdf_file_count += 1
            self._write_pdf_file(_filename(pdf_file_count), story)
        # here we concatenate all the necessary pdfs into one
        logger.debug("Merging %d files into %s", pdf_file_count, outfilename)
        to_merge = []
        for i in range(1, pdf_file_count+1):
            to_merge.append(_filename(i))
        self._merge_pdfs(outfilename, to_merge)
        # remove intermediate files
        logger.debug("Removing %d temporary files", pdf_file_count)
        for filename in to_merge:
            os.remove(filename)

      
    def _compute_column_widths(self, columns, col_width_sums):
        total_length = sum(col_width_sums.values())
        threshold = 0.05 * total_length
        lengths = []
        remaining_width = 277.0
        for col_name in columns:
            length = col_width_sums.get(col_name, 0)
            if length < threshold:
                lengths.append(20*mm)
                remaining_width -= 20
                total_length -= length
            else:
                lengths.append(None)
        dx = remaining_width / total_length
        for i, col_name in enumerate(columns):
            if lengths[i] is None:
                length = col_width_sums.get(col_name, 0) * dx
                remaining_width -= length
                lengths[i] = length * mm
        return lengths 
                
        
        
    def _write_pdf_file(self, outfilename, flowables):
        logger.debug("Writing to %s", outfilename)
        doc = SimpleDocTemplate(outfilename,
                                pagesize=(defaultPageSize[1],
                                          defaultPageSize[0]),
                                leftMargin=1*cm,
                                rightMargin=1*cm,
                                topMargin=1*cm,
                                bottomMargin=1*cm)
        doc.build(flowables)


    def _merge_pdfs(self, outfilename, filenames):
        out_pdf = PdfFileWriter()
        fobjs = []
        for filename in filenames:
            infile = file(filename, "rb")
            fobjs.append(infile)
            reader = PdfFileReader(infile)
            for page in xrange(reader.getNumPages()):
                out_pdf.addPage(reader.getPage(page))
        with file(outfilename, 'wb') as outfile:
            out_pdf.write(outfile)
        for fobj in fobjs:
            fobj.close()


    def _gen_data_rows(self):
        connection = sqlite3.connect(self.db_filename)
        cursor = connection.cursor()
        cursor.execute("SELECT date, data FROM records ORDER BY "
                       "family_name,given_name")
        for date, json_data in cursor:
            data_list = json.loads(zlib.decompress(json_data))
            data = {}
            for rec in data_list:
                name = rec['name']
                value = rec['value']
                if name == 'id_type':
                    # id_type has to be remapped (if possible)
                    value = self.id_type_remap.get(value, value)
                data[name] = value
            data['date'] = date
            yield data
        cursor.close()
        connection.close()
        

    # functions for formatting output fields
    def _f_family_name(self, data):
        bracket = []
        if "name_of_father" in data:
            bracket.append(data.get("name_of_father"))
        if "birth_name" in data:
            bracket.append(data.get("birth_name"))
        if bracket:
            return "{0} ({1})".format(data.get('family_name'), "".join(bracket))
        else:
            return "{family_name}".format(**data)

    def _f_id_number(self, data):
        if "id_origin" in data:
            return "{id_type} - {id_number} ({id_origin})".format(**data)
        else:
            return "{id_type} - {id_number}".format(**data)
        
            
if __name__ == '__main__':
    import resource
    # parse options
    from optparse import OptionParser
    opsr = OptionParser(usage="python %prog [options] result_database")
    opsr.add_option("-v", "--verbose", action="store_true",
                    dest="verbose", default=False,
                    help="Log extra info about what the script is doing")
    opsr.add_option("-c", "--country", action="store", type="string",
                    dest="country", help="Country code of the country for "
                    "which this data was collected. "
                    "(Default is to deduce it from the input filename).",
                    default="")
    opsr.add_option("-f", "--format", action="store", type="string",
                    dest="format", help="Format into which statements of "
                    "support should be exported - 'pdf' and 'xml' are "
                    "available",
                    default="pdf")
    opsr.add_option("-o", "--out-filename", action="store", type="string",
                    dest="outfilename", help="Filename to use for PDF output. "
                    "(Default is the input filename with extension set to "
                    ".pdf).",
                    default="")
    (options, args) = opsr.parse_args()
    # check args
    if len(args) != 1:
        print("This script takes exactly one argument\n", file=sys.stderr)
        opsr.print_usage()
        sys.exit()
    else:
        input_file = args[0]
    # verbosity    
    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    # country
    if options.country:
        country = options.country
    else:
        basename = os.path.basename(input_file)
        match = re.match(".*-([A-Z]{2}).edb", basename)
        if match:
            country = match.group(1)
            logging.info("Deduced country code '%s' from input file name",
                         country)
        else:
            logging.error("Could not deduce country code from input file name. "
                          "You can provide it by hand using the -c option.")
            opsr.print_usage()
            sys.exit()
    # format
    if options.format == "pdf":
        exp = ReportLabExporter(input_file, country)
        file_ext = ".pdf"
    elif options.format == "xml":
        exp = XMLExporter(input_file, country)
        file_ext = ".xml"
    else:
        logging.error("Unsupported output format '%s'. Choose one of "
                      "'pdf','xml'.")
        opsr.print_usage()
        sys.exit()
    # output
    if options.outfilename:
        out_file = options.outfilename
        logging.debug("Exporting to file '%s'", out_file)
    else:
        out_file = os.path.splitext(os.path.basename(input_file))[0] + file_ext
        logging.info("Exporting to file '%s'", out_file)
    exp.export_support_table(out_file)
    mem_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1000
    logger.debug("%d MB memory used", mem_usage)
