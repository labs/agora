# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

"""
This module enables decryption of data produced by the Agora server
using private key of the initiator or the initiative at hand
"""

# built-in modules
from __future__ import print_function
import csv
from base64 import b64decode, b64encode
import json
import gzip
import zlib
import logging
import sqlite3
import hashlib
from time import time
from multiprocessing import Process, Queue, cpu_count

# third party modules
import M2Crypto

# top-level stuff
logger = logging.getLogger("agora.decrypt")


class EncryptedBundle(object):
    
    def __init__(self, enc_key):
        self.enc_key = enc_key
        self.records = []
        

class AgoraDumpReader(object):

    def __init__(self, bundle_queue, decrypt_proc_count=1):
        self.bundle_queue = bundle_queue
        self.decrypt_proc_count = decrypt_proc_count
        
    def read_file_obj_header(self, fileobj):
        """
        Reads lines from the fileobj until it finds the first one with
        type of 0 or 1 corresponding to encrypted initiative data
        """
        reader = csv.reader(fileobj)
        rec_num = 0
        last_pos = fileobj.tell()
        header = {}
        for row in reader:
            rec_num += 1
            rec_type = row[0]
            if rec_type in "01":
                # these are encrypted data
                break
            elif rec_type == "2":
                # this is the total count of records in this file
                header['total_key_num'] = int(row[1])
                header['total_ss_num'] = int(row[2])
            elif rec_type == "3":
                # this is mapping between id-document ids and names
                header['iddoc_names'] = json.loads(b64decode(row[1]))
            else:
                logger.warn("Unknown record type '%s'", rec_type)
            last_pos = fileobj.tell()
        fileobj.seek(last_pos)
        return header
    
    def read_file_obj(self, fileobj):
        """
        Reads input file, creates bundles of data encrypted with one key
        from it and passes it into the queue for processing.
        """
        reader = csv.reader(fileobj)
        bundle = None
        rec_num = 0
        for row in reader:
            rec_num += 1
            rec_type = row[0]
            if rec_type == "0":
                # this is the encrypted symmetrical key
                if bundle:
                    self.bundle_queue.put(bundle)
                bundle = EncryptedBundle(row[1])
            elif rec_type == "1":
                # this is one data record encrypted using the most recent
                # symmetrical key
                if not bundle:
                    raise Exception("No key was found at the top of the file. "
                                    "It is probably corrupted.")
                bundle.records.append([rec_num]+row[1:])
            elif rec_type in "23":
                # these are header rows - ignore them
                logger.warn("Header record inside data records '%s'", rec_type)
            else:
                logger.warn("Unknown record type '%s'", rec_type)
        self.bundle_queue.put(bundle)
        logger.debug("Reader: end of input")
        for i in range(self.decrypt_proc_count):
            # send a None for each decrypting process
            self.bundle_queue.put(None)
            self.bundle_queue.put(None)


class AgoraDecryptorThread(Process):
    
    def __init__(self, private_key_file, bundle_queue, decrypted_queue):
        super(AgoraDecryptorThread, self).__init__()
        self.private_key = M2Crypto.RSA.load_key(private_key_file)
        self.bundle_queue = bundle_queue
        self.decrypted_queue = decrypted_queue
        self.end = False
        
    def run(self):
        """
        Decrypts data from a queue and calls callback for each row.
        Callback takes 3 parameters (country_code, date, data) where data
        is a string containing json serialization of the data of support
        statement in the country at hand.
        """
        while not self.end:
            bundle = self.bundle_queue.get()
            if not bundle:
                break
            key = self._decrypt_sym_key64(bundle.enc_key)
            for rec_num, ccode, date, cipher_name, enc_data64 in bundle.records:
                try:
                    zlib_data = self._decrypt_data64(key, cipher_name,
                                                     enc_data64)
                    data = zlib_data
                except Exception as exc:
                    logger.error("Error decrypting record #%d (%s)", rec_num,
                                 exc)
                self.decrypted_queue.put((ccode, date, data))
        logger.debug("Reader: decryptor data end")
        self.decrypted_queue.put(None)

    
    def _decrypt_sym_key64(self, enc_key64):
        sym_key = self.private_key.private_decrypt(b64decode(enc_key64),
                                                M2Crypto.RSA.pkcs1_oaep_padding)
        return sym_key
    
    def _decrypt_data64(self, key, cipher, enc_data64):
        enc_text = b64decode(enc_data64)
        iv = enc_text[0:16]
        enc_text = enc_text[16:]
        k = M2Crypto.EVP.Cipher(alg=cipher, key=key, iv=iv, op=0)
        dec_text = k.update(enc_text)
        dec_text += k.final()
        return dec_text


class SQLiteDumperThread(Process):
    """
    This class takes care of dumping the output from AgoraDecryptorThread and
    storing the data into SQLite3 databases according to country from which
    they come
    """
    
    HASHER = hashlib.sha1
    COMMIT_THRESHOLD = 5000
    
    def __init__(self, basename, data_queue, header=None, decrypt_proc_count=1):
        """
        basename is the name from which to derive the names of individual db
        file names; data_queue is a queue from which the data to save into the
        db will come; header is a dict with values comming from the header
        of the encrypted file;
        decrypt_proc_count is the number of decrypting processes. This is
        important to know because each process sends 'None' into the data_queue
        and this process should only finish when it received one from each
        process
        """
        super(SQLiteDumperThread, self).__init__()
        self.basename = basename
        self._dbs = {}
        self._rec_counters = {}
        self.processed_rec_count = 0
        self._last_time = time()
        self.data_queue = data_queue
        self.end = False
        self.decrypt_proc_count = decrypt_proc_count
        self._finished_decryptors = 0
        self.header = header or {}
        
    def run(self):
        def log_progress():
            if total_rec_num:
                logger.info("Current record count %d / %d (%.1f %%)",
                            self.processed_rec_count, total_rec_num,
                            100.0*self.processed_rec_count/total_rec_num)
            else:
                logger.info("Current record count %d",
                            self.processed_rec_count)
                
        # get total rec number from the header when available
        total_rec_num = self.header.get("total_ss_num", None)
        # process data arriving in the data_queue until its all done
        while not self.end:
            rec = self.data_queue.get()
            if rec:
                ccode, date, data = rec
            else:
                self._finished_decryptors += 1
                logger.debug("Dumper: %d of %d decryptors finished",
                             self._finished_decryptors, self.decrypt_proc_count)
                if self._finished_decryptors == self.decrypt_proc_count:
                    # all decryptors finished
                    break
                else:
                    # wait for other decryptors
                    continue
            self._rec_counters[ccode] = self._rec_counters.get(ccode, 0) + 1
            self.processed_rec_count += 1
            #logger.debug("Processing record #%d for country %s (%s)",
            #             self._rec_counters[ccode], ccode, date)
            connection = self._dbs.get(ccode)
            if not connection:
                connection = self._create_db(ccode)
            cursor = connection.cursor()
            data_hash = self._hash_data(data)
            # lets extract data that we need outside
            given_name = ""
            family_name = ""
            for rec in json.loads(zlib.decompress(data)):
                if rec.get('name') == 'given_name':
                    given_name = rec.get('value')
                elif rec.get('name') == 'family_name':
                    family_name = rec.get('value')
            # lets put it into the database   
            cursor.execute("""INSERT OR IGNORE INTO records 
                (date,data,hash,given_name,family_name) 
                VALUES (?,?,?,?,?)""",
                (date, buffer(data), data_hash, given_name, family_name))
            if self._rec_counters[ccode] % self.COMMIT_THRESHOLD == 0:
                logger.debug("Committing db for country %s", ccode)
                connection.commit()
            if time() - self._last_time > 2:
                log_progress()
                self._last_time = time()
            cursor.close()
        log_progress()
        logger.debug("Reader: dumper data end")
        if total_rec_num is not None and \
            self.processed_rec_count != total_rec_num:
            logger.error("The number of processed records differs from the one "
                         "stored in file header: %d (header) vs %s (actual)",
                         total_rec_num, self.processed_rec_count)
        self.finish()

        
    def finish(self):
        for connection in self._dbs.values():
            connection.commit()
            connection.close()

    def _construct_db_name(self, ccode):
        return "{0}-{1}.edb".format(self.basename, ccode)
    
    def _create_db(self, ccode):
        db_name = self._construct_db_name(ccode)
        logger.info("Creating database '%s' for country '%s'", db_name, ccode)
        connection = sqlite3.connect(db_name)
        cursor = connection.cursor()
        # drop tables if they already exist
        for tb_name in ("records", "iddocs"):
            try:
                cursor.execute("DROP TABLE %s;" % tb_name)
            except:
                pass
        # table into which the support statements will be stored
        cursor.execute("""CREATE TABLE records (
          hash TEXT PRIMARY KEY,
          date TEXT,
          given_name TEXT,
          family_name TEXT,
          data BLOB);""")
        # table which will be populated from the header
        cursor.execute("""CREATE TABLE iddocs (
          code TEXT PRIMARY KEY,
          name TEXT);""")
        connection.commit()
        cursor.close()
        # do header processing - this is done here to ensure exactly one
        # execution of this code for a connection
        self._store_header_data(connection)
        self._dbs[ccode] = connection
        return connection

    def _store_header_data(self, connection):
        """
        Takes values from the header (self.header) and where appropriate
        stores it into the database behind 'connection'
        """
        cursor = connection.cursor()
        id_docs = self.header.get("iddoc_names", {})
        for code, name in id_docs.iteritems():
            cursor.execute("INSERT INTO iddocs (code,name) VALUES (?,?)",
                           (code, name))
        connection.commit()
        cursor.close()

    @classmethod
    def _hash_data(cls, data):
        hasher = cls.HASHER()
        hasher.update(data)
        return b64encode(hasher.digest())


if __name__ == "__main__":
    import os
    import sys
    from optparse import OptionParser
    # parse options
    # number of processes
    try:
        cpu_count = cpu_count()
    except NotImplementedError:
        cpu_count = 1
    opsr = OptionParser(usage="python %prog [options] private_key_file "
                        "agora_dump_file")
    opsr.add_option("-v", "--verbose", action="store_true",
                    dest="verbose", default=False,
                    help="Log extra info about what the script is doing")
    opsr.add_option("-n", "--processes", action="store", type="int",
                    dest="processes", default=cpu_count,
                    help="Number of decryption processes to start (defaults "
                    "to the number of CPUs).")
    opsr.add_option("-o", "--out-filename", action="store", type="string",
                    dest="outfilename", help="Base for the names of output "
                    "database files. '-XX.edb' is always appended to the name "
                    "where XX is the country code. "
                    "(Default is the input filename stripped of extension).",
                    default="")
    (options, args) = opsr.parse_args()
    # check args
    if len(args) != 2:
        print("This script takes exactly two arguments\n", file=sys.stderr)
        opsr.print_usage()
        sys.exit()
    else:
        key_file = args[0]
        input_file = args[1]
    # output
    if options.outfilename:
        outname_base = options.outfilename
    else:
        outname_base = os.path.splitext(os.path.basename(input_file))[0]
    # verbosity    
    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    # number of decrypt processes
    decrypt_proc_count = options.processes
    logger.info("Starting %d decryption processes", decrypt_proc_count)
    bundle_queue = Queue(maxsize=30*decrypt_proc_count)
    decrypted_queue = Queue(maxsize=1000*decrypt_proc_count)
    try:
        decrypt_threads = [AgoraDecryptorThread(key_file, bundle_queue,
                                              decrypted_queue)
                           for i in range(decrypt_proc_count)]
    except M2Crypto.RSA.RSAError:
        print("Could not load private key - either the file is "
              "corrupted or does not contain valid private key.",
              file=sys.stderr)
        sys.exit()
    reader = AgoraDumpReader(bundle_queue, decrypt_proc_count=decrypt_proc_count)
    # try to open input file
    try:
        infile = gzip.open(input_file, "r")
    except IOError as exc:
        print("Cannot read input file - ", exc, file=sys.stderr)
        sys.exit()
    # try to read the header
    try:
        header = reader.read_file_obj_header(infile)
    except Exception as exc:
        print("Problem reading file header - ", exc, file=sys.stderr)
        sys.exit()
    # now that all went well, start the decryption and db-dumping threads
    [decrypt_thread.start() for decrypt_thread in decrypt_threads]
    dump_thread = SQLiteDumperThread(outname_base, decrypted_queue,
                                     header=header,
                                     decrypt_proc_count=decrypt_proc_count)
    dump_thread.start()
    try:
        reader.read_file_obj(infile)
    except (KeyboardInterrupt, Exception) as exc:
        logger.error("Caught exception '%s', exiting", exc.__class__.__name__)
        for decrypt_thread in decrypt_threads:
            decrypt_thread.end = True
            decrypt_thread.terminate()
            decrypt_thread.join()
        dump_thread.end = True
        dump_thread.terminate()
        dump_thread.join()
        while not bundle_queue.empty():
            bundle_queue.get(block=False)
        while not decrypted_queue.empty():
            decrypted_queue.get(block=False)
    else:
        for decrypt_thread in decrypt_threads:
            decrypt_thread.join()
        dump_thread.join()
    finally:
        infile.close()

