About
===========

Files in the 'contrib' directory were obtained from external sources and were
neither created nor changed by CZ.NIC, z.s.p.o.

This file contains information about individual files/directories in the contrib
directory.


FreeSerif.ttf
----------------

This file is part of the 'Free UCS Outline Fonts' (aka the GNU FreeFont) font
package. It is covered by the GNU GPLv3 license with a 'font exception' which
means that documents embedding this font are not required to be covered by the
GPL license.
As 'eci' is release under the same GNU GPLv3 license, we distribute this font as
part of the source package to make installation easier and ensure uniformity
of output documents.
