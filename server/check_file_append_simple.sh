#!/bin/sh

if [ $# -lt 2 ] ; then
    echo "Give me two file names"
    exit 10
elif [ ! -f $1 ] ; then 
    echo "File not found $1"
    exit 11
elif [ ! -f $2 ] ; then 
    echo "File not found $2"
    exit 11
fi

OLD_FILE=$1;
NEW_FILE=$2;

OLD_SIZE=`stat -c %s ${OLD_FILE}`;
OLD_SUM=`cat ${OLD_FILE} | sha1sum`;

NEW_SLICE_SUM=`head -c ${OLD_SIZE} ${NEW_FILE} | sha1sum`;

if [ "${OLD_SUM}" = "${NEW_SLICE_SUM}" ]; then
    echo "OK";
    exit 0
else
    echo "$1 is not a subset of $2";
    exit 1
fi


