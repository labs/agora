# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in modules
import json
import logging
import datetime
import os
import sys
sys.path.append("./agora")
os.environ['DJANGO_SETTINGS_MODULE'] = 'agora.settings'

# local imports
from agora.collector import models

# initialization
logging.basicConfig(level=logging.DEBUG)


def load_initiative_json(filename):
    logging.debug("Loading file %s", filename)
    with file(filename, "r") as infile:
        data = json.load(infile)
    # required data
    code_name = data['code_name']
    title = data['title']
    titles = {}
    # localized names
    for lang, int_title in title.items():
        titles["title_"+lang] = int_title
    certificate = data['certificate']
    # optional data
    start_date = data.get('start_date')
    if not start_date:
        start_date = datetime.datetime.now()
    else:
        start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S")
    end_date = data.get('end_date')
    if not end_date:
        end_date = start_date + datetime.timedelta(years=1)
    else:
        end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d %H:%M:%S")
    # status with 1 (new) as default
    status = data.get('status', models.Initiative.STATUS_NEW)
    # countries - default is all EU
    countries = data.get('countries')
    if not countries:
        countries = [x['iso_code'] 
                     for x in models.Country.objects.all().values('iso_code')]
    # website
    website = data.get("website", "")
    ec_number = data.get("ec_number", "")
    ec_website = data.get("ec_website", "")
    providers = data.get("providers", {})
    # put it into the database
    # initiative
    existing = models.Initiative.objects.filter(code_name=code_name)
    if existing:
        logging.info("Deleting existing instance of initiative")
        for obj in existing:
            obj.delete()
    initiative = models.Initiative(code_name=code_name, ec_number=ec_number,
                                   ec_website=ec_website,
                                   start_date=start_date, end_date=end_date,
                                   status=status, website=website, **titles)
    initiative.save()
    logging.debug("Created initiative #%d", initiative.pk)
    # certificate
    cert = models.Certificate(initiative=initiative, certificate=certificate)
    cert.save()
    logging.debug("Added certificate to initiative #%d", initiative.pk)
    # countries
    for ccode in countries:
        logging.debug("Connecting %s to initiative #%d", ccode, initiative.pk)
        country = models.Country.objects.get(iso_code=ccode)
        ics = models.InitiativeCountrySettings(country=country,
                                               initiative=initiative)
        ics.save()
        country_providers = providers.get(ccode, [])
        for cprov in country_providers:
            code_name = cprov['code_name']
            provider = models.UserDataProvider.objects.get(code_name=code_name)
            if cprov['required']:
                logging.debug("Adding provider %s to country %s",
                              code_name, ccode)
                ics.required_providers.add(provider)
            else:
                ics.optional_providers.add(provider) 
        ics.save()

    
    
if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print "Give a JSON file with initiative"
        sys.exit()
    load_initiative_json(sys.argv[1])
