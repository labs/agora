# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in modules
import urllib2 as u2
import urllib
import re
import cookielib
import time
import random
import string
import logging
import csv
import calendar


logging.basicConfig(level=logging.DEBUG)


def random_gen(alphabet, min_len, max_len):
    while True:
        length = random.randint(min_len, max_len)
        yield ("".join([random.choice(alphabet)
                        for _i in range(length)])).capitalize()
                       
def random_date():
    while True:
        year = random.randint(1900, 2010)
        month = random.randint(1, 12)
        day = random.randint(1, calendar.monthrange(year, month)[1])
        yield "{0}-{1}-{2}".format(year, month, day)
        

class Exception302(Exception):
    pass


class NonRedirectingHTTPRedirectHandler(u2.HTTPRedirectHandler):
    def http_error_302(self, req, fp, code, msg, headers):
        raise Exception302("302")


class TrafficSimulator(object):

    full_alphabet = "".join([chr(i) for i in range(0, 128)])

    gen_params = {
        "given_name": (string.ascii_lowercase, 3, 15),
        "family_name": (string.ascii_lowercase, 3, 15),
        "birth_name": (string.ascii_lowercase, 3, 15),
        "name_of_father": (string.ascii_lowercase, 3, 15),
        "address_country": (string.ascii_lowercase, 8, 15),
        "address_city": (string.ascii_lowercase, 6, 15),
        "address_street": (string.ascii_lowercase, 10, 25),
        "address_street_num": (string.digits, 1, 5),
        "address_zip": (string.digits, 5, 5),
        "date_of_birth": random_date,
        "place_of_birth": (string.ascii_lowercase, 10, 25),
        "id_number": (string.digits, 8, 16),
        "id_origin": (string.ascii_lowercase, 6, 20),
    }

    def __init__(self, urlbase, initiative):
        self.urlbase = urlbase
        self.initiative = initiative
        self.initiative_url = "{0}i/{1}/".format(self.urlbase, self.initiative)
        self.country_fields = self.get_country_fields()
        self.generators = self.create_generators()
        self.countries = [country for country in self.country_fields.keys()
                          if country not in ("CZ", "LU")]
        self.stop = False

    def get_country_fields(self, data_file = "data/required_fields.csv"):
        data_file = "data/required_fields.csv"
        logging.debug("Loading data file %s", data_file)
        country_to_fields = {}
        with open(data_file, 'r') as infile:
            reader = csv.reader(infile)
            header = reader.next()[2:]
            ident_fields = []
            for col in header:
                ident_fields.append(col)
            for raw_row in reader:
                row = [x.decode('utf-8') for x in raw_row]
                ccode = row[0]
                country_to_fields[ccode] = []
                for i, col in enumerate(row[2:]):
                    if col:
                        country_to_fields[ccode].append(ident_fields[i])
        return country_to_fields
    
    def create_generators(self):
        fields = set()
        gens = {}
        for cfields in self.country_fields.values():
            for cfield in cfields:
                fields.add(cfield)
        for field in fields:
            if field in self.gen_params:
                params = self.gen_params[field]
                if callable(params):
                    gens[field] = params()
                elif type(params) is tuple:
                    gens[field] = random_gen(*self.gen_params[field])
        return gens
    
    def random_country(self):
        return random.choice(self.countries)
    
    def gen_posting(self, ccode):
        res = {}
        for field in self.country_fields[ccode]:
            if field in self.generators:
                res[field] = self.generators[field].next()
            elif field == "citizenship":
                res[field] = ccode
            elif field == "id_type":
                res[field] = ccode+"01"
        res['recaptcha_response_field'] = 'PASSED'
        return res
    
    def post_submissions(self, number=1, verbose=False):
        t = time.time()
        i = 0
        cookies = cookielib.CookieJar()

        url = self.initiative_url + "DK" + "/"
        logging.debug("Working with url %s", url)
        opener = u2.build_opener(u2.HTTPCookieProcessor(cookies))
        http = opener.open(url)
        html = http.read()
        match = re.search("'csrfmiddlewaretoken' value='([0-9A-z]+)'", html)
        if match:
            csrfmiddlewaretoken = match.group(1)
        else:
            raise Exception("csrfmiddlewaretoken not found")
        http.close()  
        
        # new opener which does not follow redirection
        opener = u2.build_opener(u2.HTTPCookieProcessor(cookies),
                                 NonRedirectingHTTPRedirectHandler)
        while i < number and not self.stop:
            ccode = self.random_country()
            temp = self.gen_posting(ccode)
            url = self.initiative_url + ccode + "/"
            temp['csrfmiddlewaretoken'] = csrfmiddlewaretoken
            try:
                req = u2.Request(url, headers={"Referer": url})
                http = opener.open(req, urllib.urlencode(temp))
            except Exception302:
                # we ignore 302 - it is here to speed things up as we are
                # not interested in the redirected page, just in posting
                pass
            else:
                # if we got here, there was no redirection and so we did not
                # succeed in posting the data
                print http.read()
                http.close() 
                raise Exception("Posting of data was not successful.")
            i += 1
            if verbose and i % 10 == 0:
                print i
    
        total = time.time() - t
        print "Submissions: %d; Time: %.1fs; %.1f req/s" % (i, total, i/total)


if __name__ == "__main__":
    from threading import Thread
    from optparse import OptionParser
    # parse options
    opsr = OptionParser(usage="python %prog [options]")
    opsr.add_option("-v", "--verbose", action="store_true",
                    dest="verbose", default=False,
                    help="Log extra info about what the script is doing")
    opsr.add_option("-i", "--initiative", action="store", type="string",
                    dest="initiative", help="Code name of the initiative",
                    default="carrots")
    opsr.add_option("-u", "--url", action="store", type="string",
                    dest="url", help="Url of the initiative for which the " 
                    "traffic should be generated",
                    default="http://localhost:8000/")
    opsr.add_option("-n", "--num-of-records", action="store", type="int",
                    dest="num_of_records", default=1,
                    help="How many records should be submitted *in each thread*"
                    )
    opsr.add_option("-t", "--threads", action="store", type="int",
                    dest="thread_count", default=1,
                    help="How many threads should the tester spawn "
                    "(Note: the number_of_runs if for each thread!)")
    (options, args) = opsr.parse_args()

    print "%d thread(s) generating %d submission(s) (each) to '%s' "\
        "initiative '%s'" % (options.thread_count, options.num_of_records,
                             options.url, options.initiative)
    if options.thread_count == 1:
        sim = TrafficSimulator(initiative=options.initiative,
                               urlbase=options.url)
        sim.post_submissions(number=options.num_of_records,
                             verbose=options.verbose)
    else:
        def get_runner(simulator, number, verbose):
            def run():
                simulator.post_submissions(number=number, verbose=verbose)
            return run
        threads = []
        for i in range(options.thread_count):
            if options.verbose:
                print "starting thread #%d" % i
            sim = TrafficSimulator(initiative=options.initiative,
                                   urlbase=options.url)
            runner = get_runner(sim, options.num_of_records, options.verbose)
            t = Thread(target=runner)
            t.simulator = sim
            t.start()
            threads.append(t)
            
        while True:
            for t in threads:
                if t.is_alive():
                    break # this breaks out of for
            else:
                break # this breaks out of while (if there was no break in for)
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                print "Caught keyboard interrupt, closing down"
                for t in threads:
                    t.simulator.stop = True

        