# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

from __future__ import print_function

# built-in modules
import logging
import os
import sys
import hashlib
sys.path.append("./agora")
os.environ['DJANGO_SETTINGS_MODULE'] = 'agora.settings'

# local imports
from agora.collector import models


class ForensicChecker(object):
    
    CHUNK_SIZE = 1000

    def __init__(self):
        self.ids_not_in_log = set()
        self.seen_ids = set()
        
    def hash_it(self, salt, rec):
        hasher = hashlib.sha256()
        date, content, ccode, _init_name, key = rec
        hasher.update(salt)
        hasher.update(content)
        hasher.update(ccode)
        hasher.update(str(date))
        hasher.update(key)
        return hasher.hexdigest()

    def check_records(self, records, first_error=False):
        ids = [int(x[1]) for x in records]
        min_id = min(ids)
        max_id = max(ids)
        in_db = models.SupportStatement.objects.\
            filter(pk__gte=min_id, pk__lte=max_id).\
            values_list("pk", "date", "content", "country__iso_code",
                        "initiative__code_name",
                        "encryption_key__encrypted_key")
        id_to_db_rec = dict([(int(x[0]), x[1:]) for x in in_db])
        error = 0
        for record in records:
            log_init_code, pk, salt, log_hash = record
            pk = int(pk)
            # check if we do not have an id that we have marked as not seen
            # in log before
            if pk in self.seen_ids:
                logging.error("Duplicate record id in forensic log #%d", pk)
                error = 1
                if first_error:
                    break
            else:
                self.seen_ids.add(pk)
            if pk in self.ids_not_in_log:
                self.ids_not_in_log.discard(pk)
            # check the record against the database
            db_rec = id_to_db_rec.get(pk)
            if db_rec:
                comp_hash = self.hash_it(salt, db_rec)
                db_init_code = db_rec[3]
                if comp_hash != log_hash:
                    logging.error("Record #%d was modified!", pk)
                    error = 2
                    if first_error:
                        break
                elif db_init_code != log_init_code:
                    logging.error("Initiative for record #%d was changed from "
                                  "'%s' to '%s'!",
                                  pk, log_init_code, db_init_code)
                    error = 3
                    if first_error:
                        break
                del id_to_db_rec[pk]
            else:
                logging.error("Record #%d is missing from database", pk)
                error = 4
                if first_error:
                    break
        for rec_id in id_to_db_rec:
            if rec_id not in self.seen_ids:
                self.ids_not_in_log.add(rec_id)
        return error
    
    def check_log(self, filename, first_error=False):
        result = 0
        with file(filename, 'r') as logfile:
            chunk = []
            for line in logfile:
                parts = line.strip().split(",")
                chunk.append(parts)
                if len(chunk) >= self.CHUNK_SIZE:
                    sub_res = self.check_records(chunk, first_error=first_error)
                    result = result or sub_res
                    chunk = []
                    if result != 0 and first_error:
                        return result
            sub_res = self.check_records(chunk, first_error=first_error)
            result = result or sub_res
            if result != 0 and first_error:
                return result
        for rec_id in sorted(self.ids_not_in_log):
            logging.error("Record not in log #%d", rec_id)
            return 5
        return result

if __name__ == "__main__":
    from optparse import OptionParser
    # parse options
    opsr = OptionParser(usage="python %prog [options]")
    opsr.add_option("-l", "--log", action="store", type="string",
                    dest="log_file", default="",
                    help="Manually specify the filename of the forensic log")
    opsr.add_option("-v", "--verbose", action="store_true",
                    dest="verbose", default=False,
                    help="Log extra info about what the script is doing")
    opsr.add_option("-q", "--quiet", action="store_true",
                    dest="quiet", default=False,
                    help="Be quiet - no output will be produced")
    opsr.add_option("-f", "--first-error", action="store_true",
                    dest="first_error", default=False,
                    help="Exit on first error - useful for quick checks")
    (options, args) = opsr.parse_args()
    
    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
    elif options.quiet:
        logging.basicConfig(level=logging.CRITICAL)
    else:
        logging.basicConfig(level=logging.INFO)
    # check input
    if len(args) != 0:
        print("This script takes no arguments\n", file=sys.stderr)
        opsr.print_usage()
        sys.exit(10)
    # where is the log file
    if options.log_file:
        logfilename = options.log_file
    else:
        from django.conf import settings
        logfilename = settings.FORENSIC_LOG
    logging.info("Checking against forensic log file '%s'", logfilename)
    checker = ForensicChecker()
    try:
        result = checker.check_log(logfilename, first_error=options.first_error)
    except Exception as exc:
        logging.error("Error when processing log: %s", exc)
        result = 20
    sys.exit(result)
    
    