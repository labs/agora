# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in modules
from __future__ import print_function
import csv
import logging
import gzip
import os
from time import time
from base64 import b64encode
import json
os.environ['DJANGO_SETTINGS_MODULE'] = 'agora.settings'

# local imports
from agora.collector import models

# initialization
logging.basicConfig(level=logging.DEBUG)
audit_logger = logging.getLogger("agora.audit") 

def dump_initiative(code_name, filename):
    try:
        initiative = models.Initiative.objects.get(code_name=code_name)
    except models.Initiative.DoesNotExist:
        logging.error("No such initiative: %s", code_name)
        return 1
    if initiative.status != initiative.STATUS_CLOSED:
        # refuse dump of initiative that is not closed
        logging.error("Initiative '%s' is not closed, cannot dump data",
                      code_name)
        audit_logger.error("Attempt to export not closed initiative '%s' by "
                           "user UID=%d", code_name, os.getuid())
        return 2
    with gzip.open(filename, "w", compresslevel=6) as outfile:
        audit_logger.info("Export of closed initiative '%s' to file '%s' "
                           "by user UID=%d", code_name, filename, os.getuid())
        dumper = csv.writer(outfile)
        key_count = initiative.certificate.encryptionkey_set.all().count()
        ss_count = initiative.supportstatement_set.all().count()
        # dump the stats
        # key and record count
        dumper.writerow((2, key_count, ss_count))
        # dump id-document id->name mapping
        id_docs = {}
        for cifvv in models.CountryIdentFieldValidValue.objects.filter(
                            country_ident_field__ident_field__slug="id_type"):
            id_docs[cifvv.code] = cifvv.value
        dumper.writerow((3, b64encode(json.dumps(id_docs))))
        i = 0
        last_time = time()
        for key in initiative.certificate.encryptionkey_set.all().iterator():
            dumper.writerow((0,key.encrypted_key))
            for ccode, date, cipher, content in key.supportstatement_set.all().\
                        values_list("country__iso_code", "date",
                                    "encryption_cipher__name",
                                    "content"):
                dumper.writerow((1, ccode, date, cipher, content))
            i += 1
            if time() - last_time > 2:
                print("{0:.1f} %".format(100.0*i / key_count),
                      file=sys.stderr)
                last_time = time()
    return 0

if __name__ == "__main__":
    from optparse import OptionParser
    # parse options
    opsr = OptionParser(usage="python %prog [options] initiative")
    opsr.add_option("-v", "--verbose", action="store_true",
                    dest="verbose", default=False,
                    help="Log extra info about what the script is doing")
    opsr.add_option("-o", "--out-filename", action="store", type="string",
                    dest="outfilename", help="Name of file to use for output "
                    "(default is 'initiatives name'.agora).",
                    default="")
    (options, args) = opsr.parse_args()

    import sys
    if len(args) != 1:
        print("This script takes exactly one argument\n", file=sys.stderr)
        opsr.print_usage()
        sys.exit()
    initiative = args[0]
    outfilename = options.outfilename or (initiative+".agora") 
    result = dump_initiative(initiative, outfilename)
    sys.exit(result)