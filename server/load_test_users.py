# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in modules
import json
import logging
import os
import sys
sys.path.append("./agora")
os.environ['DJANGO_SETTINGS_MODULE'] = 'agora.settings'

# local imports
from agora.certs.models import ClientCertificate
from agora.collector.models import Initiative, InitiativeOwner
from django.contrib.auth.models import User

# initialization
logging.basicConfig(level=logging.DEBUG)


def load_users_json(filename):
    with file(filename, "r") as infile:
        data = json.load(infile)
    for user in data:
        login = user['login']
        cert_fp = user.get("cert_fp")
        try:
            user_obj = User.objects.get(username=login)
        except User.DoesNotExist:
            raise
        if cert_fp:
            try:
                cert_obj = user_obj.clientcertificate_set.get(cert_fp=cert_fp)
            except ClientCertificate.DoesNotExist:
                cert_obj = ClientCertificate(user=user_obj, cert_fp=cert_fp)
                cert_obj.save()
                logging.debug("Adding certificate with fp '%s' to user '%s'",
                              login, cert_fp)
            else:
                pass
        initiatives = user.get("initiatives", [])
        for initiative in initiatives:
            init_obj = Initiative.objects.get(code_name=initiative)
            _x, new = InitiativeOwner.objects.get_or_create(user=user_obj,
                                                            initiative=init_obj)
            if new:
                logging.debug("Adding initiative '%s' to user '%s'", initiative,
                              login)
    
    
if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print "Give a JSON file with users"
        sys.exit()
    load_users_json(sys.argv[1])
