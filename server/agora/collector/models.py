# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in imports
from datetime import date

# django imports
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

# third party imports
from transmeta import TransMeta


# ---------------- Initiative independent data ---------------------------

class Country(models.Model):
    __metaclass__ = TransMeta

    iso_code = models.SlugField(verbose_name=_("ISO code"), max_length=2,
                                unique=True)
    name = models.CharField(verbose_name=_("Country name"), max_length=127)
    native_name = models.CharField(verbose_name=_("Native name"), max_length=127)
    user_data_providers = models.ManyToManyField('UserDataProvider',
                                        verbose_name=_("User data providers"))
    ident_fields = models.ManyToManyField('IdentField',
                                        verbose_name=_("Identification fields"),
                                        through='CountryIdentField')
    support_limit = models.PositiveIntegerField(default=0,
        verbose_name=_("Support limit"),
        help_text=_('Number of citizens from country who must support the '
        'initiative for it to be accepted.'))
    languages = models.ManyToManyField('Language', verbose_name=_("Languages"),
                                       through='CountryLanguage')
    
    class Meta:
        translate = ('name',)
        ordering = ['native_name']
        verbose_name = _("Country")
        verbose_name_plural = _("Countries")

    def __unicode__(self):
        return self.name
    
    
class UserDataProvider(models.Model):
    __metaclass__ = TransMeta
    
    code_name = models.SlugField(max_length=32, unique=True)
    name = models.CharField(verbose_name=_("Name"), max_length=64)
    info = models.TextField(verbose_name=_("Info"), blank=True)
    replaces_captcha = models.BooleanField(verbose_name=_("Replaces captcha"),
                                           default=False)
    
    class Meta:
        translate = ('name','info')
        verbose_name = _("User data provider")
        verbose_name_plural = _("User data providers")
    
    def __unicode__(self):
        return self.name
    
    
class IdentField(models.Model):
    __metaclass__ = TransMeta
    
    TYPE_INT = 1
    TYPE_TEXT = 2
    TYPE_ENUM = 3
    TYPE_DATE = 4

    TYPE_CHOICES =  ((TYPE_INT, 'int'),
                     (TYPE_TEXT, 'text'),
                     (TYPE_ENUM, 'enum'),
                     (TYPE_DATE, 'date'),
                     )
    
    TYPE_NAME_TO_ID = dict([(v,k) for k,v in TYPE_CHOICES])
    FIELD_MAPPING = dict(TYPE_CHOICES)
    
    slug = models.SlugField(max_length=32, unique=True)
    name = models.CharField(verbose_name=_("Name"), max_length=64)
    data_type = models.PositiveSmallIntegerField(verbose_name=_("Data type"),
                                                 choices=TYPE_CHOICES)
    
    class Meta:
        translate = ('name',)
        verbose_name = _("Identification field")
        verbose_name_plural = _("Identification fields")
        
    def __unicode__(self):
        return self.name


class CountryIdentField(models.Model):
    
    country = models.ForeignKey(Country)
    ident_field = models.ForeignKey(IdentField)
    position = models.PositiveSmallIntegerField()
    
    class Meta:
        unique_together = ("country", "ident_field", "position")


class CountryIdentFieldValidValue(models.Model):
    
    country_ident_field = models.ForeignKey(CountryIdentField)
    position = models.PositiveSmallIntegerField()
    # the code bellow is what will be stored in the submission record
    code = models.SlugField(max_length=16, unique=True)
    value = models.CharField(max_length=255)

    class Meta:
        unique_together = ("country_ident_field", "position")


class Language(models.Model):
    
    name = models.CharField(verbose_name=_("Name"), max_length=127)
    iso_code = models.CharField(verbose_name=_("ISO code"), max_length=3)
    
    class Meta:
        verbose_name = _("Language")
        verbose_name_plural = _("Languages")


class CountryLanguage(models.Model):
    
    country = models.ForeignKey(Country)
    language = models.ForeignKey(Language)
    percentage = models.PositiveSmallIntegerField()
    

# ------------- Initiative and related models ----------------
    
class Initiative(models.Model):
    __metaclass__ = TransMeta
    
    STATUS_NEW = 1
    STATUS_PREREG = 2
    STATUS_ACTIVE = 3
    STATUS_CLOSED = 4
    
    CHOICE_STATUS = (
        (STATUS_NEW, _('New')),
        (STATUS_PREREG, _('Preregistration')),
        (STATUS_ACTIVE, _('Collecting support')),
        (STATUS_CLOSED, _('Closed')),
    )
    
    code_name = models.SlugField(verbose_name=_("Code name"), unique=True,
                                 max_length=30)
    title = models.CharField(verbose_name=_("Title"), max_length=127)
    website = models.URLField(verbose_name=_("Website"))
    ec_number = models.CharField(max_length=64, blank=True,
                            verbose_name=_("Commission registration number"))
    ec_website = models.URLField(verbose_name=_("Web address in Commission "
                                                "register"), blank=True)
    start_date = models.DateField(verbose_name=_("Start date"))
    end_date = models.DateField(verbose_name=_("End date"))
    status = models.PositiveSmallIntegerField(verbose_name=_("Status"),
                                              default=STATUS_NEW,
                                              choices=CHOICE_STATUS)
    
    class Meta:
        translate = ('title',)
        verbose_name = _("Initiative")
        verbose_name_plural = _("Initiatives")
        permissions = (
             ("close_initiative", _("Can prematurely close initiative")),
             # may change status to anything he wants, even to closed
             ("change_initiative_status", _("Can change initiative status")),
             # may only raise the status, not lower it
             ("advance_initiative_status", _("Can advance initiative status")),
             ("change_initiative_dates", _("Can change initiative dates")),
             ("change_initiative_code_name",
                                        _("Can change initiative code name")),
        )

    
    def __unicode__(self):
        return self.title
    
    def within_date_bounds(self):
        return self.start_date <= date.today() <= self.end_date
    
    def is_collecting(self):
        return self.status == self.STATUS_ACTIVE and self.within_date_bounds()
    
   
    
class InitiativeCountrySettings(models.Model):
    """
    required_providers - a set of providers through which the user must
        provide his/her data; if any exists, the user cannot enter data by hand
    optional_providers - a set of providers that the user may use to provide
        data; is ignored when any required_providers are set up; the user
        can enter data by hand bypassing any optional providers
    """
    initiative = models.ForeignKey(Initiative, verbose_name=_("Initiative"))
    country = models.ForeignKey(Country, verbose_name=_("Country"))
    required_providers = models.ManyToManyField(UserDataProvider,
                        verbose_name=_("Required providers"),
                        related_name="requiredbyinitiativecountrysettings_set")
    # the following is not implemented yet
    #optional_providers = models.ManyToManyField(UserDataProvider,
    #                verbose_name=_("Optional providers"),                         
    #                related_name="optionalforinitiativecountrysettings_set",
    #                help_text="Only used when no required_providers are set")

    class Meta:
        ordering = ['initiative', 'country__native_name']
        verbose_name = _("Initiative country settings")
        verbose_name_plural = _("Initiative country settings")
        
    def __unicode__(self):
        return u"{0} - {1}".format(self.initiative, self.country)
    

class InitiativeOwner(models.Model):

    initiative = models.ForeignKey(Initiative, verbose_name=_("Initiative"))
    user = models.ForeignKey(User, verbose_name=_("User"))
    
    def __unicode__(self):
        return "{0} - {1}".format(self.user, self.initiative) 
    
    class Meta:
        verbose_name = _("Initiative owner")
        verbose_name_plural = _("Initiative owners") 
    

class InitiativeContentFile(models.Model):
    
    initiative = models.ForeignKey(Initiative)
    language = models.ForeignKey(Language)
    content_file = models.FileField(upload_to="pdf")


class Certificate(models.Model):

    initiative = models.OneToOneField(Initiative, verbose_name=_("Initiative"))
    certificate = models.TextField(verbose_name=_("Certificate"),
                    help_text=_("Certificate whose public key will be used to "
                                "encrypt data of support statements."))
    # the following could speed loading key into memory, but lets not 
    # optimize too early
    #public_key = models.TextField()
        
    class Meta:
        verbose_name = _("Encryption certificate")
        verbose_name_plural = _("Encryption certificates") 
        
    def __unicode__(self):
        return _("Certificate for '%(initiative)s'") % dict(
                                                    initiative=self.initiative)
    

class EncryptionKey(models.Model):
    
    pub_key = models.ForeignKey(Certificate)
    encrypted_key = models.TextField()


class EncryptionCipher(models.Model):
    
    name = models.CharField(max_length=16, unique=True)

# -------------- Support statements -----------------

class SupportStatement(models.Model):
    """
    Here we store statements of support from individual citizens
    """
    initiative = models.ForeignKey(Initiative)
    content = models.TextField(help_text="Encrypted identification of citizen")
    encryption_key = models.ForeignKey(EncryptionKey)
    encryption_cipher = models.ForeignKey(EncryptionCipher)
    country = models.ForeignKey(Country)
    language = models.CharField(max_length=2, help_text=_("Language iso code"))
    date = models.DateField(auto_now=True)
    
            
    class Meta:
        verbose_name = _("Statement of support")
        verbose_name_plural = _("Statements of support") 


# -------------- Preregistration support ----------------

class Preregistration(models.Model):
    """
    A record that a user with specific email address is interested in being
    notified once an Initiative is opened to support statement submission.
    """
    initiative = models.ForeignKey(Initiative)
    email = models.EmailField()
    language = models.CharField(max_length=2, help_text=_("Language iso code"))
    
    class Meta:
        unique_together = ("initiative", "email")
    