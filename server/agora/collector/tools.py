from django.http import Http404
from django.core.urlresolvers import resolve, get_script_prefix

def resolve_with_prefix(url):
    """
    built-in Django resolve does not seem to take script_prefix into
    account. This function should take care of it
    """
    prefix = get_script_prefix()
    if prefix and prefix.endswith("/"):
        prefix = prefix[:-1]
    if not url.startswith(prefix):
        raise Http404
    test_url = url[len(prefix):]
    return resolve(test_url)