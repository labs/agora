# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

from django.conf.urls.defaults import patterns, url, include
from django.conf import settings
import views

urlpatterns = patterns('',
    # HTML stuff:
    (r'^$', views.homepage),
    (r'^about/$', views.about),
    (r'^test_cookie/$', views.test_cookie),
    (r'^i/(?P<code_name>\w+)/$', views.initiative),
    (r'^i/(?P<code_name>\w+)/prereg_stored/$', views.preregistration_processed),
    (r'^i/(?P<code_name>\w+)/(?P<ccode>\w{2})/$', views.statement_submission),
    (r'^i/(?P<code_name>\w+)/(?P<ccode>\w{2})/provider/(?P<provider_id>\w+)/$',
        views.statement_submission_provider),
    (r'^i/(?P<code_name>\w+)/(?P<ccode>\w{2})/thanks/$',
        views.statement_submission_processed),
    (r'^i/(?P<code_name>\w+)/(?P<ccode>\w{2})/error/$',
        views.statement_submission_error),
    
    #(r'^crash/$', views.crash),                   
            
)

if settings.CAPTCHA_BACKEND == "django-simple-captcha":
    urlpatterns += patterns('',
        url(r'^captcha/', include('captcha.urls')),
    ) 
