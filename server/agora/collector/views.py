# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in modules
import logging
import json
from base64 import b64encode
import zlib
import datetime
import hashlib
import traceback
import sys

# third party imports
import GeoIP
import M2Crypto

# Django imports
from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect, HttpResponseForbidden
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.views.decorators.cache import cache_page, cache_control
from django.utils.translation import ugettext_lazy as _
from django.template import TemplateDoesNotExist, loader

# models and forms
import models
from forms import SupportSubmissionForm, PreregistrationForm

# data providers
from agora import mojeid
import agora.dummy_provider as dummy

# local imports
from encryption import Encryptor
from tools import resolve_with_prefix

# static top-level stuff
logger = logging.getLogger("agora.collector.views")
#
# forensic logger
#
# it is created outside the Django logging support, because we do not want it
# to be started unless views are imported - otherwise we might have problems
# with permissions because management commands are usually run under different
# use than the http server
forensic_logger = logging.getLogger("agora.collector.forensic")
forensic_logger.setLevel(logging.INFO)
forensic_logger.propagate = False
if settings.FORENSIC_LOG:
    forensic_handler = logging.FileHandler(settings.FORENSIC_LOG)
else:
    forensic_handler = logging.NullHandler()
forensic_handler.setLevel(logging.INFO)
forensic_logger.addHandler(forensic_handler)

geo_ip = GeoIP.new(GeoIP.GEOIP_MEMORY_CACHE)
ENCRYPTORS = {}
ENC_KEYS = {}

# custom decorators
no_go_back = cache_control(no_cache=True, must_revalidate=True, private=True,
                           max_age=0, no_store=True)

#@cache_page(60 * 10)
def homepage(req):
    """
    Homepage of the whole Agora project.
    """
    cache_key = "homepage_active_initiatives"
    initiatives = cache.get(cache_key)
    if initiatives is None:
        today = datetime.date.today()
        allowed_states = (models.Initiative.STATUS_ACTIVE,
                          models.Initiative.STATUS_PREREG)
        initiatives = models.Initiative.objects.filter(status__in=allowed_states,
                                                       start_date__lte=today,
                                                       end_date__gte=today)
        cache.set(cache_key, initiatives)
    
    return render(req, 'homepage.html', {'initiatives': initiatives})

#@cache_page(60 * 60)
def about(req):
    """
    About page - information about the system.
    """
    return render(req, 'about.html')


def initiative(req, code_name):
    """
    The entry page of an initiative.
    """
    # get initiative
    initiative, response = _get_initiative(code_name, check_active=False)
    if response:
        return response
    # defaults
    countries = []
    countries_codes = []
    native_country = None
    prereg_form = None
    geoip_ccode = None
    prereg_save_error = False
    # check the status of the initiative and decide what to do
    if initiative.status == initiative.STATUS_PREREG:
        # preregistration is active, we should prepare a form
        if req.method == "POST":
            prereg_form = PreregistrationForm(data=req.POST)
            if prereg_form.is_valid():
                # preregister the user
                email = prereg_form.cleaned_data.get("email")
                if not models.Preregistration.objects.filter(
                            initiative=initiative, email=email).exists():
                    record = models.Preregistration(initiative=initiative,
                                                    email=email,
                                                    language=req.LANGUAGE_CODE)
                    try:
                        record.save()
                    except:
                        prereg_save_error = True
                if not prereg_save_error:
                    return HttpResponseRedirect(
                                reverse(preregistration_processed,
                                        args=[initiative.code_name]))
        else:
            prereg_form = PreregistrationForm()
    elif initiative.status == initiative.STATUS_ACTIVE:
        geoip_ccode = geo_ip.country_code_by_addr(req.META['REMOTE_ADDR']) or ""
        if settings.DEBUG:
            if req.META['REMOTE_ADDR'].startswith("172.20.20.") or \
                req.META['REMOTE_ADDR'].startswith("127.0.0."):
                geoip_ccode = "CZ"
        countries = []
        native_country = None
        icss_cache_key = "icss__{0}".format(code_name)
        icss = cache.get(icss_cache_key)
        if icss is None:
            icss = initiative.initiativecountrysettings_set.\
                                                select_related("country").all()
            cache.set(icss_cache_key, icss)
        
        for ics in icss:
            countries_codes.append(ics.country.iso_code)
            if ics.country.iso_code == geoip_ccode:
                native_country = ics.country
            else:
                countries.append(ics.country)
        # add the native_country as first if it exists
        if native_country:
            countries = [native_country] + countries
    else:
        pass
    data = dict(initiative=initiative, countries=countries,
                countries_codes=countries_codes, native_country=native_country,
                geoip_ccode=geoip_ccode, prereg_form=prereg_form)
    return render(req, 'initiative.html', data)


@no_go_back
def preregistration_processed(req, code_name):
    # get initiative
    initiative, response = _get_initiative(code_name, check_active=False)
    if response:
        return response
    data = dict(initiative=initiative)
    return render(req, 'preregistration_processed.html', data)


@no_go_back
def statement_submission(req, code_name, ccode):
    """
    Displays a form specific to the country specified by ccode;
    If providers are specified, it uses them to obtain and prepopulate.
    After success, it redirects to thanks page.
    """
    # get initiative
    initiative, response = _get_initiative(code_name)
    if response:
        return response
    # country settings
    ics_cache_key = "ics__{0}__{1}".format(code_name, ccode)
    ics = cache.get(ics_cache_key)
    if ics is None:
        try:
            ics = initiative.initiativecountrysettings_set.select_related()\
                                                .get(country__iso_code=ccode)
        except models.InitiativeCountrySettings.DoesNotExist:
            raise Http404
        cache.set(ics_cache_key, ics)
    country = ics.country
    
    current_url = reverse(statement_submission, args=[code_name, ccode])
    # check if cookies are working because the CSRF protection relies on it
    if not req.session.get('cookie_was_tested'):
        req.session.set_test_cookie()
        return HttpResponseRedirect(reverse(test_cookie)+"?return_to="+\
                                    current_url)

    # sessions are supported - go on
    required_providers = list(ics.required_providers.all())
    has_required_providers = bool(required_providers)
    initial_data = {}
    disable_captcha = False
    provider_name = None
    provider_code = None
    # module implementing the provider at hand might create some extra content
    # that should be rendered on the submission page
    # if yes, it will be stored here with key being the provider code_name
    provider_extra_content = {}
    prov_key = _create_provider_data_session_key(initiative, country)
    if prov_key in req.session:
        # we have data for this initiative, country in session, use them
        session_data = req.session[prov_key]
        initial_data = session_data.get('data', {})
        disable_captcha = session_data.get('disable_captcha', False)
        provider_code = session_data.get('provider_code')
        
    was_legal_post = False # if not post or not legal, then false 
    # was the form submitted, or is it just going to be filled in?
    if req.method == 'POST':
        # submitted by POST - lets see if it is valid
        # at first check if there are required_providers and if yes, then
        # ensure that we have initial_data - otherwise it is invalid
        if has_required_providers and not initial_data:
            was_legal_post = False
        else:
            was_legal_post = True
            # the form was submitted - validate it and process if valid
            form = SupportSubmissionForm(country, data=req.POST,
                                         initial=initial_data,
                                         disable_captcha=disable_captcha,
                                         provider_name=provider_name)
            if form.is_valid():
                # here we do all the hard stuff
                try:
                    if _process_submission(initiative, ics, form,
                                           req.LANGUAGE_CODE,
                                           provider_data=initial_data,
                                           provider_code=provider_code):
                        language = req.session.get('django_language')
                        req.session.flush()
                        req.session['django_language'] = language
                        if prov_key in req.session:
                            del req.session[prov_key]
                        return HttpResponseRedirect('thanks/')
                    else:
                        raise Exception("Storing submission did not succeed")
                except Exception as exc:
                    logger.error("Cannot save support statement '%s'" % exc)
                    return HttpResponseRedirect('error/')
                    
    if not was_legal_post:
        # we got here by GET, so if there is any initial_data, the user
        # probably hit reload and we want to clear the data
        if initial_data:
            if prov_key in req.session:
                del req.session[prov_key]
            initial_data = {}
        if has_required_providers:
            # there are some required providers
            # lets see if there is some data arriving from any of the providers
            # it would be in session under the name of the provider
            # (current url is used to locate the data in session)
            active_provider = None
            provided_data = None
            for provider in required_providers:
                if provider.code_name in req.session and \
                    current_url in req.session[provider.code_name]:
                    active_provider = provider
                    provided_data = req.session[provider.code_name][current_url]
                    break
            if active_provider:
                if provided_data['provider_result_ok']:
                    initial_data = provided_data['data']
                    # remove the intermediate provider data from the session
                    del req.session[active_provider.code_name]
                    # store data in a separate part of session where is remains
                    # until the submission is successfully submitted or the session
                    # expires
                    session_data = {
                            "disable_captcha": active_provider.replaces_captcha,
                            "data": initial_data,
                            "provider_code": active_provider.code_name,
                            "provider_name": active_provider.name}
                    req.session[prov_key] = session_data
                    form = SupportSubmissionForm(country, initial=initial_data,
                            disable_captcha=active_provider.replaces_captcha,
                            provider_name=active_provider.name)
                else:
                    raise Exception('problem with provider')
            else:
                # there are no arriving data from a provider, so no form
                # should be displayed, just a list of the providers 
                form = None
                # we are starting with a provider and should therefore
                # set a test cookie so that the provider can then check it
                req.session.set_test_cookie()
                # set provider specific extra stuff
                for provider in required_providers:
                    temp_name = "{0}_submission_extra_content.html".format(
                                                            provider.code_name)
                    try:
                        extra = loader.render_to_string(temp_name,
                                                        {"provider": provider})
                    except TemplateDoesNotExist:
                        pass
                    else:
                        provider_extra_content[provider.code_name] = extra
        else:
            # no post and not required providers - this is the simple case
            # where we display an empty form
            form = SupportSubmissionForm(country)

    data = dict(initiative=initiative, country=country, form=form,
                required_providers=required_providers,
                provider_extra_content=provider_extra_content)
    return render(req, 'statement_submission.html', data)


def statement_submission_provider(req, code_name, ccode, provider_id):
    """
    This view takes care of dealing with a specific UserDataProvider
    by calling its views.start
    """
    # get initiative
    initiative, response = _get_initiative(code_name)
    if response:
        return response
    try:
        ics = initiative.initiativecountrysettings_set.select_related()\
                                                .get(country__iso_code=ccode)
    except models.InitiativeCountrySettings.DoesNotExist:
        raise Http404
    try:
        provider = ics.required_providers.get(code_name=provider_id)
    except models.UserDataProvider.DoesNotExist:
        raise Http404
    # the module that contains the provider
    provider_module = globals().get(provider.code_name)
    if not provider_module:
        raise Http404
    # check if cookies are working because the provider code relies on them
    if req.session.test_cookie_worked():
        req.session.delete_test_cookie()
        req.session['cookie_was_tested'] = True
    elif req.session.get('cookie_was_tested'):
        # this is more long term way of remembering if the session works
        pass
    else:
        req.session.set_test_cookie()
        data = dict(initiative=initiative, country=ics.country,
                    provider=provider)
        return render(req, 'provider_cookie_error.html', data)
    # prepare data for the provider and run it
    return_url = reverse(statement_submission, args=[code_name, ccode])
    # requested attrs
    fields = []
    for cif in ics.country.countryidentfield_set.all().select_related():
        field = cif.ident_field
        if field.slug != 'citizenship':
            fields.append(field.slug)
    return provider_module.views.start(req, return_url, fields)    


def statement_submission_processed(req, code_name, ccode):
    # get initiative
    initiative, response = _get_initiative(code_name)
    if response:
        return response
    try:
        ics = initiative.initiativecountrysettings_set.select_related()\
                                                .get(country__iso_code=ccode)
    except models.InitiativeCountrySettings.DoesNotExist:
        raise Http404
    country = ics.country
    data = dict(initiative=initiative, country=country)
    return render(req, 'statement_submission_processed.html', data)


def statement_submission_error(req, code_name, ccode):
    # get initiative
    initiative, response = _get_initiative(code_name)
    if response:
        return response
    try:
        ics = initiative.initiativecountrysettings_set.select_related()\
                                                .get(country__iso_code=ccode)
    except models.InitiativeCountrySettings.DoesNotExist:
        raise Http404
    country = ics.country
    data = dict(initiative=initiative, country=country)
    return render(req, 'statement_submission_error.html', data)


def test_cookie(req):
    return_to = req.GET.get("return_to")
    if not return_to:
        return_to = req.META.get("HTTP_REFERER")
    # validate the return_to setting
    if return_to:
        try:
            resolve_with_prefix(return_to)
        except Http404:
            logger.warn("Someone is trying some funny business with the "
                        "'return_to' field: '%s'", return_to)
            return_to = None
    if not return_to:
        return HttpResponseRedirect(reverse(homepage))
    if req.session.test_cookie_worked():
        req.session.delete_test_cookie()
        req.session['cookie_was_tested'] = True
        return HttpResponseRedirect(return_to)  
    else:
        req.session.set_test_cookie()
        data = dict(return_to=return_to)
        return render(req, 'cookie_error.html', data)

def csrf_failure(req, reason=""):
    logging.warn("CSRF failure: %s", reason)
    return render(req, "csrf_failure.html", status=403)


def error500(request):
    data = {}
    if request.user.is_staff:
        # we add some extra into the data
        exc_type, exc_value, tb = sys.exc_info()
        data['traceback'] = "\n".join(traceback.format_tb(tb))
        data['exc_type'] = exc_type.__name__
        data['exc_value'] = unicode(exc_value)
    return render(request, "500.html", data, status=500)


def _process_submission(initiative, ics, form, language, provider_data=None,
                        provider_code=None):
    # generate the data to encrypt
    to_encrypt = []
    for key, value in sorted(form.cleaned_data.iteritems()):
        if key == "captcha":
            pass # ignore captcha
        else:
            if type(value) is datetime.date:
                value = str(value)
            record = dict(name=key, value=value)
            if key in provider_data and provider_code:
                record['src'] = provider_code
            to_encrypt.append(record)
    # encryption
    encryptor = _get_or_create_encryptor(initiative)
    to_encrypt = json.dumps(to_encrypt, sort_keys=True)
    to_encrypt = zlib.compress(to_encrypt)
    enc_key, enc_text = encryptor.encrypt_string(to_encrypt)
    del to_encrypt
    sym_cipher = _get_or_create_cipher_obj(encryptor.sym_cipher)
    # get encryption key instance
    enc_key64 = b64encode(enc_key)
    key = ENC_KEYS.get(enc_key64)
    if not key:
        try:
            key = initiative.certificate.encryptionkey_set.get(
                                                        encrypted_key=enc_key64)
        except models.EncryptionKey.DoesNotExist:
            key = models.EncryptionKey(encrypted_key=enc_key64,
                                       pub_key=initiative.certificate)
            key.save()
        ENC_KEYS[enc_key64] = key
    # store the submission
    enc_text64 = b64encode(enc_text)
    supst = models.SupportStatement(initiative=initiative, content=enc_text64,
                                    encryption_key=key, country=ics.country,
                                    encryption_cipher=sym_cipher,
                                    language=language)
    supst.save()
    # log into the forensic log
    salt = _generate_salt()
    hasher = hashlib.sha256()
    hasher.update(salt)
    hasher.update(enc_text64)
    hasher.update(ics.country.iso_code)
    hasher.update(str(supst.date))
    hasher.update(enc_key64)
    forensic_logger.info("%s,%d,%s,%s", initiative.code_name, supst.pk, salt,
                         hasher.hexdigest())
    return True
    
    
def _get_or_create_encryptor(initiative):
    result = ENCRYPTORS.get(initiative.code_name)
    if not result:
        logger.debug("Creating encryptor for %s", initiative.code_name)
        pub_key = Encryptor.extract_public_key_from_cert(
                                            initiative.certificate.certificate)
        result = Encryptor(pub_key, sym_cipher=settings.ENC_SYMMETRIC_CIPHER,
                           key_lifetime=settings.ENC_KEY_LIFETIME)
        ENCRYPTORS[initiative.code_name] = result
    return result

def _get_or_create_cipher_obj(cipher):
    key = "cipher_%s" % cipher
    cipher_obj = cache.get(key)
    if cipher_obj:
        return cipher_obj
    else:
        cipher_obj, _x = models.EncryptionCipher.objects.get_or_create(
                                                                    name=cipher)
        cache.set(key, cipher_obj, 3600)
        return cipher_obj

    
def _create_provider_data_session_key(initiative, country):
    key = "provider_{0.code_name}_{1.iso_code}".format(initiative, country)
    return key


def _get_initiative(code_name, check_active=True):
    """
    Finds a matching initiative and checks if it accepts support submissions.
    It returns a tuple (initiative, response). In case everything is ok,
    it will be (initiative, None) in case of problem (None, response) where
    response is the response that the view should return (403 or something
    similar).
    It also raises Http404 which might be ignored by the view and let to
    propagate upwards
    """
    initiative = None
    response = None
    # try cache
    key = "initiative_%s" % code_name
    initiative = cache.get(key)
    if initiative:
        return (initiative, response)
    try:
        initiative = models.Initiative.objects.select_related("certificate")\
                                                    .get(code_name=code_name)
    except models.Initiative.DoesNotExist:
        raise Http404
    cache.set(key, initiative)
    # test for initiative activity
    if check_active and not initiative.is_collecting():
        response = HttpResponseForbidden(
                                _("Forbidden: this initiative is not active"))
    return (initiative, response)


def _generate_salt():
    return b64encode(M2Crypto.m2.rand_bytes(6))

def crash(req):
    raise Exception("A simulated error.")

