# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>


"""
Contains high-level code for support of mixed symmetric and asymmetric
encryption.
"""

import M2Crypto
import time
import struct

class EncryptError(Exception):
    
    pass

class Encryptor(object):
    """
    Provides encryption facilities with adjustable rotation of symmetric
    encryption keys.
    
    On first use of encryption, a random key for symmetric encryption is
    created and encrypted using provided public key of an asymmetric cipher.
    This symmetric encryption key is then used to encrypt data until it is
    invalidated (either by user or by reaching the number of signatures)
    and a new key is generated.
    """
    
    PADDING_SCHEME = M2Crypto.RSA.pkcs1_oaep_padding
    
    
    def __init__(self, pub_key, sym_cipher="aes_256_cbc", key_lifetime=10):
        """
        pub_key - public key instance; used for encryption key encryption
        key_lifetime - the symmetrical key for encryption will be rotated after 
                       this number of uses (0 = never rotate)
        sym_cipher - name of cipher to be used - anything supported by M2Crypto
                     can be used.
        """
        self.pub_key = pub_key
        self.sym_cipher = sym_cipher
        self.key_lifetime = key_lifetime
        self._key_use_counter = 0
        self._key_size = self.keysize_from_name(self.sym_cipher)
        self._key = None
        self.encrypted_key = None
        self._block_size = 16 # assume AES cipher
        
    @classmethod
    def extract_public_key_from_cert(cls, cert):
        """
        Takes certificate as a string and extracts the public key from it.
        May be used to create the public key for instance initialization.
        """
        if type(cert) == unicode:
            cert = cert.encode('ascii')
        cert = M2Crypto.X509.load_cert_string(cert, M2Crypto.X509.FORMAT_PEM)
        pub_key = cert.get_pubkey().get_rsa()
        return pub_key
        
    @classmethod
    def keysize_from_name(self, cipher_name):
        """
        Extracts key size from a cipher name and returns it in bytes 
        """
        parts = cipher_name.split("_")
        if len(parts) == 3:
            try:
                return int(parts[1]) / 8
            except:
                return EncryptError("Could not extract keysize from cipher "
                                    "name part %s; " % parts[1])
        return EncryptError("Could not extract keysize from cipher "
                            "name %s; unexpected format" % cipher_name)
                
        
    def encrypt_string(self, text):
        """
        Takes a string and returns a tuple - symmetric key encrypted using
        the supplied public key and encrypted text (enc_key, enc_text).
        To the enc_text, random initialization vector (iv) is prepended
        with size equal to that of the sym_cipher key size 
        """
        if not self._key:
            self._generate_key()
        iv = self._generate_iv()
        k = M2Crypto.EVP.Cipher(alg=self.sym_cipher, key=self._key, iv=iv, op=1)
        out = k.update(text)
        out += k.final()
        encrypted = iv+out
        result = (self.encrypted_key, encrypted)
        self._increment_use_counter() # this nulls self.encrypted_key
        return result
            
    def decrypt_string(self, priv_key, enc_key, text):
        sym_key = priv_key.private_decrypt(enc_key, self.PADDING_SCHEME)
        iv = text[0:self._block_size]
        enc_text = text[self._block_size:]
        
        # The ciphertext won't go back to voting client, so he can't exploit
        # it for Vaudenay's padding oracle in decryption.
        k = M2Crypto.EVP.Cipher(alg=self.sym_cipher, key=sym_key, iv=iv, op=0)
        dec_text = k.update(enc_text)
        dec_text += k.final()
        return dec_text
            
    def _generate_key(self):
        self._key = M2Crypto.m2.rand_bytes(self._key_size)
        self.encrypted_key = self.pub_key.public_encrypt(self._key,
                                                         self.PADDING_SCHEME)
        self._key_use_counter = 0

    def _generate_iv(self):
        rand_part = M2Crypto.m2.rand_bytes(self._block_size-6)
        time_part = struct.pack("!d", time.time())[:6]
        return rand_part + time_part
    
    def _increment_use_counter(self):
        """
        Increment counter and invalidate key if key_lifetime is reached
        """
        self._key_use_counter += 1
        if self.key_lifetime != 0 and \
            self._key_use_counter >= self.key_lifetime:
            self._key = None
            self.encrypted_key = None

# ------------------ unittests ----------------------

import unittest

class EncryptorTest(unittest.TestCase):
    
    def _gen_callback(self, a, b):
        pass
    
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.keys = M2Crypto.RSA.gen_key(1024, 65537,
                                         callback=self._gen_callback)
        self.pub_key = M2Crypto.RSA.new_pub_key(self.keys.pub())
    
    def test_encryption(self):
        input_text = "This is test text."
        enc = Encryptor(self.pub_key)
        enc_key, enc_text = enc.encrypt_string(input_text)
        self.assertNotEqual(enc_key, None)
        dec_text = enc.decrypt_string(self.keys, enc_key, enc_text)
        self.assertEqual(input_text, dec_text,
                         "Encrypted and decrypted text are not equal")
        
    def test_rotation(self):
        input_text = "This is test text."
        enc = Encryptor(self.pub_key, key_lifetime=1)
        # encrypt first
        enc_key, enc_text = enc.encrypt_string(input_text)
        self.assertNotEqual(enc_key, None)
        self.assertEqual(enc.encrypted_key, None)
        # encrypt second
        enc_key2, enc_text2 = enc.encrypt_string(input_text)
        self.assertNotEqual(enc_key2, None)
        self.assertEqual(enc.encrypted_key, None)
        self.assertNotEqual(enc_key, enc_key2, "Keys were not rotated")
        self.assertNotEqual(enc_text, enc_text2, "Encrypted texts match even "
                            "though having different keys")
        # check decryption
        dec_text = enc.decrypt_string(self.keys, enc_key, enc_text)
        dec_text2 = enc.decrypt_string(self.keys, enc_key2, enc_text2)
        self.assertEqual(dec_text, dec_text2, "Messages encrypted different "
                         "keys are not equal after decryption")
        
    def test_rotation2(self):
        input_text = "This is test text."
        enc = Encryptor(self.pub_key, key_lifetime=2)
        # encrypt first
        enc_key, _enc_text = enc.encrypt_string(input_text)
        self.assertNotEqual(enc.encrypted_key, None, "Should not invalidate")
        # encrypt second
        enc_key2, _enc_text2 = enc.encrypt_string(input_text)
        self.assertEqual(enc_key, enc_key2, "Should not rotate")
        self.assertEqual(enc.encrypted_key, None, "Should invalidate")
        # encrypt third
        enc_key3, _enc_text3 = enc.encrypt_string(input_text)
        self.assertNotEqual(enc_key2, enc_key3, "Should rotate")
        self.assertNotEqual(enc.encrypted_key, None, "Should not invalidate")
        
    def test_key_sizes(self):
        input_text = "This is test text."
        for key_size in ("128", "192", "256"):
            enc = Encryptor(self.pub_key, sym_cipher="aes_%s_cbc" % key_size)
            enc_key, enc_text = enc.encrypt_string(input_text)
            self.assertNotEqual(enc_key, None)
            dec_text = enc.decrypt_string(self.keys, enc_key, enc_text)
            self.assertEqual(input_text, dec_text,
                             "Encrypted and decrypted text are not equal")
        
