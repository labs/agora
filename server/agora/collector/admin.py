# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

from django.contrib import admin
import models

class InitiativeOwnerAdmin(admin.ModelAdmin):
    list_display = ('user', 'initiative')
    list_filter = ('user', 'initiative')

class InitiativeCountrySettingsAdmin(admin.ModelAdmin):
    list_display = ('initiative', 'country')
    list_filter = ('initiative',)
    
class InitiativeAdmin(admin.ModelAdmin):
    """
    Customized admin to enforce the status change work-flow as well as premature
    deactivation being possible only by selected users.
    """

    def get_readonly_fields(self, request, obj=None):
        rofs = list(self.readonly_fields)
        user = request.user
        if obj:
            if not user.has_perm("collector.change_initiative_status"):
                rofs.append("status")
            if not user.has_perm("collector.change_initiative_dates"):
                rofs.append('start_date')
                rofs.append('end_date')
            if not user.has_perm("collector.change_initiative_code_name"):
                rofs.append('code_name')
        return rofs


    def save_model(self, request, obj, form, change):
        rofs = self.get_readonly_fields(request, obj=obj)
        if change:
            # we do not allow some fields to be changed. 
            # because this is taken care of by making the fields
            # read-only, here we only guard against some funny business by the
            # user and can allow an Exception to be thrown
            for rof in rofs:
                if rof in form.changed_data:
                    raise Exception("Field '%s' cannot be modified" % rof)
        obj.save()



# registration of admin instances

admin.site.register(models.InitiativeOwner, InitiativeOwnerAdmin)
admin.site.register(models.Initiative, InitiativeAdmin)
admin.site.register(models.Certificate)
admin.site.register(models.InitiativeCountrySettings,
                    InitiativeCountrySettingsAdmin)
admin.site.register(models.UserDataProvider)

