# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in modules
from datetime import datetime
import string

# django imports
from django import forms
from django.forms.extras.widgets import SelectDateWidget
from django.utils.text import truncate_words
from django.conf import settings
from django.utils.html import escape
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

# third party imports
# captcha backend based on settings
if settings.CAPTCHA_BACKEND == 'django-simple-captcha':
    from captcha.fields import CaptchaField
    def captcha_field():
        return CaptchaField()
elif settings.CAPTCHA_BACKEND == 'django-recaptcha':
    from captcha.fields import ReCaptchaField
    def captcha_field():
        return ReCaptchaField(label="", attrs={'theme' : 'white'})
else:
    raise Exception("CAPTCHA_BACKEND contains invalid value '%s'" % \
                    settings.CAPTCHA_BACKEND)


class SlightlyRestrictiveCharField(forms.CharField):
    
    def to_python(self, value):
        value = super(SlightlyRestrictiveCharField, self).to_python(value)
        for char in value:
            if ord(char) < 128:
                if char not in string.printable:
                    raise forms.ValidationError(
                                            _("Invalid character in input."))
        # normalize whitespace
        value = u" ".join(value.split())
        return value


class SupportSubmissionForm(forms.Form):
    
    def __init__(self, country, disable_captcha=False, provider_name=None,
                 *args, **kwargs):
        data = kwargs.get('data')
        initial = kwargs.get("initial", {})
        # see if there are initial data and if yes, the use them to override
        # user supplied data. This is here to prevent user from doing some
        # low-level changes to the sent data
        if data:
            new_data = {}
            # copy all things from data to new_data while replacing what
            # is in initial
            for key, value in data.iteritems():
                if key in initial and initial[key]:
                    # if there are initial data for this key
                    new_data[key] = initial[key]
                else:
                    new_data[key] = value
            # go through initial and add values that are not in data
            # this happens when a form with read-only selection is posted
            for key, value in initial.iteritems():
                if value and key not in new_data:
                    new_data[key] = value
            kwargs['data'] = new_data
        super(SupportSubmissionForm, self).__init__(*args, **kwargs)
        cifs = []
        requires_id_doc = False
        for cif in country.countryidentfield_set.all().select_related()\
                                                        .order_by("position"):
            cifs.append(cif)
            if cif.ident_field.slug == "id_number":
                requires_id_doc = True
        for cif in cifs:
            field = cif.ident_field
            # choose the type and additional data
            add = {}
            if field.data_type == field.TYPE_INT:
                ftype = forms.IntegerField
            elif field.data_type == field.TYPE_TEXT:
                ftype = SlightlyRestrictiveCharField
            elif field.data_type == field.TYPE_DATE:
                ftype = forms.DateField
                year = datetime.now().year
                add['widget'] = SelectDateWidget(years=range(year-120, year+1))
            elif field.data_type == field.TYPE_ENUM:
                ftype = forms.ChoiceField
                if field.slug == 'citizenship':
                    # when the country requires an ID document, than it probably
                    # does not allow citizens of other countries to submit
                    # support because they would not have the proper ID
                    # therefore limit the selection
                    if requires_id_doc:
                        choices = [(country.iso_code, country.native_name)]
                    else:
                        choices = [(cntry.iso_code, cntry.native_name)
                                   for cntry in country.__class__.objects.all()]
                    add['initial'] = country.iso_code
                else:
                    values = cif.countryidentfieldvalidvalue_set.all()\
                                 .order_by('position')
                    choices = [(v.code, truncate_words(v.value, 10))
                               for v in values]
                add['choices'] = choices    
            
            # this splits label by '-' and puts the second part in <span> for better formatting
            sublabels = map(escape, field.name.split("-", 1))
            label_html = sublabels[0]
            if len(sublabels) > 1:
                label_html += mark_safe("<span>") + sublabels[1] + mark_safe("</span>")
            # create the field
            field_obj = ftype(label=label_html, **add)
            # disable autocomplete on char fields
            if issubclass(ftype, forms.CharField):
                field_obj.widget.attrs['autocomplete'] = "off"
            # adjust the field as necessary
            if field.slug in initial:
                # do not allow editing of data provided in initial
                if ftype is forms.ChoiceField:
                    field_obj.widget.attrs['disabled'] = True
                    # if the field is disabled, its value will not be sent back
                    # so we cannot require it. However, this is no problem, as
                    # we have this data in initial anyway
                    field_obj.required = False
                else:
                    field_obj.widget.attrs['readonly'] = True
                if provider_name:
                    field_obj.help_text = provider_name
            # add the field to the list
            self.fields[field.slug] = field_obj
                
        # add captcha
        if not disable_captcha:
            self.fields['captcha'] = captcha_field()
            


class PreregistrationForm(forms.Form):
    
    email = forms.EmailField(label=_("Email"))
    captcha = captcha_field()