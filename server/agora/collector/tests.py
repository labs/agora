# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built in modules
from copy import copy

# django stuff
from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse
from django.conf import settings


# local imports
import views

class SubmissionTest(TestCase):
    fixtures = ['test1.json']

    test_inp_temp = {'given_name': 'Beda',
                     'family_name': 'Kosata',
                     'citizenship': 'HU',
                     'recaptcha_response_field': 'PASSED',
                     'id_type': 'HU01',
                     'id_number': 12345678}
    
    def setUp(self):
        settings.DEBUG = True
        self.client = Client()
        self.test_url = reverse(views.statement_submission,
                                args=["carrots", "HU"])
        
    def testSubmission(self):
        """
        To go around the lengthy loading of fixtures for every test, we
        call them all from here, because they should be independent.
        """
        self._testSubmissionOK()
        self._testSubmissionFailName()
        self._testSubmissionFailCaptcha()
        self._testSubmissionFailCitizenship()

    def _testSubmissionOK(self):
        response = self.client.post(self.test_url, self.test_inp_temp)
        # it should be submitted without problem and thus return 302
        self.assertEqual(response.status_code, 302)
        
    def _testSubmissionFailName(self):
        inp2 = copy(self.test_inp_temp)
        inp2['given_name'] = ""
        response = self.client.post(self.test_url, inp2)
        # it should not be submitted, this returning 200 instead of 302
        self.assertEqual(response.status_code, 200)
        
    def _testSubmissionFailCaptcha(self):
        inp2 = copy(self.test_inp_temp)
        inp2['recaptcha_response_field'] = "AAA"
        response = self.client.post(self.test_url, inp2)
        # it should not be submitted, this returning 200 instead of 302
        self.assertEqual(response.status_code, 200)
        
    def _testSubmissionFailCitizenship(self):
        inp2 = copy(self.test_inp_temp)
        inp2['citizenship'] = "BG"
        response = self.client.post(self.test_url, inp2)
        # it should not be submitted, this returning 200 instead of 302
        # when the citizenship in form data differs from URL, it should fail
        self.assertEqual(response.status_code, 200)