��    z      �  �   �      H
  �   I
  .  ?  �   n  �   1  �    �   �  J   o  �   �  �   v  /   Q     �  "   �  C   �  *   �          ,     J     j     �      �     �      �  P   �     B  	   I     S     f     �     �     �  	   �     �     �     �  	   �     �               *     B     K     Q     o       
   �  (   �     �     �  �   �          �     �  w   �     +  
   0     ;     W     h     z     �     �     �     �  	   �  	   �     �     �     �  B   �     6  V   H  q   �             =   9     w     �  �   �     6     =     N     a     n  2   r  
   �     �     �     �     �     �     �       �   0  h   (  i   �  6   �  D   2  �   w  \     \   p  �   �     }  
   �     �     �     �  #   �  "   �           	   =      P   ]   T   �   T   !  a   X!     �!  �   �!     Z"     k"  �   z"  %   !#  x  G#  �   �$  :  �%  �   �&  �   �'  �  �(  �   E*  M   +  �   ^+  �   ",  6   -  
   >-     I-  U   g-  /   �-     �-  &   .  ,   ,.  *   Y.     �.  (   �.     �.      �.  [   �.  	   T/     ^/     r/     �/  
   �/     �/     �/     �/     �/     �/     �/     �/     0     0     !0     90     R0     c0     i0     �0     �0     �0  *   �0     �0     �0  �   1     �1     �1     �1  �   �1     q2  
   v2     �2     �2     �2  
   �2     �2     �2     �2     �2     3  
   3     3     $3     >3  R   D3     �3  c   �3  �   4     �4     �4  E   �4     %5     55  �   G5      6     .6     @6     \6     j6  9   s6     �6     �6     �6     �6     �6     �6     7     7    97  q   N8  k   �8  6   ,9  J   c9  �   �9  V   M:  ]   �:  �   ;     �;  
   �;  	   �;  $   �;  &   �;     <      5<     V<     g<  ?   ~<  Y   �<  Z   =  Z   s=  ]   �=     ,>  �   8>     �>     �>  �   �>  )   �?        ^   ;   z                8       P   J       g           ]              '         :       W       T   e           5       h   I       -      N   6       q   H   %   3          F   b       U   #             ,      [   @   s   B   O                      M   
                 i   Y   A       p   Z   ?               _              	       d   *   G   >   a          (   $   .       f   1          \       m   K   w   j   S   Q   0          <   o      u      2   D   y       x   "   7   X   V       =   C       4   k         R   t   &   L   +       r          )          /   `   !   n   E   9             l      c   v    
Because of the Cross Site Request Forgery (CSRF) protection requirement of the 
European Citizens' Initiative legislation, this website requires cookies to be
enabled. Without cookie support, it is not possible to submit statements of support.
 
For more information about the European Citizens' Initiative in general,
visit its <a href="%(agora_info_page)s">official website</a>.
For more information about the software running on this server, its security and
handling of personal data, please visit our <a href="%(about_page)s">about page</a>.
 
If you wish, you can try to resubmit your support statement later on the
<a href="%(back_url)s">support statement page</a> or you can return to the
<a href="%(website)s">initiative website</a>. 
One of the most important requirements on a system for collection of statements
of support for the European Citizens' Initiative is protection of personal data.
This site implements all protection required by relevant legislation:
 
The software used to collect statements of support on this website was created
in the <a href="%(labs_page)s">CZ.NIC Labs</a>, a research and development department of the .cz domain 
registry <a href="%(cznic_page)s">CZ.NIC</a>. It fully conforms to all legislative requirements of the 
European Union for such a system and its source code is made 
<a href="%(proj_page)s">available for public review</a>.
 
This website was created to conform to 
<a href="http://www.w3.org/WAI/">Web Accessibility Initiative (WAI)</a>
accessibility rules and related Czech website accessibility regulations. 
 
You can also return to the <a href="%(website)s">initiative website</a>.
 
You do not have cookies enabled or allowed. This page requires cookies to 
function properly. Please turn cookies on and return to the 
<a href="%(back_url)s">support statement page</a>. 
You do not have cookies enabled or allowed. This page requires cookies to 
function properly. Please turn cookies on and then either reload this page manually or
follow <a href="%(back_url)s">this link</a> for reload. <a href="%(homepage)s">Back to the homepage</a> About About support statement collection All sensitive user data are encrypted immediately after submission. An error occurred when preparing this page CSRF check failure Can advance initiative status Can change initiative code name Can change initiative dates Can change initiative status Can prematurely close initiative Certificate Certificate for '%(initiative)s' Certificate whose public key will be used to encrypt data of support statements. Closed Code name Collecting support Commission registration number Confirmation Cookie error Cookies are not enabled Countries Country Country name Current initiatives Data type Detected from IP address Email Encryption certificate Encryption certificates End date Error European citizens' initiative Exception type: Exception value: Extra info Forbidden: this initiative is not active Go back and change it Homepage I hereby certify that the information that I have provided
in this form is correct and that I have only supported this proposed
citizens' inititative once. ISO code Identification field Identification fields If your country is not on the list above, the initiative is
not active here.<br />Please contact initiative organizers. Info Initiative Initiative country settings Initiative owner Initiative owners Initiatives Introduction Invalid character in input. Language Language iso code Languages More info Name Native name New No sensitive data are stored in plain form anywhere in the system. Not your country? Number of citizens from country who must support the initiative for it to be accepted. Our admins were notified about the problem and will
attend to it as soon as possible. Please try this link later. Page not found Personal data protection Please fill in the following fields required in your country. Preregistration Privacy Policy Public-key cryptography is used to ensure only initiative
organizers are able to decrypt user data. Decryption key is not stored anywhere
in the system itself. Reload Replaces captcha Required providers Server error Set Sorry, the page you are looking for was not found. Start date Statement of support Statements of support Status Submit Support limit Thank you for your interest! Thank you for your support! The Cross-site request forgery protection check
failed. This may be either caused by a malicious code running
in your browser or may result from cookies being turned off.
If you do not have cookies enabled, please turn them on and reload
the page. The initiative is active in the following countries.<br />
Select country according to your citizenship. The initiative organizers require that user data for your
country are supplied by the following provider. There are no initiatives currently collecting support. There was an error saving your preregistration, please try it again. This initiative does not collect support yet. If you wish
to be informed once this initiative becomes active, please leave us your
email in the form below. This initiative is already closed. It is no longer possible
to submit statements of support. This initiative is not active. It is currently not possible
to submit statements of support. This is the main page of a system for collection of statements
of support for the European Citizens' Initiative. Choose the initiative you want
to support from the list below. Title Traceback: User User data provider User data providers We apologize for the inconvenience. Web address in Commission register Website Website accessibility You are logged in as <a href="%(home_url)s">%(username)s</a>. You are seeing this info because you are logged in as staff user '%(username)s'. You can use this link
to return to the <a href="%(website)s">initiative website</a>. You can use this link to return
to the <a href="%(website)s">initiative website</a>. You can visit the <a href="%(homepage)s">homepage</a>
for a list of currently active initiatives. Your country Your email address was successfully stored in our
database. We will let you know once this initiative is open
for statement of support collection. Your information Your selection Your statement could not be saved due to an error on the
server side. Our administrators were notified and we will do our best to fix
the problem as soon as possible. Your statement of support was stored. Project-Id-Version: git
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-10-14 14:46+0200
PO-Revision-Date: 2012-10-19 11:16+0200
Last-Translator: Beda Kosata <bedrich.kosata@nic.cz>
Language-Team: Czech <>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 
S ohledem na ochranu proti Cross Site Request Forgery (CSRF) požadovanou legislativou
o Evropské Občanské Iniciativě, vyžadují tyto stránky podporu pro cookies. Bez podpory cookies není možné odeslat vyjádření podpory.
 
Pro více informací o Evropské Občanské Iniciativě obecně, navštivte její <a href="%(agora_info_page)s">oficiální stránky</a>.
Více informací o softwaru, který tento server využívá, jeho bezpečnosti a zpracování osobních dat najdete na stránce <a href="%(about_page)s">o sběru podpory</a>.
 
Vaše vyjádření podpory můžete později zkusit znovu odeslat
na <a href="%(back_url)s">stránce vyjádření podpory</a> a nebo se můžete vrátit
na <a href="%(website)s">webové stránky iniciativy</a>. 
Jeden z nejdůležitějších požadavků na systém pro sběr vyjádření podpory
pro Evropskou Občanskou iniciativu je ochrana osobních dat.
Tyto stránky implementují veškerou ochranu vyžadovanou příslušnou legislativou:
 
Software používaný pro sběr vyjádření podpory na těchto stránkách byl vytvořen
v <a href="%(labs_page)s">Laboratořích CZ.NIC</a>, vývojovém oddělení 
doménového registru pro .cz <a href="%(cznic_page)s">CZ.NIC</a>. Systém plně splňuje legislativní podmínky Evropské Unie pro takovéto systémy a jeho zdrojové kódy jsou k <a href="%(proj_page)s">dispozici veřejnosti k posouzení</a>. 
 
Tyto stránky byly vytvořeny podle doporučení
<a href="http://www.w3.org/WAI/">Web Accessibility Initiative (WAI)</a>
pro přístupný web a podle související české vyhlášky o přístupnosti.
 
Můžete se také vrátit na <a href="%(website)s">stránku iniciativy</a>.
 
Nemáte zapnuty a nebo povoleny cookies. Tato stránka je pro svou správnou
funkci vyžaduje. Prosím zapněte cookies a vraťte se na 
<a href="%(back_url)s">stránku vyjádření podpory</a>. 
Nemáte zapnuty a nebo povoleny cookies. Tato stránka je pro svou správnou
funkci vyžaduje. Prosím zapněte cookies a obnovte manuálně stránku a nebo použijte 
<a href="%(back_url)s">tento odkaz</a> k jejímu obnovení. <a href="%(homepage)s">Zpět na domovskou stránku</a> O aplikaci O sběru vyjádření podpory Veškerá citlivá data uživatelů jsou zašifrována bezprostředně po odeslání. Při přípravě této stránky došlo k chybě Selhání kontroly CSRF Může posunout vpřed stav iniciativy Může měnit kódové označení iniciativy Může měnit začátek a konec iniciativy Může měnit stav iniciativy Může předčasně uzavřít iniciativu Certifikát Certifikát pro '%(initiative)s' Certifikát jehož veřejný klíč bude použit k zašifrování dat vyjádření podpory. Uzavřeno Kódové označení Sbírá podporu Registrační číslo Komise Potvrzení Chyba cookie Cookies jsou vypnuty Země Země Jméno země Současné iniciativy Typ dat Detekováno z IP adresy Email Šifrovací certifikát Šifrovací certifikáty Datum ukončení Chyba Evropská občanská iniciativa Typ výjimky: Hodnota výjimky: Extra informace Zakázáno: tato iniciativa není aktivní Jděte zpět a změňte ji Domovská stránka Prohlašuji, že mnou poskytnuté informace v tomto formuláři jsou správné
a že jsem tuto navrhovanou občanskou iniciativu podpořil/a
pouze jednou. ISO kód Identifikační položka Identifikační položky Pokud není vaše země ve výše uvedeném seznamu, není v ní tato
iniciativa aktivní. Prosím kontaktujte organizátory iniciativy. Info Iniciativa Nastavení iniciativy pro zemi Vlastník iniciativy Vlastníci iniciativ Iniciativy Úvod Neplatný znak ve vstupu. Jazyk Iso kód jazyka Jazyky Více info Název Jméno v místním jazyce Nová Žádná citlivá data nejsou nikde v systému uložena v nezašifrované podobě. Není to Vaše krajina? Počet občanů daného státu, kteří musí vyjádřit podporu, aby byla iniciativa akceptována. Naši administrátoři byli na problém upozorněni 
a budou se jeho nápravě věnovat v nejbližším možném termínu. Prosím zkuste tuto stránku později. Stránka nenalezena Ochrana osobních dat Prosím vyplňte následující položky vyžadované ve vaší zemi. Předregistrace Ochrana soukromí K zašifrování dat je využita asymetrická kryptografie, která zaručuje, že data bude moci rozšifrovat pouze organizátor iniciativy.
Klíč pro rozšifrování dat není přítomen nikde v systému samotném. Znovu nahrát Nahrazuje captchu Vyžadovaní poskytovatelé Chyba serveru Nastavit Je nám líto, ale stránka, kterou hledáte, neexistuje. Datum počátku Vyjádření podpory Vyjádření podpory Stav Odeslat Limit podpory Děkujeme za Váš zájem. Děkujeme za vaši podporu. Kontrola ochrany proti Cross-site request forgery selhala. Tato situace může být 
způsobena škodlivým kódem ve vašem prohlížeči nebo může být důsledkem vypnutí podpory
cookies.
Pokud máte vypnutu podporu cookies, zapněte ji prosím a znovu nahrajte stránku. Tato iniciativa je aktivní v následujících zemích. Prosím vyberte zemi
podle svého státního občanství. Organizátoři iniciativy požadují, aby data o uživateli byla
dodána od následujícího poskytovatele. V této chvíli žádná iniciativa nesbírá podporu. Při ukládání předregistrace došlo k chybě, zkuste to prosím znovu. Pokud si přejete být informováni ve chvíli,
kdy bude tato iniciativa aktivována, zanechte nám prosím váš email v následujícím formuláři.
        Tato iniciativa je již uzavřená. Již není možné odesílat
vyjádření podpory. Tato iniciativa není aktivní. V této chvíli není možné odesílat
vyjádření podpory. Toto je hlavní stránka systému pro sběr vyjádření podpory pro
Evropskou Občanskou Iniciativu.
Zvolte iniciativu, kterou chcete podpořit, z následujícího seznamu. Název Traceback: Uživatel Poskytovatel uživatelských údajů Poskytovatelé uživatelských údajů Omlouváme se za komplikace. Webová adresa v registru Komise Webová stránka Přístupnost stránek Jste přihlášen jako <a href="%(home_url)s">%(username)s</a>. Tyto informace vidíte, protože jste zalogován jakou interní uživatel '%(username)s'. Můžete použít tento odkaz k návratu na
<a href="%(website)s">stránku iniciativy</a>. Můžete použít tento odkaz k návratu na
<a href="%(website)s">stránku iniciativy</a>. Na <a href="%(homepage)s">domovské stránce</a> najdete
seznam právě aktivních iniciativ. Vaše země Váš email by úspěšně uložen v naší databázi. Dáme vám vědět, jakmile bude tato iniciativa
otevřena ke sběru vyjádření podpory. Informace o Vás Váš výběr Vaše prohlášení nemohlo být uloženo kvůli chybě na straně serveru.
Naši administrátoři byli informováni a pokusí se problém co nejdříve odstranit. Vaše vyjádření podpory bylo uloženo. 