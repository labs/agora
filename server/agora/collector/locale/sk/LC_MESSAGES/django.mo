��    z      �  �   �      H
  �   I
  .  ?  �   n  �   1  �    �   �  J   o  �   �  �   v  /   Q     �  "   �  C   �  *   �          ,     J     j     �      �     �      �  P   �     B  	   I     S     f     �     �     �  	   �     �     �     �  	   �     �               *     B     K     Q     o       
   �  (   �     �     �  �   �          �     �  w   �     +  
   0     ;     W     h     z     �     �     �     �  	   �  	   �     �     �     �  B   �     6  V   H  q   �             =   9     w     �  �   �     6     =     N     a     n  2   r  
   �     �     �     �     �     �     �       �   0  h   (  i   �  6   �  D   2  �   w  \     \   p  �   �     }  
   �     �     �     �  #   �  "   �           	   =      P   ]   T   �   T   !  a   X!     �!  �   �!     Z"     k"  �   z"  %   !#  �  G#  �   �$  =  �%  �   '  �   �'  �  �(  �   b*  O   .+  �   ~+  �   D,  9   4-     n-     {-  K   �-  +   �-     .  '   %.  -   M.  ,   {.     �.  '   �.     �.      �.  ]   /  	   z/     �/     �/     �/  
   �/     �/     �/     �/     �/     0     0     )0     20     J0     P0     h0     �0     �0      �0     �0     �0     �0  -   �0     1     61  �   I1     �1     2     2  �   32     �2     �2  "   �2     �2     3     #3     /3     53     O3     U3     e3  	   l3     v3     }3     �3  S   �3     �3  c   4  �   k4     
5     5  F   95     �5     �5  �   �5     |6     �6     �6     �6  	   �6  ;   �6     7     !7     47     G7     L7     U7     c7     7    �7  z   �8  o   49  6   �9  H   �9  �   $:  S   �:  `   ;  �   m;     <  
   <     *<  '   6<  (   ^<  $   �<  !   �<     �<     �<  =   �<  j   4=  [   �=  [   �=  ^   W>     �>  �   �>     Y?     l?  �   y?  (   @        ^   ;   z                8       P   J       g           ]              '         :       W       T   e           5       h   I       -      N   6       q   H   %   3          F   b       U   #             ,      [   @   s   B   O                      M   
                 i   Y   A       p   Z   ?               _              	       d   *   G   >   a          (   $   .       f   1          \       m   K   w   j   S   Q   0          <   o      u      2   D   y       x   "   7   X   V       =   C       4   k         R   t   &   L   +       r          )          /   `   !   n   E   9             l      c   v    
Because of the Cross Site Request Forgery (CSRF) protection requirement of the 
European Citizens' Initiative legislation, this website requires cookies to be
enabled. Without cookie support, it is not possible to submit statements of support.
 
For more information about the European Citizens' Initiative in general,
visit its <a href="%(agora_info_page)s">official website</a>.
For more information about the software running on this server, its security and
handling of personal data, please visit our <a href="%(about_page)s">about page</a>.
 
If you wish, you can try to resubmit your support statement later on the
<a href="%(back_url)s">support statement page</a> or you can return to the
<a href="%(website)s">initiative website</a>. 
One of the most important requirements on a system for collection of statements
of support for the European Citizens' Initiative is protection of personal data.
This site implements all protection required by relevant legislation:
 
The software used to collect statements of support on this website was created
in the <a href="%(labs_page)s">CZ.NIC Labs</a>, a research and development department of the .cz domain 
registry <a href="%(cznic_page)s">CZ.NIC</a>. It fully conforms to all legislative requirements of the 
European Union for such a system and its source code is made 
<a href="%(proj_page)s">available for public review</a>.
 
This website was created to conform to 
<a href="http://www.w3.org/WAI/">Web Accessibility Initiative (WAI)</a>
accessibility rules and related Czech website accessibility regulations. 
 
You can also return to the <a href="%(website)s">initiative website</a>.
 
You do not have cookies enabled or allowed. This page requires cookies to 
function properly. Please turn cookies on and return to the 
<a href="%(back_url)s">support statement page</a>. 
You do not have cookies enabled or allowed. This page requires cookies to 
function properly. Please turn cookies on and then either reload this page manually or
follow <a href="%(back_url)s">this link</a> for reload. <a href="%(homepage)s">Back to the homepage</a> About About support statement collection All sensitive user data are encrypted immediately after submission. An error occurred when preparing this page CSRF check failure Can advance initiative status Can change initiative code name Can change initiative dates Can change initiative status Can prematurely close initiative Certificate Certificate for '%(initiative)s' Certificate whose public key will be used to encrypt data of support statements. Closed Code name Collecting support Commission registration number Confirmation Cookie error Cookies are not enabled Countries Country Country name Current initiatives Data type Detected from IP address Email Encryption certificate Encryption certificates End date Error European citizens' initiative Exception type: Exception value: Extra info Forbidden: this initiative is not active Go back and change it Homepage I hereby certify that the information that I have provided
in this form is correct and that I have only supported this proposed
citizens' inititative once. ISO code Identification field Identification fields If your country is not on the list above, the initiative is
not active here.<br />Please contact initiative organizers. Info Initiative Initiative country settings Initiative owner Initiative owners Initiatives Introduction Invalid character in input. Language Language iso code Languages More info Name Native name New No sensitive data are stored in plain form anywhere in the system. Not your country? Number of citizens from country who must support the initiative for it to be accepted. Our admins were notified about the problem and will
attend to it as soon as possible. Please try this link later. Page not found Personal data protection Please fill in the following fields required in your country. Preregistration Privacy Policy Public-key cryptography is used to ensure only initiative
organizers are able to decrypt user data. Decryption key is not stored anywhere
in the system itself. Reload Replaces captcha Required providers Server error Set Sorry, the page you are looking for was not found. Start date Statement of support Statements of support Status Submit Support limit Thank you for your interest! Thank you for your support! The Cross-site request forgery protection check
failed. This may be either caused by a malicious code running
in your browser or may result from cookies being turned off.
If you do not have cookies enabled, please turn them on and reload
the page. The initiative is active in the following countries.<br />
Select country according to your citizenship. The initiative organizers require that user data for your
country are supplied by the following provider. There are no initiatives currently collecting support. There was an error saving your preregistration, please try it again. This initiative does not collect support yet. If you wish
to be informed once this initiative becomes active, please leave us your
email in the form below. This initiative is already closed. It is no longer possible
to submit statements of support. This initiative is not active. It is currently not possible
to submit statements of support. This is the main page of a system for collection of statements
of support for the European Citizens' Initiative. Choose the initiative you want
to support from the list below. Title Traceback: User User data provider User data providers We apologize for the inconvenience. Web address in Commission register Website Website accessibility You are logged in as <a href="%(home_url)s">%(username)s</a>. You are seeing this info because you are logged in as staff user '%(username)s'. You can use this link
to return to the <a href="%(website)s">initiative website</a>. You can use this link to return
to the <a href="%(website)s">initiative website</a>. You can visit the <a href="%(homepage)s">homepage</a>
for a list of currently active initiatives. Your country Your email address was successfully stored in our
database. We will let you know once this initiative is open
for statement of support collection. Your information Your selection Your statement could not be saved due to an error on the
server side. Our administrators were notified and we will do our best to fix
the problem as soon as possible. Your statement of support was stored. Project-Id-Version: git
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-10-10 15:04+0200
PO-Revision-Date: 2012-10-31 14:58+0100
Last-Translator: Beda Kosata <bedrich.kosata@nic.cz>
Language-Team: Czech <>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.0
 
S ohľadom na ochranu proti Cross Site Request Forgery (CSRF) požadovanú legislatívou
o Európskej Občianskej Iniciatíve, vyžadujú tieto stránky podporu pre cookies. Bez podpory cookies nie je možné odoslať vyjadrenie podpory.
 
Pre viac informácií o Európskej Občianskej Iniciatíve všeobecne navštívte jej <a href="%(agora_info_page)s">oficiálne stránky</a>.
Viac informácií o software, ktorý tento server využíva, jeho bezpečnosti a spracovaní osobných dát nájdete na stránke <a href="%(about_page)s">o zbere podpory</a>.
 
Vaše vyjadrenie podpory môžete neskôr znova skúsiť odoslať
na <a href="%(back_url)s">stránke vyjadrenia podpory</a> alebo sa môžete vrátiť
na <a href="%(website)s">webové stránky iniciatívy</a>. 
Jeden z najdôležitejších požiadavkov na systém pre zber vyjadrení podpory
pre Európsku Občiansku iniciatívu je ochrana osobných údajov.
Tieto stránky implementujú všetku ochranu vyžadovanú príslušnou legislatívou:
 
Software používaný pre zber vyjadrení podpory na týchto stránkach bol vytvorený
v <a href="%(labs_page)s">Laboratóriách CZ.NIC</a>, vývojovom oddelení 
doménového registra pre .cz <a href="%(cznic_page)s">CZ.NIC</a>. Systém plne splňuje legislatívne podmienky Európskej Únie pre takéto systémy a jeho zdrojové kódy sú k <a href="%(proj_page)s">dispozícii verejnosti k posúdeniu</a>. 
 
Tieto stránky boli vytvorené podľa doporučení
<a href="http://www.w3.org/WAI/">Web Accessibility Initiative (WAI)</a>
pre prístupný web a podľa súvisejúcej českej vyhlášky o prístupnosti.
 
Môžete sa tiež vrátiť na <a href="%(website)s">stránku iniciatívy</a>.
 
Nemáte zapnuté alebo povolené cookies. Táto stránka ich pre svoju správnu
funkciu vyžaduje. Prosím zapnite cookies a vráťte sa na 
<a href="%(back_url)s">stránku vyjadrenia podpory</a>. 
Nemáte zapnuté alebo povolené cookies. Táto stránka ich pre svoju správnu
funkciu vyžaduje. Prosím zapnite cookies a obnovte manuálne stránku alebo použite 
<a href="%(back_url)s">tento odkaz</a> k jej opätovnému načítaniu. <a href="%(homepage)s">Naspäť na domovskú stránku</a> O aplikácii O zbere vyjadrenia podpory Všetky citlivé údaje užívateľov sú zašifrované hneď po odoslaní. Pri príprave tejto stránky došlo k chybe Zlyhanie kontroly CSRF Môže posunúť vpred stav iniciatívy Môže meniť kódové označenie iniciatívy Môže meniť začiatok a koniec iniciatívy Môže meniť stav iniciatívy Môže predčasne uzavrieť iniciatívu Certifikát Certifikát pre '%(initiative)s' Certifikát ktorého verejný kľúč bude použitý k zašifrovaniu dát vyjadreniu podpory. Uzavrené Kódové označenie Zbiera podporu Registračné číslo Komisie Potvrdenie Chyba cookie Cookies sú vypnuté Krajiny Krajina Meno krajiny Prebiehajúce iniciatívy Typ dát Detekované z IP adresy Email Šifrovací certifikát Šifrovacie certifikáty Dátum ukončenia Chyba Európska občianska iniciatíva Typ výnimky: Hodnota výnimky: Extra informácie Zakázané: táto iniciatíva nie je aktívna Choďte späť a zmeňte ju Domovská stránka Týmto vyhlasujem, že informácie, ktoré som uviedol/uviedla v tomto formulári, sú pravdivé 
a že túto navrhovanú iniciatívu občanov som podporil/podporila iba raz. ISO kód Identifikačná položka Identifikačné položky Pokiaľ Vaša krajina nie je v hore uvedenom zozname, nie je v nej táto
iniciatíva aktívna. Prosím kontaktujte organizátorov iniciatívy. Info Iniciatíva Nastavenie iniciatívy pre krajinu Vlastník iniciatívy Vlastníci iniciatívy Iniciatívy Úvod Neplatný znak vo vstupe. Jazyk Iso kód jazyka Jazyky Viac info Názov Meno v miestnom jazyku Nová Žiadne citlivé dáta nie sú nikde v systéme uložené v nezašifrovanej podobe. Nie je to Vaša krajina? Počet občanov daného štátu, ktorí musia vyjadriť podporu, aby bola iniciatíva akceptovaná. Naši administrátori boli na problém upozornení 
a budú sa venovať jeho náprave v najbližšom možnom termíne. Prosím skúste túto stránku neskôr. Stránka nenájdená Ochrana osobných údajov Prosím vyplňte nasledujúce položky vyžadované vo Vašej krajine. Predregistrácia Ochrana súkromia K zašifrovaniu dát je použitá asymetrická kryptografia, ktorý zaručuje, že dáta bude môcť rozšifrovať len organizátor iniciatívy.
Kľúč pre rozšifrovanie nie je nikde v systéme samotnom prítomný. Znovu načítať Nahradzuje captchu Vyžadovaní poskytovatelia Chyba serveru Nastaviť Je nám ľúto, ale stránka, ktorú hľadáte, neexistuje. Dátum začiatku Vyjadrenie podpory Vyjadrenia podpory Stav Odoslať Limit podpory Ďakujeme za Váš záujem. Ďakujeme za Vašu podporu. Kontrola ochrany proti Cross-site Request Forgery zlyhala. Táto situácia môže byť 
spôsobená škodlivým kódom vo Vašom prehliadači alebo môže byť dôsledkom vypnutia podpory cookies.
Pokiaľ máte vypnutú podporu cookies, zapnite ju prosím a znova načítajte stránku. Táto iniciatíva je aktívna v nasledujúcich krajinách. Prosím vyberte krajinu
podľa svojej štátnej príslušnosti. Organizátori iniciatívy požadujú, aby dáta o užívateľovi boli
dodané od nasledujúceho poskytovateľa. V tejto chvíli žiadna iniciatívna nezbiera podporu. Pri ukladaní predregistrácie došlo k chybe, skúste to prosím znova. Ak si prajete byť informovaní vo chvíli,
kedy budetáto iniciatíva aktivovaná, zanechajte nám prosím Váš email v nasledujúcom formulári. Táto iniciatíva je uzatvorená. Už nie je možné odosielať
vyjadrenia podpory. Táto iniciatíva nie je aktívna. V tejto chvíli nie je možné odosielať
vyjadrenia podpory. Toto je hlavná stránka systému pre zber vyjadrení podpory pre
Európsku Občiansku Iniciatívu.
Vyberte iniciatívu, ktorú chcete podporiť, spomedzi nasledujúcich. Názov Traceback: Užívateľ Poskytovateľ užívateľských údajov Poskytovatelia užívateľských údajov Ospravedlňujeme sa za komplikácie. Webová adresa v registri Komisie Webová stránka Prístupnosť stránok Ste prihlásený ako <a href="%(home_url)s">%(username)s</a>. Tieto informácie vidíte, pretože ste zalogovaný ako interný privilegovaný užívateľ'%(username)s'. Môžete použiť tento odkaz k návratu na
<a href="%(website)s">stránku iniciatívy</a>. Môžete použiť tento odkaz k návratu na
<a href="%(website)s">stránku iniciatívy</a>. Na <a href="%(homepage)s">domovskej stránke</a> nájdete
zoznam práve aktívnych iniciatív. Vaša krajina Váš email bol úspešne uložený v našej databázi. Dáme vám vedieť, akonáhle bude táto iniciatíva
otvorená k zberu vyjadrení o podpore. Informácie o Vás Váš výber Vaše prehlásenie nemohlo byť uložené kvôli chybe na strane serveru.
Naši administrátori boli informovaní a pokúsia sa problém čo najskôr odstrániť. Vaše vyjadrenie podpory bolo uložené. 