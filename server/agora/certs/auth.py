# encoding: utf-8

#   certs is a Django application for client certificate authentication
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>


# django imports
from django.contrib.auth.models import User
from models import ClientCertificate


class ClientCertAuthenticationBackend():
    
    def authenticate(self, certificate_fp=None):
        if certificate_fp is not None:
            try:
                cert_obj = ClientCertificate.objects.select_related('user')\
                                                    .get(cert_fp=certificate_fp)
            except ClientCertificate.DoesNotExist:
                return None
            return cert_obj.user
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None