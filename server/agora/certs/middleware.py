# encoding: utf-8

#   certs is a Django application for client certificate authentication
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in stuff
import logging
import base64
import hashlib

# django imports
from django.contrib.auth import authenticate, login

# top-level stuff
logger = logging.getLogger("certs.middleware")


class ClientCertMiddleware():
    
    FP_HASH = hashlib.sha1
    CERT_START = "-----BEGIN CERTIFICATE-----\n"
    CERT_END = "\n-----END CERTIFICATE-----"
    
    @classmethod
    def calc_cert_fp(cls, cert):
        start = cert.find(cls.CERT_START)
        end = cert.find(cls.CERT_END)
        if start >= 0 and end >= 0:
            cert64 = cert[start+len(cls.CERT_START):end+len(cls.CERT_END)]
            cert_bin = base64.b64decode(cert64)
            hasher = cls.FP_HASH()
            hasher.update(cert_bin)
            return hasher.hexdigest().upper()
        return None
        

    def process_request(self, request):
        if hasattr(request, '_req'):
            req = request._req
            try:
                request.client_cert = req.ssl_var_lookup("SSL_CLIENT_CERT")
            except Exception:
                pass
        else:
            request.client_cert = None
            
        if request.client_cert:
            cert_fp = self.calc_cert_fp(request.client_cert)
            logger.debug("Client cert fp: %s", cert_fp)
            user = authenticate(certificate_fp=cert_fp)
            if user:
                logger.debug("Found user: %s", user)
                request.user = user
                login(request, user)
        return None
