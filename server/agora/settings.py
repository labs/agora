# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

"""
Django settings for Agora project.
Most of the default values can and should be overridden in settings_local.py
rather than in this module. Please see the INSTALL.rst document for more
information about adjusting the settings
"""

import sys
import os


DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': '', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '',   # Or path to database file if using sqlite3.
        'USER': '',   # Not used with sqlite3.
        'PASSWORD': '',  # Not used with sqlite3.
        'HOST': '',      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',      # Set to empty string for default. Not used with sqlite3.
    },
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Prague'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True


# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
ABS_CUR_DIR = os.path.abspath(os.path.dirname(__file__))
MEDIA_ROOT = os.path.join(os.path.dirname(ABS_CUR_DIR), "media") + "/"

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(os.path.dirname(ABS_CUR_DIR), "static") + "/"


# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
#    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '**** CHANGE ME IN settings_local.py ****'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
#    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'agora.backstage.middleware.FingerprintingSessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'agora.certs.middleware.ClientCertMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

AUTHENTICATION_BACKENDS = (#'agora.certs.auth.ClientCertAuthenticationBackend',
                           'django.contrib.auth.backends.ModelBackend',)

ROOT_URLCONF = 'agora.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'transmeta',
    'captcha',
    'agora.certs',
    'agora.collector',
    'agora.backstage',
    'agora.mojeid',
)

SESSION_COOKIE_NAME = "agorasession"
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_AGE = 18000 # 5 hours, for admins, this is shortened to 15 min
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
CSRF_FAILURE_VIEW = "agora.collector.views.csrf_failure"
CSRF_COOKIE_NAME = "agoracsrftoken"

# model specifying additional user data
AUTH_PROFILE_MODULE = 'backstage.UserProfile'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(name)s [%(process)d] %(message)s'
        },
        'timed': {
            'format': '%(asctime)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            #'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'audit_log':{
            'level':'INFO',
            'class':'logging.handlers.SysLogHandler',
            'address': '/dev/log',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django': {
            'handlers':['console'],
            'propagate': True,
            'level':'INFO',
        },       
        'django.request': {
            'handlers': ['mail_admins','audit_log'],
            'level': 'ERROR',
            'propagate': True,
        },
        'agora.audit': {
            'handlers': ['audit_log'],
            'level': 'INFO',
            'propagate': True,
        },
        'agora.backstage': {
            'handlers': ['audit_log','mail_admins'],
            'level': 'INFO',
            'propagate': True,
        },
                        
    }
}

LOGGING_OVERRIDE = {} # this can be set in settings_local to adjust logging

FORENSIC_LOG = "/var/log/agora_forensic.log"

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'agora-cache'
    }
}
#CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True 

# LANGUAGES
gettext = lambda s: s
# All languages of the EU (Maltese is turned off because Django does not have
# a base translation for it)
# This list may be used to define LANGUAGES later on and is also used for
# documentation purposes.
# Only LANGUAGES below is really used by Agora
EU_LANGUAGES = (
    ("bg", gettext("Bulgarian")),
    ("cs", gettext("Czech")),
    ("da", gettext("Danish")),
    ("de", gettext("German")),
    ("el", gettext("Greek")),
    ("en", gettext("English")),
    ("es", gettext("Spanish")),
    ("et", gettext("Estonian")),
    ("fi", gettext("Finnish")),
    ("fr", gettext("French")),
    ("ga", gettext("Irish")),
    ("hu", gettext("Hungarian")),
    ("it", gettext("Italian")),
    ("lt", gettext("Lithuanian")),
    ("lv", gettext("Latvian")),
#    ("mt", gettext("Maltese")),
    ("nl", gettext("Dutch")),
    ("pl", gettext("Polish")),
    ("pt", gettext("Portuguese")),
    ("ro", gettext("Romanian")),
    ("sk", gettext("Slovak")),
    ("sl", gettext("Slovene")),
    ("sv", gettext("Swedish")),
)

# currently supported (having a translation) languages
# you can override this in settings_local.py
LANGUAGES = (
    ("cs", gettext("Czech")),
#    ("de", gettext("German")),
    ("en", gettext("English")), 
    ("sk", gettext("Slovak")),
)

# CAPTCHA settings
# CAPTCHA_BACKEND is one of 'django-simple-captcha', 'django-recaptcha'
CAPTCHA_BACKEND = "django-simple-captcha"

# Recaptcha specific options - 
# only used the django-recaptcha is the CAPTCHA_BACKEND
RECAPTCHA_USE_SSL = True
# the following should be overriden in settings_local.py
RECAPTCHA_PUBLIC_KEY = ''
RECAPTCHA_PRIVATE_KEY = ''

# django-simple-captcha specific options
# path to flite - for audio captcha support
CAPTCHA_FLITE_PATH = "/usr/bin/flite"
CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.random_char_challenge'

# encryption settings
ENC_SYMMETRIC_CIPHER = "aes_256_cbc"
ENC_KEY_LIFETIME = 10

# mojeID integration settings
MOJEID_ENFORCE_ACCOUNT_STATUS = True 

# URL prefix for static files.
# This may be overridden in settings_local.py
STATIC_URL = '/static/'

# custom login URL
LOGIN_URL = "/b/login/"

# local settings
try:
    from settings_local import *
except ImportError:
    print >> sys.stderr, "Local settings were not found - using the defaults."
    print >> sys.stderr, "This is probably an error!"


# URL prefix for admin static files -- CSS, JavaScript and images.
ADMIN_MEDIA_PREFIX = STATIC_URL+'admin/'

# adjust logging if specified in settings_local
for key, value in LOGGING_OVERRIDE.iteritems():
    if type(value) is dict and key in LOGGING:
        for subkey, subvalue in value.iteritems():
            LOGGING[key][subkey] = subvalue
    else:
        LOGGING[key] = value

if DEBUG:
    try:
        import devserver
    except ImportError:
        pass
    else:
        INSTALLED_APPS += ('devserver',)

# secure cookies - based on debug options
if not DEBUG:
    # only allow insecure cookies in debug mode
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True