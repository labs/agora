# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

"""
This module contain a minimalistic dummy data provider which both
demonstrates how a provider works and also supplies a simple prototype
for development.
"""

from django.http import HttpResponseRedirect

class views:
    """
    The data provider interface expects 'views.start' to be available in the
    provider module and calls it with the following attributes
    Here we simulate the views with a simple class with a static method
    """
    @classmethod
    def start(cls, request, return_to, requested_attrs):
        to_session = {'provider_result_ok': True,
                      'data': {"given_name": "John", "family_name": "Smith"}
                      }
        in_session = request.session.get('dummy', {})
        in_session[return_to] = to_session
        request.session['dummy'] = in_session
        return HttpResponseRedirect(return_to)