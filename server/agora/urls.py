# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.contrib import admin
admin.autodiscover()

from agora.backstage.forms import StrongPasswordChangeForm

URL_BASE_RE = r"^"

handler500 = 'agora.collector.views.error500'

urlpatterns = patterns('',
    # Examples:
    url(URL_BASE_RE, include('agora.collector.urls')),
    url(URL_BASE_RE+"b/", include('agora.backstage.urls')),
    url(URL_BASE_RE+'mojeid/', include('agora.mojeid.urls')),
    
    url(URL_BASE_RE+'i18n/', include('django.conf.urls.i18n')),

    # override the password change page with a password validating form
    # so that the password requirements are enforced
    url(URL_BASE_RE+'admin/password_change/$', 
        'django.contrib.auth.views.password_change',
        {'password_change_form': StrongPasswordChangeForm}),
    url(URL_BASE_RE+'admin/password_changed/done/$',
        'django.contrib.auth.views.password_change_done'),
    # Django admin:
    url(URL_BASE_RE+'admin/', include(admin.site.urls)),
    
    (URL_BASE_RE+'media/(?P<path>.*)$', 'django.views.static.serve',
                                       {'document_root': '../media'}),
    (URL_BASE_RE+'static/(?P<path>.*)$', 'django.views.static.serve',
                                       {'document_root': 'collector/static'}),
)
