# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

from django.conf.urls.defaults import patterns
import views

urlpatterns = patterns('',
    (r'^$', views.top),
    (r'^end/$', views.finish_mojeid),
    (r'^create/$', views.create_mojeid),
    (r'^xrds.xml$', views.xrds),
)
