��          �            h     i     r  2   �  p   �  �   $  #   �  1   �       "        B  j   I  *   �     �     �  �  �     �     �  0   �  �   �  �   r  4     &   ;     b  &   ~     �  y   �  +   (     T     a               
                                               	                  Continue Create mojeID Information about account status was not supplied. Initiative information is mission from session, cookies were disabled or reset during communication with mojeID. Insufficient account validation - account must be either fully identified or validated to be usable for statement of support collection. No data were found in mojeID reply. Retreival of data from mojeID was not successful. Return back to the homepage Return back to the submission page Submit This page will redirect you to mojeID. If it does not do so
automatically, please use the continue button. You did not provide all the required data. mojeID error mojeID redirection Project-Id-Version: git
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-10-15 08:54+0200
PO-Revision-Date: 2012-11-09 11:31+0100
Last-Translator: Beda Kosata <bedrich.kosata@nic.cz>
Language-Team: Czech <>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.0
 Pokračovať Založiť mojeID Neboli predané informácie o stupni validácie. V relácii (session) chýbajú informácie o iniciatíve - v priebehu komunikácie s mojeID boli pravdepodobne vypnuté alebo vymazané cookies. Nedostatočná validácia účtu - účet musí byť plne identifikovaný alebo validovaný, aby mohol byž použitý pri zbere vyjadrení podpory. V odpovedi od mojeID neboli nájdené žiadne dáta. Prenos dát z mojeID nebol úspešný. Návrat na hlavnú stránku Návrat na stránku vyjadrenia podpory Odoslať Táto stránka vás presmeruje do mojeID. Pokiaľ sa tak nestane
automaticky, stlačte prosím tlačítko "Pokračovať". Neposkytli ste všetky požadované údaje. Chyba mojeID Presmerovanie do mojeID 