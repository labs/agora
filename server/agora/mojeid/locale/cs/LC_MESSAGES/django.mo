��          �            h     i     r  2   �  p   �  �   $  #   �  1   �       "        B  j   I  *   �     �     �  x  �     x     �  -   �  }   �  �   @  1   �  '     #   -  0   Q     �  y   �  -        2     ?               
                                               	                  Continue Create mojeID Information about account status was not supplied. Initiative information is mission from session, cookies were disabled or reset during communication with mojeID. Insufficient account validation - account must be either fully identified or validated to be usable for statement of support collection. No data were found in mojeID reply. Retreival of data from mojeID was not successful. Return back to the homepage Return back to the submission page Submit This page will redirect you to mojeID. If it does not do so
automatically, please use the continue button. You did not provide all the required data. mojeID error mojeID redirection Project-Id-Version: git
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-10-15 08:54+0200
PO-Revision-Date: 2012-11-09 11:30+0100
Last-Translator: Beda Kosata <bedrich.kosata@nic.cz>
Language-Team: Czech <>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 Pokračovat Založit mojeID Nebyly předány informace o stupni validace. V relaci chybí informace o iniciativě - v průběhu komunikace s mojeID byly pravděpodobně vypnuty nebo smazány cookies. Nedostatečná validace účtu - účet musí být plně identifikovaný nebo validovaný, aby mohl být použit při sběru vyjádření podpory. V mojeID odpovědi nebyla nalezena žádná data. Přenos dat z mojeID nebyl úspěšný. Návrat zpátky na hlavní stránku Návrat zpátky na stránku vyjádření podpory Odeslat Tato stránka vás přesměruje do mojeID. Pokud se tak nestane
automaticky, stiskněte prosím tlačítko "Pokračovat". Neposkytl(a) jste všechna požadovaná data. Chyba mojeID Přesměrování do mojeID 