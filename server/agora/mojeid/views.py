# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in imports
import logging
import struct
import M2Crypto
import base64
import time

# django imports
from django.db import connection
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

# openid imports
from openid.store.filestore import FileOpenIDStore
from openid.store import sqlstore

from openid.consumer import consumer
from openid.consumer.discover import OPENID_IDP_2_0_TYPE, OPENID_2_0_TYPE,\
        OPENID_1_1_TYPE, OPENID_1_0_TYPE, OpenIDServiceEndpoint
from openid.extensions import ax, pape
from openid.yadis.constants import YADIS_CONTENT_TYPE
from openid.yadis.manager import Discovery

SERVICE_NS = [
    OPENID_IDP_2_0_TYPE,
    OPENID_2_0_TYPE,
    OPENID_1_1_TYPE,
    OPENID_1_0_TYPE,
]

# include 1.x OpenID server service types
OpenIDServiceEndpoint.openid_type_uris = SERVICE_NS

AGORA_TO_MOJEID_ATTR_REMAP = {
    'given_name': 'http://specs.nic.cz/attr/contact/name/first',
    'family_name': 'http://specs.nic.cz/attr/contact/name/last',
    }

MOJEID_TO_AGORA_REMAP = dict([(v,k)
                              for k,v in AGORA_TO_MOJEID_ATTR_REMAP.items()])

ACCOUNT_STATUS_ATTR = "http://specs.nic.cz/attr/contact/status"

# the following must be present in the answer in order for it to be passed on
REQUIRED_ATTRS = set(['given_name', 'family_name'])

# remap between type of document and its code in Agora
# we cannot remap this directly in MOJEID_TO_AGORA_REMAP because
# we store id_type and id_number as two fields, while in mojeID its in one
ID_DOC_REMAP = {'http://specs.nic.cz/attr/contact/ident/card': "CZ01",
                'http://specs.nic.cz/attr/contact/ident/pass': "CZ02",
                }

# this key will this module use to store intermediate stuff in session
SESSION_KEY = "mojeid_params"
# under this name data are put into the session for Agora to use them
PROVIDER_KEY = "mojeid"

PAPE_POLICIES = [
    'AUTH_PHISHING_RESISTANT',
    'AUTH_MULTI_FACTOR',
    'AUTH_MULTI_FACTOR_PHYSICAL',
]

# List of (name, uri) for use in generating the request form.
POLICY_PAIRS = [(p, getattr(pape, p))
                for p in PAPE_POLICIES]

MOJEID_EMPTY_PAGE = '''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="x-xrds-location" content="%s" />

    <title>Agora mojeID endpoint</title>
</head>
<body>
<p>This is mojeID endpoint.</p>
</body>
</html>'''


def xrds(request):
    xrds = '''<?xml version="1.0" encoding="UTF-8"?>
<xrds:XRDS xmlns:xrds="xri://$xrds" xmlns:openid="http://openid.net/xmlns/1.0"
    xmlns="xri://$xrd*($v*2.0)">
    <XRD>
        <Service>
            <Type>http://specs.openid.net/auth/2.0/return_to</Type>
            <URI>%s</URI>
        </Service>
    </XRD>
</xrds:XRDS>''' % request.build_absolute_uri(reverse(top))
    response = HttpResponse(xrds)
    response['Content-Type'] = YADIS_CONTENT_TYPE
    return response


def top(request):
    ret = MOJEID_EMPTY_PAGE % request.build_absolute_uri(reverse(xrds))
    return HttpResponse(ret)


def start(request, return_to, requested_attrs):
    """
    request_id specifies a key under which the resulting user data should
    be stored in a session. It is also a key under which the URL to which
    this module should return after it finished.
    """
    # at first generate a request_id which will allow us to find our way
    # back to return_to once we return from mojeID
    rand_part = M2Crypto.m2.rand_bytes(10)
    time_part = struct.pack("!d", time.time())[:6]
    request_id = base64.b16encode(rand_part + time_part)
    params = request.session.get(SESSION_KEY, {})
    params[request_id] = return_to
    request.session[SESSION_KEY] = params
    return start_mojeid(request, request_id, requested_attrs)


def start_mojeid(request, request_id, requested_attrs):
    if SESSION_KEY not in request.session:
        raise Exception("No '%s' value in session." % SESSION_KEY)
    openid_url = 'https://mojeid.cz/endpoint'
    # create consumer instance
    cons = _get_consumer(request)
    
    if request.session.has_key(Discovery.PREFIX + cons.session_key_prefix):
        del request.session[Discovery.PREFIX + cons.session_key_prefix]

    OpenIDServiceEndpoint.openid_type_uris = SERVICE_NS
    auth_request = cons.begin(openid_url)
    # add list of requested attributes
    ax_request = ax.FetchRequest()
    for uri, required in _remap_agora_to_mojeid(requested_attrs):
        # remap requested attrs from Agora to mojeID
        ax_request.add(ax.AttrInfo(uri, required=required))
    # add status information request
    ax_request.add(ax.AttrInfo(ACCOUNT_STATUS_ATTR, required=required))
    auth_request.addExtension(ax_request)
    
    # Add PAPE request information.
    requested_policies = []
    for k, v in POLICY_PAIRS:
        requested_policies.append(v)
    if requested_policies:
        pape_request = pape.Request(requested_policies)
        auth_request.addExtension(pape_request)

    # Compute the trust root and return URL values to build the
    # redirect information.
    trust_root = _reverse_to_full_url(request, top)
    return_to = _reverse_to_full_url(request, finish_mojeid) + \
                                                "?request_id="+request_id
    # Send the browser to the server either by sending a redirect
    # URL or by generating a POST form.
    if auth_request.shouldSendRedirect():
        url = auth_request.redirectURL(trust_root, return_to)
        return HttpResponseRedirect(url)
    else:
        # Beware: this renders a template whose content is a form
        # and some javascript to submit it upon page load.  Non-JS
        # users will have to click the form submit button to
        # initiate OpenID authentication.
        form_id = 'openid_message'
        form_message = auth_request.getMessage(trust_root, return_to=return_to)
        form_html = form_message.toFormMarkup(auth_request.endpoint.server_url,
                                              form_tag_attrs={'id': form_id},
                                              submit_text=_("Continue"))
        return render(request,
                      'mojeid_request_form.html',
                      {'html': form_html})


@csrf_exempt
def finish_mojeid(request):
    """
    Finish the OpenID authentication process.  Invoke the OpenID
    library with the response from the OpenID server and render a page
    detailing the result.
    """
    request_args = dict([(k,v) for k,v in request.GET.iteritems()])
    request_id = request_args.get("request_id")
    if not request_id:
        return HttpResponse(MOJEID_EMPTY_PAGE % \
                            request.build_absolute_uri(reverse(xrds)))
    if request.method == 'POST':
        request_args.update(dict([(k,v) for k,v in request.POST.iteritems()]))
    # things stored in session
    provider_params = request.session.get(SESSION_KEY, {})
    return_url = provider_params.get(request_id)
    
    if request_args:
        cons = _get_consumer(request)
        return_to = _reverse_to_full_url(request, finish_mojeid)
        response = cons.complete(request_args, return_to)
        
        # process the response according to its status
        data = {}
        mojeid_ok = False
        problem = None
        if response.status == consumer.SUCCESS:
            # success - we can store the data into a session
            ax_items = {}
            ax_response = ax.FetchResponse.fromSuccessResponse(response)
            if ax_response:
                ax_items = ax_response.data
            data['provider_result_ok'] = True
            data['data'] = _remap_mojeid_to_agora(ax_items)
            account_status = ax_items.get(ACCOUNT_STATUS_ATTR, "")
            if account_status:
                account_status = account_status[0]
            if settings.MOJEID_ENFORCE_ACCOUNT_STATUS and not account_status:
                mojeid_ok = False
                problem = _("Information about account status was not "
                            "supplied.")
            elif settings.MOJEID_ENFORCE_ACCOUNT_STATUS and \
                 account_status not in ('IDENTIFIED', 'VALIDATED'):
                mojeid_ok = False
                problem = _("Insufficient account validation - account must be "
                            "either fully identified or validated to be usable "
                            "for statement of support collection.")
            elif _has_required_data(data['data']):
                mojeid_ok = True
            else:
                mojeid_ok = False
                problem = _("You did not provide all the required data.")
        else:
            mojeid_ok = False
            problem = _("Retreival of data from mojeID was not successful.")
        
        if mojeid_ok and not return_url:
            mojeid_ok = False
            problem = _("Initiative information is mission from session, "
                        "cookies were disabled or reset during communication "
                        "with mojeID.")
        if mojeid_ok:
            # put received data into the session
            provider_storage = request.session.get(PROVIDER_KEY, {})
            provider_storage[return_url] = data
            request.session[PROVIDER_KEY] = provider_storage
            # remove current temporary data from session
            del provider_params[request_id]
            # redirect to return_url
            return HttpResponseRedirect(return_url)
        else:
            # we display an error page with a possibility to redirect back
            # to the entry
            return render(request, "mojeid_error_page.html",
                          dict(problem=problem, return_url=return_url))
    problem = _("No data were found in mojeID reply.")
    return render(request, "mojeid_error_page.html",
                  dict(problem=problem, return_url=return_url))



def create_mojeid(request):
    """
    Called when user requests creation of mojeID account on the Agora
    support statement submission page
    """
    realm = request.build_absolute_uri(reverse(top))
    return render(request, 'create_mojeid.html', {"realm": realm})


def _get_consumer(request):
    return consumer.Consumer(request.session,
                             _get_openid_store(table_prefix="mojeid_"))


def _get_openid_store(filestore_path=None, table_prefix=None):
    if filestore_path:
        return FileOpenIDStore(filestore_path)
    elif table_prefix: 
        # Possible side-effect: create a database connection if one isn't
        # already open.
        connection.cursor()
        # Create table names to specify for SQL-backed stores.
        tablenames = {
            'associations_table': table_prefix + 'openid_associations',
            'nonces_table': table_prefix + 'openid_nonces',
            }
    
        s = sqlstore.PostgreSQLStore(connection.connection,
                                     **tablenames)
        try:
            s.cleanupNonces()
        except Exception as exc:
            # we do not want to import psycopg2, so we detect the type of 
            # error by its name
            if exc.__class__.__name__ == "ProgrammingError":
                # this is probably caused by the table to existing, we create it
                s.createTables()
            else:
                raise
        return s
    else:
        raise Exception("Either filestore_path or table_prefix must be set")
    

def _reverse_to_full_url(request, view):
    return request.build_absolute_uri(reverse(view))


def _remap_mojeid_to_agora(data):
    result = {}
    for key, value in data.iteritems():
        if value:
            if key in MOJEID_TO_AGORA_REMAP:
                result[MOJEID_TO_AGORA_REMAP[key]] = value[0]
            elif key in ID_DOC_REMAP:
                if value:
                    result['id_type'] = ID_DOC_REMAP[key]
                    result['id_number'] = value[0]
                else:
                    pass # we ignore it if there is empty value
            elif key == ACCOUNT_STATUS_ATTR:
                pass
            else:
                logging.warn("Unknow mojeID attribute %s (%s)", key, value)
        else:
            # we ignore empty values
            pass
    return result

def _remap_agora_to_mojeid(data):
    result = []
    for key in data:
        if key in AGORA_TO_MOJEID_ATTR_REMAP:
            result.append((AGORA_TO_MOJEID_ATTR_REMAP[key], True)) # required
        elif key == 'id_type':
            pass
        elif key == 'id_number':
            for key2 in ID_DOC_REMAP:
                result.append((key2, False))
        else:
            logging.warn("Unknown Agora attribute %s", key)
    return result

def _has_required_data(received):
    if set(received.keys()) & REQUIRED_ATTRS != REQUIRED_ATTRS:
        return False
    return True
