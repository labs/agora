# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in modules
import logging
from datetime import timedelta, date

# Django imports
from django.contrib import auth
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.forms.extras.widgets import SelectDateWidget

# third party imports
import M2Crypto

# local imports
from utils import validate_password_strength
from agora.collector import models


logger = logging.getLogger("backstage.forms")


class StrongPasswordChangeForm(auth.forms.PasswordChangeForm):
    """
    Password change form validating the new password and enforcing a strong
    password compatible with the ECI regulation requirements.
    """

    def clean_new_password1(self):
        password = self.cleaned_data.get('new_password1')
        return validate_password_strength(password)
    

class InitiativeCreationForm(forms.ModelForm):
    """
    Form for creation of a new initiative instance
    """
    year = date.today().year
    start_date = forms.DateField(label=_("Start date"),
                                 widget = SelectDateWidget(years=range(year-1,
                                                                       year+3)))
    end_date = forms.DateField(label=_("End date"),
                               widget = SelectDateWidget(years=range(year-1,
                                                                     year+4)))
        
    class Meta:
        model = models.Initiative
        exclude = ('status',)
        
    def clean_end_date(self):
        start_date = self.cleaned_data['start_date']
        end_date = self.cleaned_data['end_date']
        if end_date < start_date:
            raise forms.ValidationError(
                _("The end date must be after the start date."))
        if start_date + timedelta(days=365) < end_date:
            raise forms.ValidationError(
                _("The end date must be within one year of the start date."))
        if end_date < date.today():
            raise forms.ValidationError(
                _("The initiative cannot end before current date."))     
        return end_date

        
class InitiativeCertificateForm(forms.Form):
    """
    Form to add encryption certificate to an initiative
    """
    certificate_file = forms.FileField(label=_("Certificate file"))
    initiative_owners = forms.ModelMultipleChoiceField(
                            queryset=User.objects,
                            required=False,
                            label=_("Initiative owners"))
    
    def __init__(self, start_date, end_date, *args, **kwargs):
        super(InitiativeCertificateForm, self).__init__(*args, **kwargs)
        self.start_date = start_date
        self.end_date = end_date
    
    
    def clean_certificate_file(self):
        data = self.cleaned_data['certificate_file']
        cert = data.read()
        data.seek(0) # reset the stream to original state
        # try to read the cert
        if type(cert) == unicode:
            cert = cert.encode('ascii')
        try:
            cert = M2Crypto.X509.load_cert_string(cert,
                                                  M2Crypto.X509.FORMAT_PEM)
        except Exception as exc:
            logger.error("Invalid certificate file: %s", exc)
            raise forms.ValidationError(
                                _("The file is not a valid certificate!"))
        # check the certificate validity
        not_before = cert.get_not_before().get_datetime().date()
        not_after = cert.get_not_after().get_datetime().date()
        if self.start_date != None:
            max_start_date = max(date.today(), self.start_date)
            if not_before > max_start_date:
                # the cert should not start after the max_start_date - we permit
                # it to start its validity after the initiative start date, but
                # only up to current date - the cert must be valid when the
                # collection starts
                raise forms.ValidationError(
                    _("The certificate 'notBefore' field (%(not_before)s) is "
                      "later than the collection of electronic support starts "
                      "(%(max_start_date)s). This certificate cannot be used.")
                    % {"max_start_date": max_start_date,
                       "not_before": not_before})
        if self.end_date:
            min_end_date = self.end_date + timedelta(days=30)
            if not_after < min_end_date:
                # the cert should be valid at least 30 days after the collection
                # of support ends
                raise forms.ValidationError(
                    _("The certificate 'notAfter' field (%(not_after)s) should "
                      "extend at least 30 days after the desired end of "
                      "collection of support, that is at least till "
                      "%(min_end_date)s.") %\
                    {"min_end_date": min_end_date, "not_after": not_after})
        return data
    

class InitiativeCountryForm(forms.Form):
    """
    Form to select countries for which an initiative should be active
    """
    countries = forms.ModelMultipleChoiceField(
                        queryset=models.Country.objects.order_by("name_en"),
                        label=_("Countries"))
    

class InitiativeProviderForm(forms.Form):
    """
    Form for selection of data providers for selected countries
    """
    
    def __init__(self, countries, *args, **kwargs):
        super(InitiativeProviderForm, self).__init__(*args, **kwargs)
        for country in countries:
            providers = country.user_data_providers.all()
            if providers:
                choices = [(provider.code_name, provider.name)
                           for provider in providers]
                field_obj = forms.MultipleChoiceField(label=country.name,
                                                      choices=choices,
                                                      required=False)
                self.fields[country.iso_code] = field_obj