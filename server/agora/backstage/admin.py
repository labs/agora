# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# django imports
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib import admin
from agora.backstage.utils import validate_password_strength

class CustomUserCreationForm(UserCreationForm):
    
    def clean_password1(self):
        password = self.cleaned_data.get('password1')
        return validate_password_strength(password)


class CustomUserAdmin(UserAdmin):
    
    add_form = CustomUserCreationForm
    
    def _can_create_staff_user(self, user):
        if user.has_perm('backstage.create_staff_users'):
            return True
        return False
    
    def get_readonly_fields(self, request, obj=None):
        rofs = self.readonly_fields[:]
        if not self._can_create_staff_user(request.user):
            # only those with special permissions can create staff users
            rofs.append('is_staff')
        if not request.user.is_superuser:
            # only superuser can create superusers
            rofs.append('is_superuser')
        return rofs


    def save_model(self, request, obj, form, change):
        """
        Check permissions here for users who might get funny ideas about
        modifying data lowlevel
        """
        if not self._can_create_staff_user(request.user):
            # only those with special permissions can create staff users
            obj.is_staff = False
        if not request.user.is_superuser:
            obj.is_superuser = False
        obj.save()


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)