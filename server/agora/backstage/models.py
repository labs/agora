# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in modules
import logging
audit_logger = logging.getLogger("agora.audit") 

# django imports
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin.models import LogEntry
from django.db.models.signals import post_save


class UserProfile(models.Model):
    """
    Stores additional user information
    """
    
    class Meta:
        permissions = (
            ("create_staff_users", _("Can create staff users")),
        )
    
    user = models.ForeignKey(User)
    needs_password_change = models.BooleanField(default=True)
    
    def __unicode__(self):
        return _("User profile for '%s'") % self.user
    

def create_user_profile(sender, instance, created, **kwargs):
    if not UserProfile.objects.filter(user=instance).exists():
        UserProfile.objects.create(user=instance, needs_password_change=True)

post_save.connect(create_user_profile, sender=User)


def force_short_admin_session(sender, **kwargs):
    """
    make session short for admins - this is a requirement in the
    ECI legislation
    """
    request = kwargs.get('request')
    if request:
        if request.user.is_staff or request.user.is_superuser:
            request.session.set_expiry(900) # 15 minutes maximum

        
def log_user_login(sender, **kwargs):
    """
    Implements user login logging according to the ECI legislation
    """
    request = kwargs.get('request')
    if request:
        content_type = ContentType.objects.get_for_model(User)
        LogEntry.objects.log_action(request.user.pk, content_type.pk,
                                    request.user.pk, unicode(request.user),
                                    4, "Logged in")
        
def log_user_logout(sender, **kwargs):
    """
    Implements user logout logging according to the ECI legislation
    """
    request = kwargs.get('request')
    if request:
        content_type = ContentType.objects.get_for_model(User)
        LogEntry.objects.log_action(request.user.pk, content_type.pk,
                                    request.user.pk, unicode(request.user),
                                    5, "Logged out")
        

user_logged_in.connect(force_short_admin_session)
user_logged_in.connect(log_user_login)
user_logged_out.connect(log_user_logout)

def copy_logentry_to_audit_log(sender=None, instance=None, *args, **kwargs):
    if instance:
        audit_logger.info("User: %s, Object: %s (id=%s, type=%s.%s), Action: '%s'",
                         instance.user, instance.object_repr,
                         instance.object_id, instance.content_type.app_label,
                         instance.content_type.model,
                         instance.change_message)

post_save.connect(copy_logentry_to_audit_log, sender=LogEntry)
