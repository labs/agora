# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in modules
import logging
import string

# Django imports
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseForbidden, HttpResponseRedirect,\
                        HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin.models import LogEntry
from django.core.cache import cache
from django.utils.translation import get_language
from django.db.models import Count
from django.conf import settings
from django.contrib.formtools.wizard.views import SessionWizardView
from django.core.files.storage import DefaultStorage
from django.utils.translation import ungettext, ugettext_lazy as _

# local imports
import agora.collector.models as col_models
from agora.collector.views import no_go_back
import forms
import models
from agora.collector.tools import resolve_with_prefix

logger = logging.getLogger("agora.backstage")

# some settings
IP_ADDR_CHARS = set(string.hexdigits+":. ")
LOGIN_ATTEMPTS = 5
LOGIN_BLOCK_TIME = 300 # in seconds
LOGIN_ATTEMPTS_IP_ONLY = 50

# custom decorators
def no_stale_password(view_func):
    """
    Checks if the user has 'needs_password_change' set to true in user profile
    and if yes, then redirects him to the password change page.
    """
    def _wrapped_view(request, *args, **kwargs):
        try:
            profile = request.user.get_profile()
        except models.UserProfile.DoesNotExist:
            profile = models.UserProfile.objects.create(user=request.user)
        if profile.needs_password_change:
            return HttpResponseRedirect(reverse(password_change_page))
        return view_func(request, *args, **kwargs)
    return _wrapped_view
    

@login_required
@no_stale_password
@no_go_back
def user_page(req):
    try:
        print req.user.get_profile().needs_password_change
    except:
        req.user.save()
    initiatives = [iown.initiative
                   for iown in req.user.initiativeowner_set.all()]
    perms = {"add_initiative": req.user.has_perm("collector.add_initiative")}
    data = dict(initiatives=initiatives, perms=perms)
    return render(req, "user_page.html", data)


@login_required
@no_stale_password
@no_go_back
def initiative_overview(req, code_name):
    initiative, response = _get_initiative(req, code_name, mode='owner')
    if not initiative and response:
        # something was not ok
        return response
    # prepare the data for overview of support in individual countries
    country_support = []
    local_name_col = "country__name_{0}".format(get_language())
    total_sup = 0
    ready_countries = 0
    # counts for countries
    ccode_to_count = dict([(rec['country_id'], rec['pk__count'])
                           for rec in initiative.supportstatement_set.\
                                values("country_id").annotate(Count("pk"))])
    for ics in initiative.initiativecountrysettings_set.\
            select_related("country").\
            order_by(local_name_col, 'country__native_name').\
            all():
        supp_count = ccode_to_count.get(ics.country.pk, 0)
        perc = 100.0*supp_count/ics.country.support_limit
        country_support.append((ics.country, supp_count, perc))
        total_sup += supp_count
        if supp_count >= ics.country.support_limit:
            ready_countries += 1
    total = (total_sup, 1000000, 100.0*total_sup/1000000)
    
    data = dict(initiative=initiative, country_support=country_support,
                total=total, ready_countries=(ready_countries, 7))
    return render(req, "initiative_overview.html", data)


@login_required
@no_stale_password
@no_go_back
def initiative_creation_page(request):
    def show_provider_form(wizard):
        """this enables skipping of provider step if no country has a provider"""
        country_data = wizard.get_cleaned_data_for_step('2')
        if not country_data:
            return True
        countries = country_data.get('countries', [])
        for country in countries:
            if country.user_data_providers.all().exists():
                return True
        return False
    
    if not request.user.has_perm("collector.add_initiative"):
        return HttpResponseForbidden(
                    "Forbidden: You are not allowed to create initiatives.")
    view = InitiativeCreationWizard.as_view(
                [forms.InitiativeCreationForm,
                 forms.InitiativeCertificateForm,
                 forms.InitiativeCountryForm,
                 forms.InitiativeProviderForm,
                 ], 
                condition_dict={'3': show_provider_form})
    return view(request)


class InitiativeCreationWizard(SessionWizardView):
    
    template_name = "forms/initiative_creation_wizard.html"
    file_storage = DefaultStorage()
    step_titles = [_('Basic initiative information'),
                   _('Initiative encryption certificate and owners'),
                   _('Countries in which the initiative should be open'),
                   _('Required user data providers for active countries'),
                   ]
    step_to_title = dict([(str(i), title) 
                          for i, title in enumerate(step_titles)])
    
    def done(self, form_list, **kwargs):
        create_form = form_list[0]
        certificate_form = form_list[1]
        country_form = form_list[2]
        if len(form_list) > 3:
            # provider form might not be present if there are not countries
            # with providers
            provider_form = form_list[3]
        else:
            provider_form = None
        # initiative itself
        initiative = create_form.save()
        # certificate
        cert_file = certificate_form.cleaned_data.get('certificate_file')
        certificate = col_models.Certificate(initiative=initiative,
                                             certificate=cert_file.read())
        certificate.save()
        # owners
        for user in certificate_form.cleaned_data.get('initiative_owners', []):
            iown = col_models.InitiativeOwner(initiative=initiative,
                                              user=user)
            iown.save()
        # country settings and providers
        all_providers = dict([(provider.code_name, provider) for provider in 
                              col_models.UserDataProvider.objects.all()])
        for country in country_form.cleaned_data['countries']:
            required_providers = []
            if provider_form:
                provider_ids = provider_form.cleaned_data.get(country.iso_code,
                                                              [])
                for prov_id in provider_ids:
                    provider = all_providers.get(prov_id)
                    if not provider:
                        raise ValueError("No such provider %s", prov_id)
                    required_providers.append(provider)
            ics = col_models.InitiativeCountrySettings(initiative=initiative,
                                                       country=country)
            ics.save()
            for provider in required_providers:
                ics.required_providers.add(provider)
        initiative.save()
        # log it
        content_type = ContentType.objects.get_for_model(initiative)
        LogEntry.objects.log_action(self.request.user.pk, content_type.pk,
                                    initiative.pk, unicode(initiative),
                                    1, "Created using wizard")
        # cleanup
        self.file_storage.delete(cert_file.name)
        cache.clear() # clear the whole cache to make sure no stale data is there
        return HttpResponseRedirect(
                reverse(initiative_created, args=[initiative.code_name]))


    def get_form(self, step=None, data=None, files=None):
        if step is None:
            step = self.steps.current
        if step == '1':
            prev_data = self.get_cleaned_data_for_step('0') or {}
            start_date = prev_data.get('start_date')
            end_date = prev_data.get('end_date')
            return forms.InitiativeCertificateForm(start_date, end_date,
                                                   data=data, files=files)
        if step == '3':
            prev_data = self.get_cleaned_data_for_step('2') or {}
            countries = prev_data.get('countries')
            return forms.InitiativeProviderForm(countries=countries, data=data,
                                                files=files)
        form = super(InitiativeCreationWizard, self).get_form(step, data, files)
        return form
    
    def get_context_data(self, form, **kwargs):
        context = super(InitiativeCreationWizard, self).get_context_data(
                                                            form=form, **kwargs)
        context['step_title'] = self.step_to_title.get(self.steps.current)
        return context


@login_required
@no_stale_password
@no_go_back
def initiative_created(request, code_name):
    if not request.user.has_perm("collector.add_initiative"):
        return HttpResponseForbidden(
                    "Forbidden: You are not allowed to view this page.")
    try:
        initiative = col_models.Initiative.objects.get(code_name=code_name)
    except col_models.Initiative.DoesNotExist:
        raise Http404
    data = dict(initiative=initiative)
    return render(request, "initiative_created.html", data)


@login_required
@no_stale_password
@no_go_back
def initiative_maintenance_page(request):
    if not (request.user.is_staff):
        return HttpResponseForbidden("Forbidden: You are not use this page.")
    initiative = None
    action = None
    if request.method == "POST":
        # we have some post data
        code = request.POST.get("initiative")
        initiative, response = _get_initiative(request, code, mode='staff')
        if not initiative and response:
            return response
        action = request.POST.get("action")
        if action not in ("preregistration","activate","close"):
            action = None
    if initiative and action:
        if action == "close":
            if _allowed_to_close_initiative(request.user, initiative):
                # ok, close it
                initiative.status = initiative.STATUS_CLOSED
                initiative.save()
            else:
                return HttpResponseForbidden(
                                    "You are not allowed to close initiative")
        elif action in ("preregistration","activate"):
            if action == "activate":
                requested_status = initiative.STATUS_ACTIVE
            elif action == "preregistration":
                requested_status = initiative.STATUS_PREREG
            else:
                raise Http404
            can_advance = request.user.has_perm(
                                    "collector.advance_initiative_status")
            can_change = request.user.has_perm(
                                    "collector.change_initiative_status")
            if (initiative.status < requested_status and can_advance) or \
               can_change:
                initiative.status = requested_status
                initiative.save()
            else:
                return HttpResponseForbidden(
                    "You are not allowed to change status of the initiative")
        else:
            raise Http404
        # clear the whole cache to make sure the change comes immediately
        # in effect
        cache.clear()
        return HttpResponseRedirect(reverse(initiative_maintenance_page))
    else:
        initiatives = col_models.Initiative.objects.all().order_by("code_name")
        irecords = []
        for initiative in initiatives:
            irecords.append(
                {"initiative": initiative,
                 "can_manipulate": _allowed_to_manipulate_initiatives(request.user),
                 "can_advance": _allowed_to_advance_initiative_status(request.user),
                 "can_close": _allowed_to_close_initiative(request.user, initiative),
                 })
        data = dict(initiatives=irecords)
        return render(request, "initiative_maintenance.html", data)


@login_required
@no_stale_password
@no_go_back
def initiative_test_output(req, code_name):
    import json
    import zlib
    from base64 import b64encode
    from cStringIO import StringIO
    from gzip import GzipFile
    import csv
    from datetime import datetime
    from agora.collector.encryption import Encryptor
    # get initiative
    initiative, response = _get_initiative(req, code_name, mode='owner')
    if not initiative and response:
        # something was not ok
        return response

    pub_key = Encryptor.extract_public_key_from_cert(
                                            initiative.certificate.certificate)
    encryptor = Encryptor(pub_key, sym_cipher=settings.ENC_SYMMETRIC_CIPHER,
                         key_lifetime=settings.ENC_KEY_LIFETIME)
    # generate the data to encrypt
    to_encrypt = [{'name': 'given_name', 'value': 'Test'},
                  {'name': 'family_name', 'value': 'User'},
                  {'name': 'citizenship', 'value': 'CZ'},
                  {'name': 'id_number', 'value': '0123456789'},
                  {'name': 'id_type', 'value': 'CZ01'},
                  ]
    # encryption
    to_encrypt = json.dumps(to_encrypt, sort_keys=True)
    to_encrypt = zlib.compress(to_encrypt)
    enc_key, enc_text = encryptor.encrypt_string(to_encrypt)
    # generate the test data
    test_data = StringIO()
    test_gzip = GzipFile(mode="w", fileobj=test_data)
    dumper = csv.writer(test_gzip)
    dumper.writerow((2, 1, 1))
    # dump id-document id->name mapping
    id_docs = {}
    for cifvv in col_models.CountryIdentFieldValidValue.objects.filter(
                            country_ident_field__ident_field__slug="id_type"):
        id_docs[cifvv.code] = cifvv.value
        dumper.writerow((3, b64encode(json.dumps(id_docs))))
    dumper.writerow((0, b64encode(enc_key)))
    now = datetime.now().date()
    dumper.writerow((1, "CZ", now, settings.ENC_SYMMETRIC_CIPHER,
                     b64encode(enc_text)))
    test_gzip.close()
    res = HttpResponse(test_data.getvalue(),
                       mimetype="application/octet-stream")
    res["Content-Disposition"] = "attachment; filename={0}-test.agora".format(
                                                                    code_name)
    return res
    
        
@no_go_back
def login_page(request):
    next_page = request.POST.get("next", None)
    if not next_page:
        next_page = request.GET.get("next", None)
    if next_page:
        # next page supplied by the user - we need to check and sanitize it
        try:
            resolve_with_prefix(next_page)
        except Http404:
            logger.warn("Someone is trying some funny business with the "
                        "'next_page' field: '%s'", next_page)
            next_page = None
    if not next_page:
        next_page = reverse(user_page)
    if request.user and request.user.is_active:
        return HttpResponseRedirect(next_page)
    username = request.POST.get('username')
    password = request.POST.get('password')
    error = ""
    # detect too many unsuccessful attempts
    forward_addr = request.META.get('HTTP_X_FORWARDED_FOR', '')[:200] # limit
    if forward_addr:
        if set(forward_addr) - IP_ADDR_CHARS:
            # if there are strange characters in the field, ignore it
            forward_addr = None
    ip_addr = request.META.get("REMOTE_ADDR")
    if forward_addr:
        used_addr = forward_addr
    else:
        used_addr = ip_addr
    limit_cache_key = "_login_fail:%s" % used_addr
    limit_cache_key_ip_only = "_login_fail_ip:%s" % ip_addr
    wrong_attempts = cache.get(limit_cache_key, 0)
    wrong_attempts_ip_only = cache.get(limit_cache_key_ip_only, 0)
    too_many_wrong = False
    if wrong_attempts >= LOGIN_ATTEMPTS or \
       wrong_attempts_ip_only >= LOGIN_ATTEMPTS_IP_ONLY:
        too_many_wrong = True
        minutes = (LOGIN_BLOCK_TIME // 60)
        error = ungettext("Too many unsuccessful attempts at login - "
            "you have to wait %d minute until you are allowed to try "
            "again. Until then all attempts will be ignored and the "
            "timeout will be renewed with every new attempt.",
            "Too many unsuccessful attempts at login - "
            "you have to wait %d minutes until you are allowed to try "
            "again. Until then all attempts will be ignored and the "
            "timeout will be renewed with every new attempt.",
            minutes) % minutes
    # do the authentication itself
    if username and password:
        if too_many_wrong:
            # we set it again to restart the counter because the client
            # tried to supply login data, otherwise we would not reset it
            cache.set(limit_cache_key, wrong_attempts, LOGIN_BLOCK_TIME)
        else:
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(next_page)
                else:
                    error = _("Account is disabled")
            else:
                # log an unsuccessful attempt
                wrong_attempts += 1
                wrong_attempts_ip_only += 1
                if wrong_attempts >= LOGIN_ATTEMPTS:
                    logger.error("%d wrong login attempts from effective ip %s",
                                 wrong_attempts, used_addr)
                if wrong_attempts_ip_only >= LOGIN_ATTEMPTS_IP_ONLY:
                    logger.error("%d wrong login attempts from real ip %s",
                                 wrong_attempts_ip_only, ip_addr)
                cache.set(limit_cache_key, wrong_attempts, LOGIN_BLOCK_TIME)
                cache.set(limit_cache_key_ip_only, wrong_attempts_ip_only,
                          LOGIN_BLOCK_TIME)
                error = _("Invalid login: wrong username or password")
    return render(request, "login.html", dict(error=error, next=next_page,
                                              too_many_wrong=too_many_wrong))


@login_required
@no_go_back
def password_change_page(request):
    # get the user profile
    try:
        profile = request.user.get_profile()
    except models.UserProfile.DoesNotExist:
        profile = models.UserProfile.objects.create(user=request.user)
    # check the data in the form
    if request.method == 'POST': 
        form = forms.StrongPasswordChangeForm(request.user, data=request.POST)
        if form.is_valid():
            password = form.cleaned_data['new_password1']
            request.user.set_password(password)
            request.user.save()
            profile.needs_password_change = False
            profile.save()
            return HttpResponseRedirect(reverse(password_change_success_page))
    else:
        form = forms.StrongPasswordChangeForm(request.user)

    change_forced = profile.needs_password_change
    return render(request, "password_change.html",
                  dict(form=form, change_forced=change_forced))


@login_required
@no_stale_password
@no_go_back
def password_change_success_page(request):
    return render(request, "password_change_success.html")


@login_required
@no_go_back
def logout_page(request):
    language = request.session.get('django_language')
    if request.method == 'POST':
        next_page = request.POST.get("next", reverse(login_page))
        logout(request)
    else:
        next_page = request.META.get("HTTP_REFERER", reverse(user_page))
    # validate the next_page setting
    if next_page:
        # next page supplied by the user - we need to check and sanitize it
        try:
            resolve_with_prefix(next_page)
        except Http404:
            logger.warn("Someone is trying some funny business with the "
                        "'next_page' field: '%s'", next_page)
            next_page = None
    if not next_page:
        next_page = reverse(login_page)
    if language:
        request.session['django_language'] = language
    return HttpResponseRedirect(next_page)  


def _get_initiative(request, code_name, mode="owner"):
    """
    Finds a matching initiative and checks if the current user has rights to
    work with it.
    In mode 'owner' only owners are permitted to get the initiative,
    in mode 'staff' only staff members with rights to work with initiatives
    are allowed to get the initiative.
    It returns a tuple (initiative, response). In case everything is ok,
    it will be (initiative, None) in case of problem (None, response) where
    response is the response that the view should return (403 or something
    similar).
    It also raises Http404 which might be ignored by the view and let to
    propagate upwards
    """
    try:
        initiative = col_models.Initiative.objects.get(code_name=code_name)
    except col_models.Initiative.DoesNotExist:
        raise Http404
    # check the authorization for this user to access this page
    if mode == 'owner':
        # ownership is important here
        owned_initiative_ids = set([res['initiative__id']
                                    for res in request.user.initiativeowner_set\
                                            .values('initiative__id').all()])
        if initiative.id in owned_initiative_ids:
            return (initiative, None)
        return (None, HttpResponseForbidden("Forbidden: You are not allowed "
                                            "to access this page."))
    elif mode == 'staff':
        # test if the user is staff
        if _allowed_to_manipulate_initiatives(request.user):
            return (initiative, None)
        return (None, HttpResponseForbidden("Forbidden: You are not allowed "
                                            "to access this page."))
    else:
        raise Exception("Unsupported mode '%s'" % mode)


def _allowed_to_manipulate_initiatives(user):
    """
    Returns True if the user is allowed to manipulate initiative data
    (create and change initiatives).
    """
    return user.has_perm("collector.modify_initiative")

def _allowed_to_close_initiative(user, initiative):
    if not initiative or initiative.within_date_bounds():
        close_is_advance = False
    else:
        close_is_advance = True
    if user.has_perm("collector.close_initiative") or \
       user.has_perm("collector.change_initiative_status"):
        return True
    if close_is_advance and \
       user.has_perm("collector.advance_initiative_status"):
        return True
    return False

def _allowed_to_advance_initiative_status(user):
    if user.has_perm("collector.change_initiative_status") or \
       user.has_perm("collector.advance_initiative_status"):
        return True
    return False
