# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# django imports
from django import forms
from django.utils.translation import ungettext_lazy, ugettext_lazy as _

def validate_password_strength(password):
    MIN_LENGTH = 10
    # minimum length
    if len(password) < MIN_LENGTH:
        raise forms.ValidationError(
            ungettext_lazy("The new password must be at least %(min_length)d "
                           "character long.",
                           "The new password must be at least %(min_length)d "
                           "characters long.",
                           MIN_LENGTH) % {"min_length": MIN_LENGTH})
    # at least one letter
    alpha = len([1 for letter in password if letter.isalpha()])
    number = len([1 for letter in password if letter.isdigit()])
    other = len(password) - alpha - number
    if alpha == 0 or number == 0 or other == 0:
        raise forms.ValidationError(
            _("The new password must contain at least one letter, "
              "one number and one special character."))
    return password