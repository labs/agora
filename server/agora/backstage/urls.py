# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

from django.conf.urls.defaults import patterns, url
import views
from forms import InitiativeCreationForm

urlpatterns = patterns('',
    # HTML stuff:
    (r'^login/$', views.login_page),
    (r'^logout/$', views.logout_page),
    (r'^$', views.user_page),
    (r'^password_change/$', views.password_change_page),
    (r'^password_change_success/$', views.password_change_success_page),
    # organizer interface related stuff
    (r'^initiative/(?P<code_name>\w+)/$', views.initiative_overview),
    (r'^initiative/(?P<code_name>\w+)/test_output/$', views.initiative_test_output),
    #(r'^initiative/(?P<code_name>\w+)/change_status/$', views.change_initiative_status),
    # staff stuff
    #(r'^create_initiative/$', views.initiative_creation_page),
    url(r'^create_initiative/$', views.initiative_creation_page),
    url(r'^maintain_initiatives/$', views.initiative_maintenance_page),
    url(r'^initiative_created/(?P<code_name>\w+)/$', views.initiative_created),
)