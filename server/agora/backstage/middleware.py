# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in stuff
import logging
import base64
import hashlib

# top-level stuff
logger = logging.getLogger("agora.backstage.middleware")


class FingerprintingSessionMiddleware(object):
    """
    Implements fingerprinting of user ip address and user agent in order to
    help limit a session to one real user and prevent session stealing
    """
    
    USER_FP_KEY = "_USER_FP"
    FINGERPRINTED_KEYS = ("REMOTE_ADDR", "HTTP_X_FORWARDED_FOR",
                          "HTTP_USER_AGENT")
    HASH_METHOD = hashlib.sha1

    def process_request(self, request):
        user_fp = request.session.get(self.USER_FP_KEY)
        current_fp = self._calculate_user_fp(request)
        if not user_fp:
            request.session[self.USER_FP_KEY] = current_fp
        else:
            if user_fp != current_fp:
                logger.info("Detected session fingerprint mismatch, "
                            "flushing session")
                request.session.flush()

    def _calculate_user_fp(self, request):
        hasher = self.HASH_METHOD()
        for key in self.FINGERPRINTED_KEYS:
            hasher.update(request.META.get(key, ""))
        return base64.b64encode(hasher.digest())
