# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

"""
Tests for the Agora backstage application
"""

# built-in modules
from copy import copy

# django stuff
from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

# local imports
import views
import models
from agora.collector import models as col_models


class MemFile(object):
    
    def __init__(self, name, content):
        self.name = name
        self.content = content
        
    def read(self, *args, **kwargs):
        return self.content
    

class InitiativeCreationTest(TestCase):
    fixtures = ['test_basic_data.json']

    test_cert_file1 = MemFile("test.crt", """-----BEGIN CERTIFICATE-----
MIICxjCCAa6gAwIBAgIBATANBgkqhkiG9w0BAQUFADA0MTIwMAYDVQQDEylFQ0kg
Y2xpZW50IHNlbGYtc2lnbmVkIGNlcnRpZmljYXRlIGlzc3VlcjAeFw0xMjA5MTIw
NjMxMTNaFw0xNDA5MTIwNjMxMTNaMBkxFzAVBgNVBAMTDkVDSSBpbml0aWF0aXZl
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnBS91jypFvFSQqSbuRfh
xUhfY/y4XPSo5nIEv1Q956M/bc7EWYzK/8OSJ7/dM9GZwnXe/KSTm+w98G9rYSYt
FmxnsAnnlP+PI3942bDfe0E5PyWwnR8myXPj8UrgEO/3cC2cYewiUOmj6YjOOVJV
eo/3b1JlJIZkLKYUYP49p7MWjdrexv7WuOWwWGuCRSKktII4ONY8D/RyJJ2zLHjq
OyB3njoHX0SIp3j1eL+l5Ex8ZSb4oTlujG7hjYFmzFoJw+HwjYS7ejeLpKrMTdnu
Yn9SLX7O+h7OWAJC2ANPYvv93EscPj/6RHN/d1OXIE5CdnPCEh3YFaQSeC2s1M2+
AwIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQB6rMQgO1ct7n3FWCoIsaNL9rH/FY0Z
SYaZkB595+5r2YwxdAWThgDbYv5Z8bo1bbaNLRJXZpZhwro6BrPUgp6qBFSknfeh
eunojzt+6Mr7iW7wBwZw0krdooR7kzxdLrv+tcnbCjaq+lDesYdgDMHQrtwffA5j
8VGUR9WO5EhuGcg2Mg0E7bXQQdCYuiI1EMuyjvCJsQoCKbN0z9Ovmuy2JpitZiSG
1S2EgRIJESKYIrJ2gdEcMRskd5cGVA4EQ0VPyKlM0Efw8rt8klGTm2eDVMtBM8g9
ai4+sdu0+M2eoTnyqL96VKN0poLe6iD9zZX1vSCOli6OK0sWLwAKliBE
-----END CERTIFICATE-----""")
    
    test_cert_file2 = MemFile("test.crt", "corrupted file")

    test_data_ok = [{'code_name': 'test',
                     'title_en': 'Test initiative',
                     'start_date': '2012-09-07',
                     'end_date': '2013-09-06',
                     'website': 'http://labs.nic.cz/',
                     },
                    {'certificate_file': test_cert_file1,
                     'initiative_owners': [1]},
                    {'countries': [3, 16, 4, 10]}, #(CZ, HU, DK, FR)
                    {'CZ': ['mojeid']},
                    ]
    
    test_data_bad_cert = [{'code_name': 'test',
                           'title_en': 'Test initiative',
                           'start_date': '2012-09-07',
                           'end_date': '2013-09-06',
                           'website': 'http://labs.nic.cz/',
                           },
                          {'certificate_file': test_cert_file2,
                           'initiative_owners': [1]},
                          ]
    
    test_user_name = "test"
    test_user_password = "test_password"
    
    def setUp(self):    
        self.client = Client()
        self.test_url = reverse(views.initiative_creation_page)
        user = User.objects.create_user(self.test_user_name,
                                        password=self.test_user_password)
        user.is_staff=True
        user.save()
        try:
            profile = user.get_profile()
        except models.UserProfile.DoesNotExist:
            profile = models.UserProfile.objects.create(user=user)
        profile.needs_password_change = False
        profile.save()
        
        
    def test_ok(self):
        self.client.login(username=self.test_user_name,
                          password=self.test_user_password)
        test_data = self.test_data_ok
        code_name = test_data[0]['code_name']
        for i, data in enumerate(test_data):
            # prepare the data
            if i in (1, 3):
                # these do not use prefix - who knows why
                post_data = copy(data)
            else:
                post_data = {}
                for key, value in data.iteritems():
                    post_data["{0}-{1}".format(i, key)] = value
            post_data['initiative_creation_wizard-current_step'] = str(i)
            # post the data
            response = self.client.post(self.test_url, post_data)
            # check the result
            if i == (len(test_data) - 1):
                # the last step
                self.assertRedirects(response, reverse(views.initiative_created,
                                                       args=[code_name]))
            else:
                # intermediate step
                self.assertEqual(response.status_code, 200)
                self.assertContains(response,
                    'name="initiative_creation_wizard-current_step" '
                    'value="{0}"'.format(i+1))
        try:
            instance = col_models.Initiative.objects.get(code_name=code_name)
        except col_models.Initiative.DoesNotExist:
            self.fail("An initiative instance was not created.")
        else:
            self.assertEqual(instance.certificate.certificate,
                             self.test_cert_file1.content)


    def test_wrong_cert_file(self):
        self.client.login(username=self.test_user_name,
                          password=self.test_user_password)
        test_data = self.test_data_bad_cert
        code_name = test_data[0]['code_name']
        for i, data in enumerate(test_data):
            post_data = {}
            post_data['initiative_creation_wizard-current_step'] = str(i)
            for key, value in data.iteritems():
                post_data["{0}-{1}".format(i, key)] = value
            response = self.client.post(self.test_url, post_data)
            self.assertEqual(response.status_code, 200)
            if i == (len(test_data) - 1):
                # the last step - should fail thus remaining at the same step
                self.assertContains(response,
                    'name="initiative_creation_wizard-current_step" '
                    'value="{0}"'.format(i))
            else:
                # intermediate step - should be successful and current step 
                # should increase
                self.assertContains(response,
                    'name="initiative_creation_wizard-current_step" '
                    'value="{0}"'.format(i+1))
