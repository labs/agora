��    S      �  q   L        �     7  �  0   �  C   	  6   X	     �	     �	     �	     �	     �	  /   �	     
  ;   
     X
     j
     �
     �
     �
     �
     �
  	   �
  0   �
     	     "     *     <     A     J     O     X  ,   _     �  7   �     �     �  )        7  
   =     H     O  �   d     �     �     �      	  )   *     T     ]  "   m  
   �     �     �  1   �  
   �      �               .     5  !   M     o  �   {  �   #  *   �  7   �  $   *  .   O  �   ~  X        Z  �  `     �  	             !     *  ;   7  H   s     �     �     �     �  z  �  �   q  L  �  3   G  ;   {  9   �     �       	             /  ;   L     �  N   �     �  "     %   '     M     d  	   q     {     �  0   �     �     �     �     �  	   �     �            /        L  6   ^     �  &   �  D   �          &  	   ?     I  �   ^     �     �     	       &   3     Z     `  !   m     �     �     �  2   �     �           &     +     :     B  $   Z       �   �  �   )  .   �  @     &   C  4   j  �   �  _   b      �   �  �      b#     s#     �#     �#  	   �#  ^   �#  O   &$     v$     �$     �$  
   �$     @   H   G          -   >   <      ,       L                 /      '           	          J   O                   *   3   9   2   "      D       &   4         6   :       .   F   E      A   P   $   B       K   R   
   0                  !       )              M          ;              ?   Q          C              5   S         =   7       N              +      (                  8             1   I   %          #    
After the ability to decrypt support statements has been successfully verified,
the staff can activate the initiative in the administrative interface. 
 
At first the owner of the initiative has to visit the overview page of this initiative,
download the test output and verify, that he is able to decrypt it in the
client application. <strong>This step is critical and without it all
statements of support might be lost due to inability to decrypt them!</strong>
 
Initiative '%(title)s' was successfully created 
The initiative is not yet active and so does not collect support.
 A change of password is required for security reasons. Account is disabled Actions Activate Activate preregistration Admin interface Admin interface to adjust initiative properties Administration At present, you can continue to the following destinations: Back to user page Basic initiative information Can create staff users Certificate file Change password Close Continue Countries Countries in which the initiative should be open Countries over threshold Country Create initiative Date Download Edit End date Error: Initiative encryption certificate and owners Initiative maintenance Initiative maintenance page to change initiative status Initiative owners Initiative successfully created Invalid login: wrong username or password Login Login page Logout Maintain initiatives Note: The new password must be at least 10 characters long
and contain at least one letter, one number and one special character.
 OK Obtained support Outside range Overview of initiative %(title)s Overview of initiative <em>%(title)s</em> Password Password change Password was successfully changed. Percentage Previous step Required support Required user data providers for active countries Start date Statements of support by country Status Step %(step)s: Submit Support collection page Support statement collection page Test output The certificate 'notAfter' field (%(not_after)s) should extend at least 30 days after the desired end of collection of support, that is at least till %(min_end_date)s. The certificate 'notBefore' field (%(not_before)s) is later than the collection of electronic support starts (%(max_start_date)s). This certificate cannot be used. The end date must be after the start date. The end date must be within one year of the start date. The file is not a valid certificate! The initiative cannot end before current date. The new password must be at least %(min_length)d character long. The new password must be at least %(min_length)d characters long. The new password must contain at least one letter, one number and one special character. Title Too many unsuccessful attempts at login - you have to wait %d minute until you are allowed to try again. Until then all attempts will be ignored and the timeout will be renewed with every new attempt. Too many unsuccessful attempts at login - you have to wait %d minutes until you are allowed to try again. Until then all attempts will be ignored and the timeout will be renewed with every new attempt. Total support User page User profile for '%s' Username Within range You can use this file to test decryption in client program. You will not be able to use your account until you change your password. Your account Your initiatives collecting support inactive Project-Id-Version: 0-git
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-10-29 09:03+0100
PO-Revision-Date: 2012-10-29 09:05+0100
Last-Translator: Beda Kosata <bedrich.kosata@nic.cz>
Language-Team: Czech <>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 
Po úspěšném ověření schopnosti rozšifrovat data o podpoře může 
podpora aktivovat iniciativu v administrativním rozhraní.
 
Nejprve je třeba, aby vlastník iniciativy navštívil stránku s přehledem této iniciativy,
stáhl testovací výstup a ověřil, že je schopen jej v klientské aplikaci rozšifrovat.
<strong>Tento krok je nezbytný - bez něj by mohlo dojít ke ztrátě
 všech dat o podpoře z důvodu nemožnosti je rozšifrovat!</strong>
 
Iniciativa '%(title)s' byla úspěšně vytvořena 
Iniciativa není dosud aktivní a nesbírá tedy podporu.
 Z bezpečnostních důvodů je vyžadována změna hesla. Účet je neaktivní Akce Aktivovat Aktivovat předregistraci Administrátorské rozhraní Administrativní rozhraní pro změnu nastavení iniciativy Administrace V této chvíli můžete pokračovat na některou z následujících stránek: Zpět na uživatelskou stránku Základní informace o iniciativě Může vytvářet interní uživatele Soubor s certifikátem Změna hesla Uzavřít Pokračovat Země Země, ve kterých má být iniciativa otevřena Zemí nad hranicí Země Vytvořit iniciativu Datum Stáhnout Upravit Datum ukončení Chyba: Šifrovací certifikát a vlastníci iniciativy Správa iniciativ Stránka správy iniciativ pro změnu stavu iniciativy Vlastníci iniciativy Iniciativa byla úspěšně vytvořena Neplatné přihlášení: špatné přihlašovací jméno nebo heslo Přihlášení Přihlašovací stránka Odhlásit Spravovat iniciativy Pozor: Nové heslo musí být minimálně 10 znaků dlouhé a obsahovat
minimálně jedno písmeno, jedno číslo a jeden speciální znak.
 V pořádku Získaná podpora Mimo rozsah Přehled iniciativy %(title)s Přehled iniciativy <em>%(title)s</em> Heslo Změna hesla Heslo bylo úspěšně změněno. V procentech Předchozí krok Vyžadovaná podpora Vyžadovaní poskytovatelé dat pro aktivní země Datum počátku Vyjádření podpory podle zemí Stav Krok %(step)s: Odeslat Stránka sběru podpory Stránka sběru vyjádření podpory Testovací výstup Položka certifikátu 'notAfter' (%(not_after)s) by měla pokrývat nejméně 30 dní po skončení sběru podpory, tedy alespoň do %(min_end_date)s. Položka certifikátu 'notBefore' (%(not_before)s) je pozdější než začátek sběru elektronické podpory (%(max_start_date)s). Tento certifikát není použitelný. Datum ukončení musí být po datu počátku. Datum ukončení musí být v rozsahu jednoho roku od počátku. Soubor neobsahuje platný certifikát! Iniciativa nemůže končit před aktuálním datem. Nové heslo musí být minimálně %(min_length)d znak dlouhé. Nové heslo musí být minimálně %(min_length)d znaky dlouhé. Nové heslo musí být minimálně %(min_length)d znaků dlouhé. Nové heslo musí obsahovat minimálně jedno písmeno, jedno číslo a jeden speciální znak. Název Příliš mnoho neúspěšných pokusů o přihlášení - musíte počkat %d minutu dokud nebudete moci přihlášení opakovat. Do té doby budou pokusy o přihlášení ignorovány a doba čekání bude vždy obnovena. Příliš mnoho neúspěšných pokusů o přihlášení - musíte počkat %d minuty dokud nebudete moci přihlášení opakovat. Do té doby budou pokusy o přihlášení ignorovány a doba čekání bude vždy obnovena. Příliš mnoho neúspěšných pokusů o přihlášení - musíte počkat %d minut dokud nebudete moci přihlášení opakovat. Do té doby budou pokusy o přihlášení ignorovány a doba čekání bude vždy obnovena. Celková podpora Uživatelská stránka Uživatelský profil pro '%s' Uživatelské jméno V rozsahu Tento soubor můžete použít k otestování rozšifrování výstupu v klientském programu. Dokud vaše heslo nezměníte, nebude možné používat funkce vašeho účtu. Váš účet Vaše iniciativy sbírá podporu iniciativa 