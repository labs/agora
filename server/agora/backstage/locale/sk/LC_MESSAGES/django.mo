��    Q      �  m   ,      �  �   �  7  {  0   �  C   �  6   (	     _	     s	     {	     �	     �	  /   �	     �	  ;   �	     (
     :
     W
     n
     
     �
     �
  	   �
  0   �
     �
     �
     �
                         (  ,   /     \  7   s     �     �  )   �       
               �   4     �     �     �      �  )   �     $     -  "   =  
   `     k     y  1   �  
   �      �     �     �     �       !        ?  �   K  �   �  *   �  7   �  $   �  .     �   N  X   �     *     0  	   >     H     Q  ;   ^  H   �     �     �            �    �   �  R  U  2   �  >   �  8        S     h  
   n     y     �  ;   �     �  I   �  $   G  $   l  ;   �     �     �  	   �     �       2        B     W     _     u  	   |     �     �     �  0   �     �  7   �     $  %   ;  :   a     �     �  
   �     �  �   �  
   l     w     �     �  '   �     �     �     �               1  5   F     |  !   �     �     �     �     �  !   �       �     �   �  5   Y  A   �  &   �  8   �  �   1  _   �     P     W     h     �  	   �  \   �  R   �     P      ]      o      ~                  
       :   )         !   6   *              O       4   (           L   7      %   I   F   /         +   Q       &           P   ;   G              A   B       8           <          @   9                   #         '       K                ?         C      E   =            H   M   "       2         0   	      D   3   ,       .      -   N   1               >   $   5       J               
After the ability to decrypt support statements has been successfully verified,
the staff can activate the initiative in the administrative interface. 
 
At first the owner of the initiative has to visit the overview page of this initiative,
download the test output and verify, that he is able to decrypt it in the
client application. <strong>This step is critical and without it all
statements of support might be lost due to inability to decrypt them!</strong>
 
Initiative '%(title)s' was successfully created 
The initiative is not yet active and so does not collect support.
 A change of password is required for security reasons. Account is disabled Actions Activate Activate preregistration Admin interface Admin interface to adjust initiative properties Administration At present, you can continue to the following destinations: Back to user page Basic initiative information Can create staff users Certificate file Change password Close Continue Countries Countries in which the initiative should be open Countries over threshold Country Create initiative Date Download Edit End date Error: Initiative encryption certificate and owners Initiative maintenance Initiative maintenance page to change initiative status Initiative owners Initiative successfully created Invalid login: wrong username or password Login Login page Logout Maintain initiatives Note: The new password must be at least 10 characters long
and contain at least one letter, one number and one special character.
 OK Obtained support Outside range Overview of initiative %(title)s Overview of initiative <em>%(title)s</em> Password Password change Password was successfully changed. Percentage Previous step Required support Required user data providers for active countries Start date Statements of support by country Status Step %(step)s: Submit Support collection page Support statement collection page Test output The certificate 'notAfter' field (%(not_after)s) should extend at least 30 days after the desired end of collection of support, that is at least till %(min_end_date)s. The certificate 'notBefore' field (%(not_before)s) is later than the collection of electronic support starts (%(max_start_date)s). This certificate cannot be used. The end date must be after the start date. The end date must be within one year of the start date. The file is not a valid certificate! The initiative cannot end before current date. The new password must be at least %(min_length)d character long. The new password must be at least %(min_length)d characters long. The new password must contain at least one letter, one number and one special character. Title Total support User page Username Within range You can use this file to test decryption in client program. You will not be able to use your account until you change your password. Your account Your initiatives collecting support inactive Project-Id-Version: 0-git
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-10-18 13:52+0200
PO-Revision-Date: 2012-10-30 13:34+0100
Last-Translator: 
Language-Team: American English <kde-i18n-doc@kde.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Lokalize 1.0
 
Po úspešnom overení schopnosti rozšifrovať dáta o podpore môže 
podpora (privilegovaní úžívatelia) aktivovať iniciatívu v administratívnom rozhraní.
 
Najprv je treba, aby vlastník iniciatívy navštívil stránku s prehľadom tejto iniciatívy,
stiahol testovací výstup a overil, že je schopný ho v klientskej aplikácii rozšifrovať.
<strong>Tento krok je nevyhnutný - bez neho by mohlo dojsť k strate
všetkých dát o podpore z dôvodu nemožnosti ich rozšifrovať!</strong>
 
Iniciatíva '%(title)s' bola úspešne vytvorená 
Iniciatíva nie je zatiaľ aktívna a nezbiera teda podporu.
 Z bezpečnostných dôvodov je vyžadovaná zmena hesla. Účet je neaktívny Akcia Aktivovať Aktivovať predregistráciu Administrátorské rozhranie Administratívne rozhranie pre zmenu nastavení iniciatívy Administrácia V tejto chvíli môžete pokračovať na jednu z nasledujúcich stránok: Naspäť na užívateľskú stránku Základné informácie o iniciatíve Môže vytvárať interných privilegovaných užívateľov Súbor s certifikátom Zmena hesla Uzavrieť Pokračovať Krajiny Krajiny, v ktorých má byť iniciatíva otvorená Krajiny nad hranicou Krajina Vytvoriť iniciatívu Dátum Stiahnuť Upraviť Dátum ukončenia Chyba: Šifrovací certifikát a vlastníci iniciatívy Správa iniciatív Stránka správy iniciatív pre zmenu stavu iniciatívy Vlastníci iniciatívy Iniciatíva bola úspešne vytvorená Neplatné prihlásenie: zlé prihlasovacie meno nebo heslo Prihlásenie Prihlasovacia stránka Odhlásiť Spravovať iniciatívy Pozor: Nové heslo musí byť minimálne 10 znakov dlhé a obsahovať
minimálne jedno písmeno, jedno číslo a jeden špeciálny znak.
 V poriadku Získaná podpora Mimo rozsah Prehľad iniciatívy %(title)s Prehľad iniciatívy <em>%(title)s</em> Heslo Zmena hesla Heslo bolo úspešne zmenené. V percentách Predchádzajúci krok Vyžadovaná podpora Vyžadovaní poskytovatelia dát pre aktívne krajiny Dátum začiatku Vyjadrenia podpory podľa krajín Stav Krok %(step)s: Odoslať Stránka zberu podpory Stránka zberu vyjadrení podpory Testovací výstup Položka certifikátu 'notAfter' (%(not_after)s) by mala siahať aspoň 30 dní po skončení dátumu podpory,teda aspoň do %(min_end_date)s. Položka certifikátu 'notBefore' (%(not_before)s) má neskorší dátum než začiatok zberu elektronickej podpory (%(max_start_date)s). Tento certifikát nie je použiteľný. Dátum ukončenia musí byť pred dátumom začiatku. Dátum ukončenia musí byť najneskǒr rok od dátumu začiatku. Súbor neobsahuje platný certifikát! Iniciatíva nemôže skončiť pred aktuálnym dátumom. Dĺžka nového hesla v znakoch musí byť aspoň %(min_length)d. Nové heslo musí byť minimálne %(min_length)d znaky dlhé. Nové heslo musí byť minimálne %(min_length)d znakov dlhé. Nové heslo musí obsahovať minimálne jedno písmeno, jedno číslo a jeden špeciálny znak. Názov Celková podpora Užívateľská stránka Užívateľské meno V rozsahu Tento súbor môžete použiť na otestovanie rozšifrovania výstupu v klientskom programe. Pokiaľ vaše heslo nezmeníte, nebude možné používať funkcie vašeho účtu. Váš účet Vaše iniciatívy zbiera podporu iniciatíva 