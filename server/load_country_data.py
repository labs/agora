# encoding: utf-8

#   Agora is a web application for electronic submission of statements
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

# built-in modules
import csv
import logging
import re
import json
import os
import sys
sys.path.append("./agora")
os.environ['DJANGO_SETTINGS_MODULE'] = 'agora.settings'

# local imports
from agora.collector import models

# initialization
logging.basicConfig(level=logging.DEBUG)

def process_required_fields():
    data_file = "data/required_fields.csv"
    logging.debug("Loading data file %s", data_file)
    with open(data_file, 'r') as infile:
        reader = csv.reader(infile)
        header = reader.next()[2:]
        ident_fields = []
        for col in header:
            ident_field = models.IdentField.objects.get(slug=col)
            ident_fields.append(ident_field)
        for raw_row in reader:
            row = [x.decode('utf-8') for x in raw_row]
            ccode = row[0]
            _name_cs = row[1]
            # country must already exist - we let a potential error propagate
            country = models.Country.objects.get(iso_code=ccode)
            for i, col in enumerate(row[2:]):
                if col:
                    ident_field = ident_fields[i]
                    try:
                        cidf = models.CountryIdentField.objects.\
                                get(country=country, ident_field=ident_field)
                    except models.CountryIdentField.DoesNotExist:
                        cidf = models.CountryIdentField(country=country,
                                                        ident_field=ident_field,
                                                        position=i)
                        cidf.save()
                        logging.debug("Connected %s - %s", ccode,
                                      ident_field.slug)
                    else:
                        if cidf.position != i:
                            cidf.position = i
                            cidf.save()
                            logging.debug("Synced connection %s - %s", ccode,
                                          ident_field.slug)
            
def process_countries():
    data_file = "data/countries.csv"
    logging.debug("Loading data file %s", data_file)
    with open(data_file, 'r') as infile:
        reader = csv.reader(infile)
        header = reader.next()
        for raw_row in reader:
            row = dict([(header[i], x.decode('utf-8')) 
                        for i,x in enumerate(raw_row)])
            ccode = row['code']
            native_name = row['name_native']
            name_en = row['name_en']
            minimum = int(row['minimum'])
            try:
                country = models.Country.objects.get(iso_code=ccode)
            except models.Country.DoesNotExist:
                logging.debug("Created Country [%s] %s", ccode, name_en)
                country = models.Country(iso_code=ccode, name_en=name_en,
                                         native_name=native_name,
                                         support_limit=minimum)
                country.save()
            else:
                change = False
                if country.name_en != name_en:
                    country.name_en = name_en
                    change = True
                if country.native_name != native_name:
                    country.native_name = native_name
                    change = True
                if country.support_limit != minimum:
                    country.support_limit = minimum
                    change = True
                if change:
                    logging.debug("Synced Country [%s] %s", ccode, name_en)
                    country.save()
            # check flag picture
            if not os.path.isfile("agora/collector/static/%s.png" % ccode.lower()):
                logging.warn("No flag for %s", ccode)


def process_fields():
    data_file = "data/fields.csv"
    logging.debug("Loading data file %s", data_file)
    with open(data_file, 'r') as infile:
        reader = csv.reader(infile)
        _header = reader.next()
        for raw_row in reader:
            slug, typ = raw_row
            ftype = models.IdentField.TYPE_NAME_TO_ID[typ]
            try:
                field = models.IdentField.objects.get(slug=slug)
            except models.IdentField.DoesNotExist:
                field = models.IdentField(slug=slug, data_type=ftype)
                field.save()
                logging.debug("Created IdentField %s", slug)
            else:
                if field.data_type != ftype:
                    field.data_type = ftype
                    logging.debug("Synced IdentField %s", slug)
                    field.save()
                    
def process_id_documents():
    data_file = "data/id_documents.txt"
    logging.debug("Loading data file %s", data_file)
    code_to_documents = {}
    with open(data_file, 'r') as infile:
        ccode = None
        for raw_line in infile:
            line = raw_line.decode('utf-8').strip()
            if line.startswith("#"):
                # this is comment
                pass
            elif line.startswith("-"):
                assert(ccode is not None)
                # this is an id document
                match = re.match("^- (\w+) \| ([^{]+)( {.*})?$", line)
                if match:
                    if ccode not in code_to_documents:
                        code_to_documents[ccode] = []
                    code_to_documents[ccode].append((match.group(1),
                                                     match.group(2)))
                else:
                    raise Exception("Wrong line format: %s", line)
            else:
                ccode = line.strip()
    for ccode, documents in code_to_documents.items():
        cif = models.CountryIdentField.objects.get(country__iso_code=ccode,
                                                   ident_field__slug="id_type")
        for i, (code, name) in enumerate(documents):
            try:
                cifvv = cif.countryidentfieldvalidvalue_set.get(code=code)
            except models.CountryIdentFieldValidValue.DoesNotExist:
                cifvv = models.CountryIdentFieldValidValue(
                                country_ident_field=cif, code=code, value=name,
                                position=i)
                cifvv.save()
                logging.debug("Created ValidValue %s: %s", ccode, code)
            else:
                save = False
                if cifvv.position != i:
                    cifvv.position = i
                    save = True
                if cifvv.value != name:
                    cifvv.value = name
                    save = True
                if save:
                    cifvv.save()
                    logging.debug("Synced ValidValue %s: %s", ccode, code)
                
def process_field_names():
    data_file = "data/field_names.csv"
    logging.debug("Loading data file %s", data_file)
    fields = {}
    with open(data_file, 'r') as infile:
        reader = csv.reader(infile)
        header = reader.next()
        for col in header[1:]:
            fields[col] = {}
        for raw_row in reader:
            row = [cell.decode('utf-8') for cell in raw_row]
            lang = row[0]
            for i, cell in enumerate(row[1:]):
                field_name = header[i+1]
                fields[field_name][lang] = cell
    for field_name, int_names in fields.iteritems():
        field_obj = models.IdentField.objects.get(slug=field_name)
        en_name = int_names['en']
        for lang, int_name in int_names.items():
            if not int_name:
                int_name = en_name
            setattr(field_obj, 'name_'+lang, int_name)
        field_obj.save()
            
def process_data_providers():
    data_file = "data/data_providers.json"
    logging.debug("Loading data file %s", data_file)
    with open(data_file, 'r') as infile:
        data = json.load(infile)
    for rec in data:
        code_name = rec['code_name']
        name = rec['name']
        info = rec.get("info", {})
        replaces_captcha = rec.get("replaces_captcha")
        save = False 
        try:
            provider = models.UserDataProvider.objects.get(code_name=code_name)
        except models.UserDataProvider.DoesNotExist:
            provider = models.UserDataProvider(code_name=code_name)
            save = True
        # simple attrs
        if replaces_captcha is not None:
            if provider.replaces_captcha != replaces_captcha:
                provider.replaces_captcha = replaces_captcha
                save = True
        # name may be either string or mapping into different languages
        if type(name) in (str, unicode):
            name = {"en": name}
        for lang, loc_name in name.items():
            attr = "name_"+lang
            if getattr(provider, attr) != loc_name:
                setattr(provider, attr, loc_name)
                save = True
        # description
        for lang, desc in info.items():
            attr = "info_"+lang
            if getattr(provider, attr) != desc:
                setattr(provider, attr, desc)
                save = True
        if save:
            logging.debug("Loaded provider %s", code_name)
            provider.save()
        # countries for which this provider is supported
        # this information is used when an initiative is created only enable
        # providers in countries where it makes sense
        # if the countries field is not present, then this provider is active
        # regardless of country, to disable a provider an empty list can be used
        all_countries = dict([(ctry.iso_code, ctry)
                              for ctry in models.Country.objects.all()])
        iso_codes = rec.get('countries', list(all_countries.keys()))
        for iso_code in iso_codes:
            country = all_countries[iso_code]
            if not country.user_data_providers.filter(pk=provider.pk).exists():
                logging.debug("Connecting provider '%s' with country '%s'",
                              provider.code_name, iso_code)
                country.user_data_providers.add(provider)
            

            
if __name__ == "__main__":
    process_countries()
    process_fields()
    process_field_names()
    process_required_fields()
    process_id_documents()
    process_data_providers()