# encoding: utf-8

#   Agora is a web application for electronic submission of statements 
#   of support for European Citizens' Initiative
#   Copyright (C) 2012 CZ.NIC, z.s.p.o. (http://www.nic.cz)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

from __future__ import print_function

# built-in modules
import logging
import sys
import unittest
from cStringIO import StringIO


def check_append(old_filename, new_filename):
    """
    Call check_append_obj of files with names old_filename, new_filename
    """
    with file(old_filename, 'r') as old_file:
        with file(new_filename, 'r') as new_file:
            result = check_append_obj(old_file, new_file)
    return result
            

def check_append_obj(old_file, new_file):
    """
    Compare two file objects line by line and return 0 if all lines in old_file
    match those on new_file => new_file was created from old_file by appending.
    Return values:
        0    new_file is old_file with something appended
        2    there is a difference on some line
        3    new_file is shorter than old_file but contains the same content -
             it was truncated
        4    some other error reading new file
    """
    for i, old_line in enumerate(old_file):
        logging.debug("Line #%d", i)
        try:
            new_line = new_file.next()
        except StopIteration:
            logging.debug("Fewer lines in new file and old file, either "
                          "truncation took place or arguments were interposed")
            return 3
        except Exception as exc:
            logging.debug("Error '%s' reading line from new_file", exc)
            return 4
        if old_line.rstrip("\n") != new_line.rstrip("\n"):
            logging.debug("Difference on line #%d", i)
            return 2
    return 0


class AppendCheckTest(unittest.TestCase):
    """
    Unittests for check_append_obj
    """
    
    lines = ["This is first line",
             "this is second",
             "third line.",
             "This is the fourth and last line"]
    
    def test_same(self):
        f1 = StringIO("\n".join(self.lines))
        f2 = StringIO("\n".join(self.lines))
        self.assertEqual(check_append_obj(f1, f2), 0,
                         "Test of equal files failed")
        
    def test_ok(self):
        f1 = StringIO("\n".join(self.lines[:2]))
        f2 = StringIO("\n".join(self.lines))
        self.assertEqual(check_append_obj(f1, f2), 0,
                         "Test of appended content failed")
        
    def test_fail(self):
        f1 = StringIO("\n".join(self.lines[:2]))
        f2 = StringIO("\n".join(self.lines[1:]))
        self.assertEqual(check_append_obj(f1, f2), 2,
                         "Test of not appended content failed")
        
    def test_fail2(self):
        f1 = StringIO("\n".join(self.lines))
        f2 = StringIO("\n".join(self.lines[:2]))
        self.assertEqual(check_append_obj(f1, f2), 3,
                         "Test of reverse appended content failed") 

if __name__ == "__main__":
    from optparse import OptionParser
    # parse options
    opsr = OptionParser(usage="python %prog [options] old_file new_file")
    opsr.add_option("-v", "--verbose", action="store_true",
                    dest="verbose", default=False,
                    help="Log extra info about what the script is doing")
    (options, args) = opsr.parse_args()
    
    if options.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.ERROR)
    # check input
    if len(args) != 2:
        print("This script takes exactly two arguments\n", file=sys.stderr)
        opsr.print_usage()
        sys.exit(1)
    old_filename = args[0]
    new_filename = args[1]
    result = check_append(old_filename, new_filename)
    if result != 0:
        logging.info("File #2 is not derived from file #1!")
    sys.exit(result)